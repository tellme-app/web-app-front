import AuthData from "../models/auth/AuthData";
import RuleName from "../models/auth/RuleName";

export default class AuthContextData {

    data?: AuthData
    loggedIn: boolean

    constructor(loggedIn: boolean, user?: AuthData) {
        this.loggedIn = loggedIn;
        this.data = user;
    }

    hasRule(...rule: RuleName[]): boolean {
        return false
    }
}