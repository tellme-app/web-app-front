import React, {ReactNode, useState} from 'react'
import AuthContextData from "./AuthContextData";

export type AuthContextState = [AuthContextData, (auth: AuthContextData) => void]

const defaultContext: AuthContextState = [
    new AuthContextData(false),
    () => 0
]

export const AuthContext = React.createContext<AuthContextState>(defaultContext)

export const AuthProvider = (props: { children?: ReactNode }) => (
    <AuthContext.Provider
        {...props}
        value={useState(defaultContext[0])}
    />
);
