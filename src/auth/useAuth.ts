import {useContext} from 'react'
import {AuthContext} from './authContext'
import AuthContextData from "./AuthContextData";

export const useAuth = (): AuthContextData => {
    const [authData] = useContext(AuthContext)
    return authData;
}
