export interface FakeClientResponse<T> {
    data: T
    response?: Response
}

export interface FakeSendOptions {
    delay?: number
    error?: any
}

export const fakeSend = <T>(data: T, options: FakeSendOptions = {}) => new Promise<FakeClientResponse<T>>(((resolve, reject) => {
    const delay = options.delay || 1700
    const id = setTimeout(() => {
        if (options.error) reject(options.error)
        else resolve({data})
    }, delay)
}))

export const fakeSendSimple = <T>(data: T, options: FakeSendOptions = {}) => fakeSend(data, options).then(r => r.data)
