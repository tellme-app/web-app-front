import {useEffect, useRef, useState} from "react";

export interface UseRequestStateOptions {
}

export interface RequestState<C, E> {
    loading: boolean,
    error?: E,
    content: C,
    setContent: (content: C) => void
    setError: (error: E) => void
    setLoading: (loading: boolean) => void
}

export const useRequestStateBase = <C, E = any>(getRequest: () => Promise<C>, defaultContent: C) => {
    const [content, setContent] = useState<C>(defaultContent)
    const [error, setError] = useState<any>()
    const [loading, setLoading] = useState(true)

    const state = useRef<RequestState<C, E>>({
        loading,
        error,
        content,
        setContent,
        setError,
        setLoading
    })

    state.current.loading = loading
    state.current.error = error
    state.current.content = content
    state.current.setContent = setContent
    state.current.setError = setError
    state.current.setLoading = setLoading

    useEffect(() => {
        if (!loading) return
        if (error) setError(false)
        getRequest()
            .then(setContent)
            .catch(setError)
            .then(() => setLoading(false))
    }, [loading])

    return state.current
}
const useRequestState = <C, E = any>(getRequest: () => Promise<C>) => useRequestStateBase<C | undefined, E>(getRequest, undefined)

export default useRequestState