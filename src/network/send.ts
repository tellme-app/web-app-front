interface RequestParams extends RequestInit {
    params?: Params
    data?: any
}

export interface ClientResponse<T> {
    data: T
    response: Response
}

type ResponseHandler = (response: Response) => void

type Params = { [key: string]: any }

type Method = 'GET' | 'POST' | 'PUT' | 'DELETE'

const handlers: ResponseHandler[] = []

export const addResponseHandler = (handler: ResponseHandler) =>
    handlers.push(handler)

export const send = async <T>(
    method: Method,
    url: string,
    options?: RequestParams
) =>
    new Promise<ClientResponse<T>>(async (resolve, reject) => {
        try {
            const preparedUrl = prepareUrl(url, options?.params)
            const reqBody = prepareBody(options?.data)

            const response = await fetch(preparedUrl, {
                method,
                body: reqBody,
                headers: reqBody ? {'Content-Type': 'application/json'} : undefined,
                ...options,
            })
            callHandlers(response)

            const data: T = await parseBody(response)
            if (!response.ok) reject({response, data})
            else resolve({response, data})
        } catch (e) {
            console.error(e)
            reject(e)
        }
    })

export const sendSimple = async <T>(
    method: Method,
    url: string,
    options?: RequestParams
) => send<T>(method, url, options).then(r => r.data)

const prepareUrl = (urlString: string, params?: Params) => {
    if (!params) return urlString
    const paramsString = Object.keys(params)
        .map((name) => `${name}=${params[name]}`)
        .join('&')
    return urlString + '?' + paramsString
}

const prepareBody = (data: any) => {
    return JSON.stringify(data)
}

const parseBody = async (response: Response) => {
    let contentType = response.headers.get('Content-Type')
    if (!contentType) return
    if (contentType.toLowerCase().indexOf('application/json') !== -1)
        return await response.json()
    return null
}

const callHandlers = (response: Response) => {
    handlers.forEach((h) => {
        try {
            h(response)
        } catch (e) {
            console.error(e)
        }
    })
}
