import {useEffect, useRef} from "react";

export interface DeferCallbackOptions {
    delay?: number
    unmountCall?: boolean
}

const useDeferCallback = (callback: (() => void), opt?: DeferCallbackOptions) => {
    const timerId = useRef<NodeJS.Timeout>()
    const callbackMut = useRef(callback)
    callbackMut.current = callback

    const restart = () => {
        if (timerId.current) clearTimeout(timerId.current)
        timerId.current = setTimeout(() => {
            timerId.current = undefined
            callbackMut.current()
        }, opt?.delay || 400)
    }

    useEffect(() => () => {
        if (timerId.current) {
            clearTimeout(timerId.current)
        }
        if (opt?.unmountCall !== undefined ? opt.unmountCall : true)
            callbackMut.current()
    }, [])

    return restart
}
export default useDeferCallback