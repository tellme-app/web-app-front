import {RefObject, useRef} from "react";
import useGlobalEvent from "./useGlobalEvent";

const useOutClick = <T extends HTMLElement>(
    onOutClick: (e: MouseEvent) => void,
    element?: T | null
): RefObject<T> => {
    const el = useRef<T>(null).current || element

    useGlobalEvent('click', (e) => {
        if (!el) return
        let parent = e.target as HTMLElement | null

        while (parent) {
            if (parent === el) return;
            parent = parent.parentElement
        }

        onOutClick(e)
    }, true)

    return {current: el || null}
}

export default useOutClick
