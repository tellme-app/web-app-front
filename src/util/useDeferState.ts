import {SetStateAction, useEffect, useRef, useState} from "react";

export interface DeferStateOptions<T> {
    delay?: number
    onStart?: (state: T) => void
    onEnd?: (state: T) => void
}

const useDeferState = <T>(defaultState: T | (() => T), opt?: DeferStateOptions<T>):
    [T, (action: SetStateAction<T>) => void] => {
    const [state, setState] = useState<T>(defaultState)
    const mutState = useRef<T>(state)
    const timerId = useRef<NodeJS.Timeout>()
    const set = (action: SetStateAction<T>) => {
        if (timerId.current) {
            clearTimeout(timerId.current)
            if (opt?.onStart) opt.onStart(mutState.current)
        }
        mutState.current = typeof action === 'function' ? (action as any)(mutState.current) : action
        timerId.current = setTimeout(() => {
            setState(mutState.current)
            timerId.current = undefined
            if (opt?.onEnd) opt.onEnd(mutState.current)
        }, opt?.delay !== undefined ? opt.delay : 300)
    }

    useEffect(() => () => {
        if (timerId.current) clearTimeout(timerId.current)
    }, [])

    return [state, set]
}
export default useDeferState