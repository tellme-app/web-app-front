import {useEffect, useRef} from "react";

const useGlobalEvent = <K extends keyof WindowEventMap>(
    type: K,
    listener: (ev: WindowEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions
) => {
    const callback = useRef(listener)
    callback.current = listener

    const ref = useRef({
        type,
        listener: (ev: WindowEventMap[K]) => callback.current(ev),
        options
    })

    useEffect(() => {
        window.addEventListener(ref.current.type, ref.current.listener, ref.current.options)
        return () => {
            window.removeEventListener(ref.current.type, ref.current.listener, ref.current.options)
        }
    }, [])
}
export default useGlobalEvent