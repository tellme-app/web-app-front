import React from "react";
import GrammarCard from "../grammar-card";
import {RouteComponentProps} from "react-router";
import TopHeader from "../TopHeader";

const WORDS = 3, CONTEXTS = 2

const Promo = ({match}: RouteComponentProps<{ phrase: string }>) => (
    <>
        <TopHeader/>
        <GrammarCard
            phrase={match.params.phrase.replace("_", " ")}
            words={WORDS}
            contexts={CONTEXTS}
        />
    </>
)

export default Promo;