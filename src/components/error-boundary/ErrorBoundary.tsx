import React, {ErrorInfo, ReactNode} from "react";

interface ErrorBoundaryProps {
    children?: ReactNode
}


export default class ErrorBoundary extends React.Component {
    state: { error?: Error } = {}

    constructor(props: ErrorBoundaryProps) {
        super(props);
    }

    static getDerivedStateFromError(error: any) {
        return {hasError: true};
    }

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        this.setState({error})
    }

    render() {
        if (this.state.error) {
            return <h1>Something went wrong...</h1>;
        }
        return this.props.children;
    }
}