import React from "react";
import TopHeader from "../TopHeader";
import PageContent from "../core/containers/page-content";
import Story from "../../models/story/Story";
import Text from "../core/text";
import styles from "./stories.module.scss"
import useStyle from "../../style/useStyle";
import classNames from "classnames";
import EllipseButton from "../core/buttons/ellipse-button";
import {MdBackspace, MdSave} from "react-icons/all";

interface StoryCreateProps {
  story: Story
  onChange: (story: Story) => void
  onSave: () => void
  onClose: () => void
  disabled: boolean
  header: string
}

const StoryCreate = ({disabled, ...props}: StoryCreateProps) => {
  const s = useStyle()

  return (
    <>
      <TopHeader/>
      <PageContent flex justifyContentCenter>
        <div>
          <Text variant='h5'>{props.header}</Text>
          <Text elm='div' variant='subTip'>Name</Text>
          <input
            disabled={disabled}
            className={classNames(s.size.mini, styles.inputs)}
            value={props.story.storyName}
            onChange={e => props.onChange({...props.story, storyName: e.target.value})}
          />
          <Text disabled={disabled} elm='div' variant='subTip'>Description</Text>
          <input
            disabled={disabled}
            className={classNames(s.size.mini, styles.inputs)}
            value={props.story.description}
            onChange={e => props.onChange({...props.story, description: e.target.value})}
          />
          <br/>
          <EllipseButton
            disabled={disabled}
            text='Back'
            icon={MdBackspace}
            m='x3'
            onClick={props.onClose}
          />
          <EllipseButton
            disabled={disabled}
            text='Save'
            preset='primary'
            icon={MdSave}
            iconPosition='right'
            m='x3'
            onClick={props.onSave}
          />
        </div>
      </PageContent>
    </>
  )
}

export default StoryCreate