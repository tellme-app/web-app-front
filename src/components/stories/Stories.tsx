import React, {useEffect, useState} from "react";
import TopHeader from "../TopHeader";
import PageContent from "../core/containers/page-content";
import {sendSimple} from "../../network/send";
import Story from "../../models/story/Story";
import Pageable from "../../models/Pageable";
import IconButtonsPaging from "../core/paging-actions/presets";
import Footer from "../core/footer";
import Button from "../core/buttons";
import {MdAdd, MdContentCopy, MdDelete} from "react-icons/all";
import Splitter from "../core/splitter";
import {useSnack} from "../basic/global-snack";
import Layout from "../core/containers/layout";
import Text from "../core/text";
import useStyle from "../../style/useStyle";
import IconButton from "../core/buttons/icon-button";
import styles from "./stories.module.scss"
import {Link, useHistory} from "react-router-dom";
import StoryCreate from "./StoryCreate";
import UnitsResponse from "../../models/story/UnitsResponse";
import CustomLink from "../core/custom-link";

const Stories = () => {
  const [params, setParams] = useState({page: 0, size: 25})
  const [loading, setLoading] = useState(true)
  const [stories, setStories] = useState<Pageable<Story>>()

  const [createdStory, setCreatedStory] = useState<Story>()
  const [copyId, setCopyId] = useState<number>()

  const snack = useSnack()
  const s = useStyle()
  const history = useHistory()

  const setCreateStory = (copedStory?: Story) => {
    setCopyId(copedStory?.storyId)
    setCreatedStory({...copedStory, storyId: undefined} || {})
  }

  const closeCreate = () => {
    setCopyId(undefined)
    setCreatedStory(undefined)
  }

  useEffect(() => {
    setLoading(true)
    sendSimple<Pageable<Story>>(
      'GET',
      `/builder-server/app/story`,
      {params}
    )
      .then(setStories)
      .catch((e) => snack.show("Error: " + String(e?.data?.message || e?.data), "error"))
      .then(() => setLoading(false))
  }, [params])

  const save = () => {
    setLoading(true)
    sendSimple<Story>('POST', `/builder-server/app/story`, {data: createdStory})
      .then(story => {
        const idToCopy = copyId
        if (idToCopy !== undefined)
          return sendSimple<UnitsResponse>('GET', `/builder-server/app/story/${idToCopy}/units`)
            .then(data => sendSimple<UnitsResponse>(
              'PUT',
              `/builder-server/app/story/${story.storyId}/units`,
              {data}
            )).then(() => story)
        return story
      })
      .then((story) => history.push(`/story/builder/${story.storyId}`))
      .catch((e) => snack.show("Error: " + String(e?.data?.message || e?.data), "error"))
      .then(() => setLoading(false))
  }

  const removeStory = (storyId: number) => {
    setLoading(true)
    sendSimple<Pageable<Story>>(
      'DELETE',
      `/builder-server/app/story/${storyId}`
    )
      .then(() => setParams({...params}))
      .then((e) => snack.show('Удалено'))
      .catch((e) => snack.show("Error: " + String(e?.data?.message || e?.data), "error"))
      .then(() => setLoading(false))
  }

  if (createdStory) return (
    <StoryCreate
      disabled={loading}
      story={createdStory}
      onChange={setCreatedStory}
      onClose={closeCreate}
      onSave={save}
      header={copyId !== undefined ? 'Copy story' : 'Create story'}
    />
  )

  return (
    <>
      <TopHeader/>
      <PageContent>
        <Layout flex alignItemsCenter>
          <Text elm='div' disableBottomIdent variant='h5' color='secondaryDark'>Stories</Text>
          <Button
            flat
            icon={MdAdd}
            text='Create'
            preset='accident'
            mTop='x3'
            mLeft='auto'
            onClick={() => setCreateStory()}
          />
        </Layout>
        <Splitter/>
        {!stories?.empty && stories?.content.map((story, i) => (
          <Layout flex key={i} boxShadow mTop='x2' mBottom='x2' p='x2' bkg='main'>
            <div className={s.mRight.auto}>
              <CustomLink underline className={s.color.inherit} to={`/story/builder/${story.storyId}`}>
                <Text variant='subH2'>{story?.storyName}</Text>
              </CustomLink>
              {story.description && <Text variant='text2'>{story.description}</Text>}
            </div>
            <div className={styles.storyButtons}>
              <IconButton disabled={loading} icon={MdContentCopy} onClick={() => setCreateStory(story)}/>
              <IconButton disabled={loading} icon={MdDelete} onClick={() => removeStory(story.storyId || 0)}/>
            </div>
          </Layout>
        ))}
        <Footer className={s.bkg.main}>
          <IconButtonsPaging
            disabled={loading}
            pageNumber={params.page}
            viewedNumber={params.page + 1}
            totalPages={stories?.totalPages}
            onPageChange={(page) => setParams({...params, page})}
          />
        </Footer>
      </PageContent>
    </>
  )
}

export default Stories