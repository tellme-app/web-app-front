import React, {useContext, useState} from "react";
import styles from "./signin.module.scss";
import useStyle from "../../style/useStyle";
import Text from "../core/text";
import classNames from "classnames";
import EllipseButton from "../core/buttons/ellipse-button";
import TextField from "./TextField";
import Layout from "../core/containers/layout";
import {FaFacebook, FaGoogle} from "react-icons/all";
import Splitter from "../core/splitter";
import RegistrationData from "../../models/auth/RegistrationData";
import {useSnack} from "../basic/global-snack";
import {send, sendSimple} from "../../network/send";
import {AuthContext} from "../../auth";
import AuthData from "../../models/auth/AuthData";
import AuthContextData from "../../auth/AuthContextData";
import {Link, useHistory} from "react-router-dom";
import {facebookAuthUrl, googleAuthUrl} from "./constants";
import CustomLink from "../core/custom-link";

const defaultData: RegistrationData = {
    name: "",
    fio: "",
    email: "",
    password: "",
    confirmPassword: ""
}

const SignUpForm = () => {
    const s = useStyle();
    const [_, setAuthCtx] = useContext(AuthContext);
    const snack = useSnack();
    const history = useHistory();
    const [fetching, setFetching] = useState(false)
    const [data, setData] = useState(defaultData)

    const onChange = (name: keyof RegistrationData) => (e: React.ChangeEvent<HTMLInputElement>) => {
        if (name !== 'fio' && e.target.value.indexOf(" ") !== -1) return
        setData({...data, [name]: e.target.value})
    }

    const disableSignUp = Object.keys(data).some((k) => !data[k as keyof RegistrationData])

    const onSignUp = () => {
        setFetching(true)
        send('POST', '/auth/register', {data})
            .then(() => send('POST', '/auth/login', {
                data: {
                    username: data.name,
                    password: data.password
                }
            }))
            .then(() => sendSimple<AuthData>('GET', '/auth'))
            .then(authData => setAuthCtx(new AuthContextData(true, authData)))
            .then(() => history.push('/'))
            .catch(e => snack.show(e?.data?.message || "Error", 'error'))
            .then(() => setFetching(false))
    }

    return (
        <div>
            <Text
                disableTopIdent
                variant='h1'
                className={classNames(styles.title, styles.centerAlign)}
            >
                Sign Up With
            </Text>
            <Layout flex className={styles.buttonsContainer}>
                <a href={googleAuthUrl}>
                    <EllipseButton
                        className={styles.socialButtons}
                        m='x2'
                        size='x-small'
                        icon={FaGoogle}
                        iconPosition='inside'
                        disabled={fetching}
                    />
                </a>
                <a href={facebookAuthUrl}>
                    <EllipseButton
                        className={styles.socialButtons}
                        m='x2'
                        size='x-small'
                        icon={FaFacebook}
                        iconPosition='inside'
                        disabled={fetching}
                    />
                </a>
            </Layout>
            <Layout mTop='x8' flex alignItemsCenter>
                <Text>OR</Text>
                <Splitter mTop='x0' mLeft="x2" style={{width: "100%"}}/>
            </Layout>
            <form onSubmit={e => {
                e.preventDefault()
                onSignUp()
            }}>
                <TextField
                    required
                    value={data.name}
                    onChange={onChange("name")}
                    placeholder="login"
                    className={classNames(s.mTop.x8, s.size["x-small"])}
                    disabled={fetching}
                />
                <TextField
                    required
                    value={data.fio}
                    onChange={onChange("fio")}
                    placeholder="full name"
                    className={classNames(s.mTop.x8, s.size["x-small"])}
                    disabled={fetching}
                />
                <TextField
                    required
                    value={data.email}
                    onChange={onChange("email")}
                    placeholder="email"
                    type="email"
                    className={classNames(s.mTop.x8, s.size["x-small"])}
                    disabled={fetching}
                />
                <TextField
                    required
                    minLength={6}
                    value={data.password}
                    onChange={onChange("password")}
                    placeholder="password"
                    type="password"
                    className={classNames(s.mTop.x8, s.size["x-small"])}
                    disabled={fetching}
                />
                <TextField
                    minLength={6}
                    required
                    value={data.confirmPassword}
                    onChange={onChange("confirmPassword")}
                    placeholder="confirm password"
                    type="password"
                    className={classNames(s.mTop.x8, s.size["x-small"])}
                    disabled={fetching}
                />
                <Layout mTop='x11' flex className={styles.buttonsContainer}>
                    <CustomLink to='/login' className={classNames(s.m.x2, styles.loginButtonsWrp)}>
                        <EllipseButton
                            type={"button"}
                            className={styles.loginButtons}
                            size='x-small'
                            text='Login'
                            disabled={fetching}
                        />
                    </CustomLink>
                    <EllipseButton
                        className={styles.loginButtons}
                        type='submit'
                        preset='primary'
                        color='inverse'
                        m='x2' size='x-small' text='Sign Up'
                        disabled={fetching || disableSignUp}
                    />
                </Layout>
            </form>
        </div>
    )
}

export default SignUpForm