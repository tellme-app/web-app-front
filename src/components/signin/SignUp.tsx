import React from "react";
import styles from "./signin.module.scss";
import classNames from "classnames";
import AuthFormContainer from "./AuthFormContainer";
import SignUpForm from "./SignUpForm";


const SignUp = () => (
    <div className={classNames(styles.root, styles.signUpBackgroundImage, styles.disableImage)}>
        <AuthFormContainer className={styles.authFormContainerRight}>
            <SignUpForm/>
        </AuthFormContainer>
    </div>
)

export default SignUp