import React, {InputHTMLAttributes} from "react";
import useStyle from "../../style/useStyle";
import classNames from "classnames";
import style from "./signin.module.scss"

const TextField = (props: InputHTMLAttributes<HTMLInputElement>) => {
    const s = useStyle();
    return (
        <input
            {...props}
            className={classNames(
                style.input,
                s.bkg.primaryLight,
                s.bkgDisabled.primaryLight,
                s.p.x3,
                s.pLeft.x4,
                s.pRight.x4,
                s.size.mini,
                props.className
            )}
        />
    )
}

export default TextField