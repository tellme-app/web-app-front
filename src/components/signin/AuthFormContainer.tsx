import React from "react";
import styles from "./signin.module.scss";
import classNames from "classnames";

const AuthFormContainer = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) => {
    return (
        <div {...props} className={classNames(styles.authFormContainer, props.className)} >
            <span className={styles.authFormContainerWrapper}>{props.children}</span>
        </div>
    )
}

export default AuthFormContainer