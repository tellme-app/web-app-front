import React, {useContext, useState} from "react";
import styles from "./signin.module.scss";
import useStyle from "../../style/useStyle";
import Text from "../core/text";
import classNames from "classnames";
import EllipseButton from "../core/buttons/ellipse-button";
import TextField from "./TextField";
import Layout from "../core/containers/layout";
import {FaFacebook, FaGoogle} from "react-icons/all";
import Credentials from "../../models/auth/Credentials";
import {facebookAuthUrl, googleAuthUrl} from "./constants";
import {send, sendSimple} from "../../network/send";
import AuthData from "../../models/auth/AuthData";
import AuthContextData from "../../auth/AuthContextData";
import {AuthContext} from "../../auth";
import {useSnack} from "../basic/global-snack";
import {Link, useHistory} from "react-router-dom";
import CustomLink from "../core/custom-link";


const SignInForm = () => {
    const s = useStyle();
    const [data, setData] = useState<Credentials>({username: "", password: ""})
    const [_, setAuthCtx] = useContext(AuthContext);
    const snack = useSnack();
    const history = useHistory();
    const [fetching, setFetching] = useState(false)
    const disableLogin = Object.keys(data).some((k) => !data[k as keyof Credentials])

    const onLogin = () => {
        setFetching(true)
        send('POST', '/auth/login', {data})
            .then(() => sendSimple<AuthData>('GET', '/auth'))
            .then(authData => setAuthCtx(new AuthContextData(true, authData)))
            .then(() => history.push('/'))
            .catch(e => snack.show(e?.data?.message || "Error", 'error'))
            .then(() => setFetching(false))
    }

    return (
        <div>
            <Text disableTopIdent variant='h1' className={styles.title}>Welcome Back</Text>
            <form onSubmit={e => {
                e.preventDefault()
                onLogin()
            }}>
                <TextField
                    onChange={e => setData({...data, username: e.target.value})}
                    placeholder="login"
                    className={classNames(s.mTop.x8, s.size["x-small"])}
                    disabled={fetching}
                />
                <br/>
                <TextField
                    onChange={e => setData({...data, password: e.target.value})}
                    placeholder="pass"
                    type="password"
                    className={classNames(s.mTop.x8, s.size["x-small"])}
                    disabled={fetching}
                />
                <Layout mTop='x11' flex className={styles.buttonsContainer}>
                    <EllipseButton
                        type='submit'
                        className={styles.loginButtons}
                        m='x2'
                        size='x-small'
                        preset='primary'
                        color='inverse'
                        disabled={fetching || disableLogin}
                        text='Login'
                    />
                    <CustomLink to='/signup' className={classNames(s.m.x2, styles.loginButtonsWrp)}>
                        <EllipseButton
                            type='button'
                            disabled={fetching}
                            className={styles.loginButtons}
                            size='x-small' text='Sign Up'
                        />
                    </CustomLink>
                </Layout>
            </form>
            <Layout mTop='x11' flex className={styles.buttonsContainer}>
                <a href={googleAuthUrl}>
                    <EllipseButton
                        disabled={fetching}
                        className={styles.socialButtons}
                        m='x2'
                        size='x-small'
                        icon={FaGoogle}
                        iconPosition='inside'
                    />
                </a>
                <a href={facebookAuthUrl}>
                    <EllipseButton
                        disabled={fetching}
                        className={styles.socialButtons}
                        m='x2'
                        size='x-small'
                        icon={FaFacebook}
                        iconPosition='inside'
                    />
                </a>
            </Layout>
        </div>
    )
}

export default SignInForm