import React from "react";
import styles from "./signin.module.scss";
import useStyle from "../../style/useStyle";
import classNames from "classnames";
import AuthFormContainer from "./AuthFormContainer";
import SignInForm from "./SignInForm";
import TextLogo from "./TextLogo";

const SignIn = () => {
    const s = useStyle();
    return (
        <div className={classNames(styles.root, styles.signInBackgroundImage, styles.disableImage)}>
            <TextLogo className={classNames(s.mTop.x8, s.pLeft.x8)}/>
            <AuthFormContainer>
                <SignInForm/>
            </AuthFormContainer>
        </div>
    )
}

export default SignIn