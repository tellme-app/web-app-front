import React from "react";
import {RouteComponentProps} from "react-router";
import { Switch, Route, Redirect } from "react-router-dom";
import ThemeDemo from "./ThemeDemo";
import DemoCorePopup from "./DemoCorePopup";
import CoreDemo from "./CoreDemo";
import ComponentDemo from "./ComponentDemo";
import DemoCoreInputs from "./DemoCoreInputs";

export default ({match}: RouteComponentProps<{ phrase: string }>) => (
    <Switch>
        <Route path={match.path + "/theme"} component={ThemeDemo}/>
        <Route path={match.path + "/core/popup"} component={DemoCorePopup}/>
        <Route path={match.path + "/core/inputs"} component={DemoCoreInputs}/>
        <Route path={match.path + "/core/"} component={CoreDemo}/>
        <Route path={match.path + "/components/"} component={ComponentDemo}/>
        <Redirect to={match.path + "/core/"}/>
    </Switch>
);