import {useState} from "react";

export const useBool = () => {
  const [bools, setBools] = useState<{ [key: string]: boolean }>({})
  const set = (key: string, val: boolean) => setBools({...bools, [key]: val})
  return {
    set,
    enable: (key: string) => set(key, true),
    disable: (key: string) => set(key, false),
    reverse: (key: string) => set(key, !bools[key]),
    get: (key: string) => bools[key]
  }
}