import React, {useState} from "react";
import DemoPart from "./DemoPart";
import PageContent from "../core/containers/page-content";
import PartHeader from "./DemoHeader";
import Layout from "../core/containers/layout";
import TopHeader from "../TopHeader";
import BaseInput from "../core/input";
import BaseTextArea from "../core/input/BaseTextArea";

const DemoCoreInputs = () => {
    const [state, setState] = useState({
        baseInput: 'base input text',
        baseTextArea: 'base text area text',
    })

    const onInputChange = (name: string) => (e: React.ChangeEvent<'input'>) => setState({...state, [name]: (e.target as any).value})
    const onAreaChange = (name: string) => (e: React.ChangeEvent<'textarea'>) => setState({...state, [name]: (e.target as any).value})
    return (
        <div>
            <TopHeader/>
            <PageContent maxWidth={1500} p='x5'>
                <DemoPart>
                    <PartHeader>BaseInput</PartHeader>
                    <Layout flex justifyContentCenter>
                        <BaseInput onChange={onInputChange('baseInput')} value={state.baseInput}/>
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <PartHeader>BaseInput</PartHeader>
                    <Layout flex justifyContentCenter>
                        <BaseTextArea onChange={onAreaChange('baseTextArea')} value={state.baseTextArea}/>
                    </Layout>
                </DemoPart>
            </PageContent>
        </div>
    )
}

export default DemoCoreInputs;