import React from "react";
import Text from "../core/text";
import {MdAlarmOff} from "react-icons/all";
import EllipseButton from "../core/buttons/ellipse-button";
import {ButtonProps} from "../core/buttons/Button";
import Layout from "../core/containers/layout";
import useStyle from "../../style/useStyle";
import style from "./demo.module.scss"

interface DemoButtonsProps {
    buttonProps: ButtonProps
    setButtonProps: (props: ButtonProps) => void
}
const DemoButtonsOpt = ({buttonProps, setButtonProps}: DemoButtonsProps) => (
    <Layout className={style.sticky} bkg="secondaryLight" m="x11">
        <EllipseButton
            size="mini"
            text="Outlined"
            m="x2"
            bkg={buttonProps.outlined ? "primary" : "main"}
            onClick={() => setButtonProps({...buttonProps, outlined: !buttonProps.outlined})}
        />
        <EllipseButton
            size="mini"
            text="Border"
            m="x2"
            bkg={buttonProps.border ? "secondary" : "main"}
            onClick={() => setButtonProps({...buttonProps, border: !buttonProps.border})}
        />
        <EllipseButton
            size="mini"
            text="Flat"
            m="x2"
            bkg={buttonProps.flat ? "primary" : "main"}
            onClick={() => setButtonProps({...buttonProps, flat: !buttonProps.flat})}
        />
        <EllipseButton
            size="mini"
            text="Hovers"
            m="x2"
            bkg={buttonProps.hovers ? "primary" : "main"}
            onClick={() => setButtonProps({...buttonProps, hovers: !buttonProps.hovers})}
        />
        <EllipseButton
            size="mini"
            text="Disabled"
            m="x2"
            bkg={buttonProps.disabled ? "error" : "main"}
            onClick={() => setButtonProps({...buttonProps, disabled: !buttonProps.disabled})}
        />
        <EllipseButton
            size="mini"
            text="Icon"
            m="x2"
            bkg={buttonProps.icon ? "primary" : "main"}
            onClick={() => setButtonProps({...buttonProps, icon: buttonProps.icon ? undefined : MdAlarmOff})}
        />
        <EllipseButton
            size="mini"
            text={"icon pos: "}
            m="x2"
            onClick={() => setButtonProps({
                ...buttonProps,
                iconPosition: buttonProps.iconPosition === "left" ? "right" : "left"
            })}
        >
            <Text className={useStyle().color.error} variant="action"> {buttonProps.iconPosition}</Text>
        </EllipseButton>
    </Layout>
)
export default DemoButtonsOpt;