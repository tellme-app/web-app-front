import React from "react";
import style from "./demo.module.scss"
import classNames from "classnames";
import Layout from "../core/containers/layout";
import {StyleProps} from "../props";
import {LayoutProps} from "../core/containers/layout/Layout";

const DemoPart = (props: LayoutProps & StyleProps) => (
    <Layout {...props} className={classNames(style.part, props.className)}>
        {props.children}
        <div className={style.partSplitter}/>
    </Layout>
)

export default DemoPart;