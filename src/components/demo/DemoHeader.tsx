import React, {PropsWithChildren} from "react";
import DeprecatedText from "../core/deprecated-text";
import {DeprecatedTextProps} from "../core/deprecated-text/deprecatedText";
import style from "./demo.module.scss"
import classNames from "classnames";

const PartHeader = (props: DeprecatedTextProps & PropsWithChildren<any>) => (
    <>
        <DeprecatedText MainElement='div' className={classNames(style.partHeader, props.className)}
                        variant={"header"} {...props}/>
        <p/>
    </>
)

export default PartHeader;