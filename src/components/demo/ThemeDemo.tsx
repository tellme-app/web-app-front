import React from "react";
import useStyle from "../../style/useStyle";
import classNames from "classnames";
import Layout from "../core/containers/layout";
import Text from "../core/text";
import PageContent from "../core/containers/page-content";
import StyleColors from "../../style/StyleColors";
import TopHeader from "../TopHeader";

const getKeys = (s: StyleColors) => (Object.keys(s) as (keyof StyleColors)[])

const ThemeDemo = () => {
    const s = useStyle();
    const cb = getKeys(s.bkg).flatMap(background => getKeys(s.color).map(color => ({color, background})))

    return (
        <>
            <TopHeader/>
            <PageContent>
                {cb.map(({color, background}, i) => (
                    <Layout flex boxShadow m='x2' mTop='x4' mBottom='x4' p='x6' bkg={background}>
                        <Text className={classNames(s.color[color])}>
                            {i + 1}. Эй, жлоб! Где туз? Прячь юных съёмщиц в шкаф. (текст {color}/ фон {background})
                        </Text>
                    </Layout>
                ))}
            </PageContent>
        </>
    )
}

export default ThemeDemo;
