import React, {useState} from "react";
import DemoPart from "./DemoPart";
import PageContent from "../core/containers/page-content";
import PartHeader from "./DemoHeader";
import Button from "../core/buttons";
import Layout from "../core/containers/layout";
import TopHeader from "../TopHeader";
import Popup from "../core/popup";
import StyledPopup from "../core/popup/StyledPopup";
import Tooltip from "../core/tooltip";

const DemoCorePopup = () => {
    const [anchorOpen, setAnchorOpen] = useState(false)
    return (
        <div>
            <TopHeader/>
            <PageContent maxWidth={1500} p='x5'>
                <DemoPart>
                    <PartHeader>Popup</PartHeader>
                    <Layout flex justifyContentCenter>
                        <Popup onClose={() => setAnchorOpen(false)} content='Supertitle' show={anchorOpen}>
                            <Button
                                m='x2'
                                preset='primaryDark'
                                text='controlled'
                                color='inverse'
                                onClick={() => setAnchorOpen(!anchorOpen)}
                            />
                        </Popup>
                        <Popup trigger='hover' position='top' content='Hover popup'>
                            <Button
                                m='x2'
                                preset='primary'
                                text='hover'
                                color='inverse'
                            />
                        </Popup>
                        <Popup trigger='click' position='right' content='Click popup'>
                            <Button
                                m='x2'
                                preset='primaryLight'
                                text='click'
                            />
                        </Popup>
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <PartHeader>Styled popup</PartHeader>
                    <Layout flex justifyContentCenter>
                        <StyledPopup
                            popupProps={{trigger: 'hover', position: 'top'}}
                            content='Primary'
                            p='x2' bkg='primary'
                        >
                            <Button
                                m='x2'
                                preset='secondary'
                                text='Primary'
                                color='inverse'
                            />
                        </StyledPopup>
                        <StyledPopup
                            popupProps={{trigger: 'hover', position: 'bottom'}}
                            content='Inverse'
                            p='x2'
                            bkg='inverse'
                            color='inverse'
                        >
                            <Button
                                m='x2'
                                preset='secondary'
                                text='Inverse'
                                color='inverse'
                            />
                        </StyledPopup>
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <PartHeader>Tooltip</PartHeader>
                    <Layout flex justifyContentCenter>
                        <Tooltip text='Tooltip text'>
                            <Button
                                m='x2'
                                preset='primaryLight'
                                text='One'
                            />
                        </Tooltip>
                    </Layout>
                </DemoPart>
            </PageContent>
        </div>
    )
}

export default DemoCorePopup;