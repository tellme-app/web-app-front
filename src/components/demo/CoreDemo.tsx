import React, {ElementType, PropsWithChildren, useState} from "react";
import DemoPart from "./DemoPart";
import Text from "../core/text";
import Alert from "../core/alert";
import useStyle from "../../style/useStyle";
import PageContent from "../core/containers/page-content";
import ExamplePhrase from "../core/examplePhrase";
import PartHeader from "./DemoHeader";
import {AlertSnackbar} from "../basic/snackbar";
import {useSnack} from "../basic/global-snack";
import FilledLoading from "../core/states/FilledLoading";
import LocalLoading from "../core/states/LocalLoading";
import LocalError from "../core/states/LocalError";
import {Transparency} from "../core/animation";
import WordButton from "../core/wordButton";
import {Md3DRotation, MdAccountBox, MdAcUnit, MdAlarm, MdAssessment, MdCake, MdHearing} from "react-icons/all";
import Button, {BaseButton, ButtonIcon} from "../core/buttons";
import RoundButton from "../core/buttons/round-button";
import EllipseButton from "../core/buttons/ellipse-button";
import Icon from "../core/icon";
import {useBool} from "./useBool";
import {Collapse} from "../core/animation/presets/Collapse";
import {Popping} from "../core/animation/presets/Popping";
import {StyleSizes} from "../../style/StyleSizes";
import {ButtonProps} from "../core/buttons/Button";
import DemoButtonsOpt from "./DemoButtonsOpt";
import Layout from "../core/containers/layout";
import Chip from "../core/buttons/chip";
import StyleColors from "../../style/StyleColors";
import IconButton from "../core/buttons/icon-button";
import Header from "../header";

const buttonDefaultProps: ButtonProps = {
    outlined: false,
    flat: false,
    hovers: true,
    iconPosition: "left",
}

const CoreDemo = () => {
    const s = useStyle();
    const b = useBool()
    const snack = useSnack();

    const sizes = (Object.keys((s.size)) as (keyof StyleSizes)[])
        .filter(s => !["plusSize", "giant", "x-large"].includes(s));

    const presets = (Object.keys((s.color)) as (keyof StyleColors)[]).filter(p => p !== "inherit")

    const [buttonProps, setButtonProps] = useState(buttonDefaultProps)
    const [roundButtonProps, setRoundButtonProps] = useState<ButtonProps>({...buttonDefaultProps, icon: MdAlarm})
    const [ellipseButtonProps, setEllipseButtonProps] = useState(buttonDefaultProps)
    const [chipProps, setChipProps] = useState<ButtonProps>({
        ...buttonDefaultProps,
        hovers: false,
        flat: true,
        icon: MdAcUnit
    })
    const [iconButtonProps, setIconButtonProps] = useState<ButtonProps>({
        ...buttonDefaultProps,
        flat: true,
        outlined: true,
        icon: MdCake
    })

    const buttonsSet = (Element: ElementType<ButtonProps>, props: ButtonProps) => presets
        .flatMap(p => [
            <Text>{p}</Text>, <p/>,
            sizes.map(size => (
                <Element
                    preset={p}
                    size={size}
                    m='x2'
                    text={size}
                    {...(((p === "inverse" || p === "dark") && !props.outlined) ? {color: "inverse"} : undefined)}
                    {...props}
                />
            )),
            <p/>
        ])

    return (
        <>
            <Header/>
            <PageContent maxWidth={1500} className={s.p.x5}>
                <DemoPart>
                    <PartHeader>Alert</PartHeader>
                    <div style={{maxWidth: 1000}}>
                        <Alert className={s.mTop.x2} variant={"none"}>Пустое сообщение</Alert>
                        <Alert className={s.mTop.x2} variant={"info"}>Информационная фигня</Alert>
                        <Alert className={s.mTop.x2} variant={"success"}>Хорошее сообщение</Alert>
                        <Alert className={s.mTop.x2} variant={"warn"}>Предупреждаю</Alert>
                        <Alert className={s.mTop.x2} variant={"error"}>Все очень плохо</Alert>
                    </div>
                </DemoPart>
                <DemoPart>
                    <PartHeader>Text</PartHeader>
                    <Text variant={"h1"}>Заголовок h1</Text>
                    <Text variant={"h2"}>Заголовок h2</Text>
                    <Text variant={"h3"}>Заголовок h3</Text>
                    <Text variant={"h4"}>Заголовок h4</Text>
                    <Text variant={"h5"}>Заголовок h5</Text>
                    <Text variant={"h6"}>Заголовок h6</Text>
                    <Text variant={"subH1"}>Подзаголовок subH1</Text>
                    <Text variant={"subH2"}>Подзаголовок subH2</Text>
                    <Text variant={"text1"}>
                        <b>text1</b>
                        Образование микрорайона связано со строительством в 1929 году станции Свердловск-Сортировочный
                        (ныне
                        — Екатеринбург-Сортировочный). В 1920-е вокруг станции на месте леса строились дома барачного
                        типа.
                        В 1936 5-этажными домами застраивается ул. Крупской, с 1946 — 2-3-этажными домами ул.
                        Техническая,
                        Коуровская, пер. Клубный. 1 мая 1949 открыта автономная трамвайная линия (с 1958 — соединена с
                        городом). С 1960-х — массовое жилищное строительство 5-этажными домами в «хрущевском» стиле, с
                        1976
                        - 9-этажными домами. С конца 1980-х, после взрыва на станции Свердловск-Сортировочный построен
                        ряд
                        9—16-этажных зданий[1].
                    </Text>
                    <Text variant={"text2"}>
                        <b>text2</b>
                        В 1920—1930-е здесь располагался посёлок Красная Звезда, возникший вокруг
                        одноимённой ж/д станции и состоявший из 1-этажных деревянных домов. С 1975 года началось
                        освоение
                        микрорайона, выстроены жилые кварталы 9—16-этажных домов типовых серий I-468 (9-этажный,
                        «пентагон»,
                        первым в 1976 сдан дом по ул. Пехотинцев, 5) и 141 (16-эт.), школы, дошкольные учреждения. С
                        1994 -
                        крупнейший вещевой рынок Таганский Ряд, который в 1980-е годы располагался в посёлке Шувакиш[2].
                    </Text>
                    <Text variant={"action"}>Экшен action</Text><p/>
                    <Text variant={"tip"}>Подсказка tip</Text><p/>
                    <Text variant={"subTip"}>Подводные советы subTip</Text>
                </DemoPart>
                <DemoPart>
                    <PartHeader>BaseButton</PartHeader><p/>
                    <BaseButton m='x4' bkg='accident' p='x5' size='giant' color='primaryLight'>Кнопка</BaseButton>
                </DemoPart>
                <DemoPart>
                    <Layout style={{textAlign: "center"}}>
                        <PartHeader>Button</PartHeader><p/>
                        <DemoButtonsOpt buttonProps={buttonProps} setButtonProps={setButtonProps}/>
                        {buttonsSet(Button, buttonProps)}
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <Layout style={{textAlign: "center"}}>
                        <PartHeader>RoundButton</PartHeader><p/>
                        <DemoButtonsOpt buttonProps={roundButtonProps} setButtonProps={setRoundButtonProps}/>
                        {buttonsSet(
                            RoundButton,
                            {...roundButtonProps, text: roundButtonProps.icon ? undefined : "10%"}
                        )}
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <Layout style={{textAlign: "center"}}>
                        <PartHeader>EllipseButton</PartHeader><p/>
                        <DemoButtonsOpt buttonProps={ellipseButtonProps} setButtonProps={setEllipseButtonProps}/>
                        {buttonsSet(EllipseButton, ellipseButtonProps)}
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <Layout style={{textAlign: "center"}}>
                        <PartHeader>IconButton</PartHeader><p/>
                        <DemoButtonsOpt buttonProps={iconButtonProps} setButtonProps={setIconButtonProps}/>
                        {buttonsSet(IconButton, iconButtonProps)}
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <Layout style={{textAlign: "center"}}>
                        <PartHeader>Chip</PartHeader><p/>
                        <DemoButtonsOpt buttonProps={chipProps} setButtonProps={setChipProps}/>
                        {buttonsSet(Chip, chipProps)}
                        <Chip icon={MdAlarm} m="x1" text="Chipi" preset="light" color="main"/>
                        <Chip icon={MdAccountBox} outlined m="x1" text="chipi" color="primaryDark"/>
                        <Chip outlined m="x1" color="primaryDark" border>
                            <ButtonIcon icon={MdAccountBox} color="accident"/>
                            <Text color="error" variant="action">customChipi</Text>
                        </Chip>
                        <Chip icon={MdAssessment} m="x1" text="Deil!" preset="secondaryLight"
                              color="secondaryDark"/>
                    </Layout>
                </DemoPart>
                <DemoPart>
                    <PartHeader>WordButton</PartHeader>
                    <WordButton word={"word"}/>
                    <WordButton word={"progress"} progress={50}/>
                </DemoPart>
                <DemoPart>
                    <PartHeader>Icon</PartHeader><p/>
                    <Icon icon={Md3DRotation} size="micro"/>
                    <Icon icon={Md3DRotation} size="mini"/>
                    <Icon icon={Md3DRotation} size="x-small"/>
                    <Icon icon={Md3DRotation} size="small"/>
                    <Icon icon={Md3DRotation} size="medium"/>
                    <Icon icon={Md3DRotation} size="large"/>
                    <Icon icon={Md3DRotation} size="x-large" color="accident"/>
                    <Icon icon={Md3DRotation} size="giant"/>
                    <Icon icon={Md3DRotation} size="plusSize"/>
                    <Icon icon={MdHearing} color="error" size="x-large" scale="1.2"/>
                </DemoPart>
                <DemoPart>
                    <PartHeader>ExamplePhrase</PartHeader><p/>
                    <ExamplePhrase className={s.p.x2} variant="positive">positive</ExamplePhrase>
                    <ExamplePhrase className={s.p.x2} variant="negative">negative</ExamplePhrase>
                    <ExamplePhrase className={s.p.x2} variant="question">question</ExamplePhrase>
                    <ExamplePhrase className={s.p.x2} variant="neutral">neutral</ExamplePhrase>
                </DemoPart>
                <DemoPart>
                    <PartHeader>SnackBar</PartHeader><p/>
                    <AlertSnackbar
                        onClose={() => b.disable("AlertSnack")}
                        autoClose vertical='top' variant='error'
                        open={b.get("AlertSnack")} text='Очень важно!'
                    />
                    <EllipseButton className={s.m.x2} onClick={() => b.reverse("AlertSnack")}>
                        <Text variant='tip'>Show AlertSnackBar</Text>
                    </EllipseButton>
                    <p/>
                    <EllipseButton className={s.m.x2} onClick={() => snack.show("Message: error", "error")}>
                        <Text variant='tip'>Alert SnackBar hook error</Text>
                    </EllipseButton>
                    <p/>
                    <EllipseButton className={s.m.x2} onClick={() => snack.show("Message: info", "info")}>
                        <Text variant='tip'>Alert SnackBar hook info</Text>
                    </EllipseButton>
                    <p/>
                    <EllipseButton className={s.m.x2} onClick={() => snack.show("Message: success")}>
                        <Text variant='tip'>Alert SnackBar hook success</Text>
                    </EllipseButton>
                </DemoPart>
                <DemoPart>
                    <PartHeader>States</PartHeader><p/>
                    <Text className={s.m.x2} variant='tip'>FilledLoading</Text>
                    <FilledLoading style={{height: 100, minHeight: 0}}/>
                    <FilledLoading style={{height: 100, minHeight: 0}} message="very important message"/>
                    <Text className={s.m.x2} variant='tip'>LocalLoading</Text>
                    <LocalLoading/>
                    <LocalLoading message="very important message"/>
                    <Text className={s.m.x2} variant='tip'>LocalError</Text>
                    <LocalError message={"Не сделано еще"}/>
                    <Text className={s.m.x2} variant='tip'>Empty</Text>
                    <LocalError message={"Тоже не сделано еще, но может и не надо"}/>
                </DemoPart>
                <DemoPart>
                    <PartHeader>Animations</PartHeader>
                    <EllipseButton preset="primary" onClick={() => b.reverse("Collapse")}
                                   className={s.m.x2}>Collapse</EllipseButton>
                    <Collapse open={b.get("Collapse")}><StupidContainer/></Collapse>
                    <EllipseButton preset="secondary" onClick={() => b.reverse("Transparency")}
                                   className={s.m.x2}>Transparency</EllipseButton>
                    <Transparency unmountAfterClose open={b.get("Transparency")}><StupidContainer/></Transparency>
                    <EllipseButton preset="accident" onClick={() => b.reverse("Popping")}
                                   className={s.m.x2}>Popping</EllipseButton>
                    <Popping unmountAfterClose open={b.get("Popping")}><StupidContainer/></Popping>
                </DemoPart>
            </PageContent>
        </>
    )
}

const StupidContainer = ({children = "Show me the way to the next whiskey bar..."}: PropsWithChildren<any>) => (
    <div style={{
        width: 400,
        height: 200,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "grey"
    }}
    >
        {children}
    </div>
)

export default CoreDemo;