import React from "react";
import PageContent from "../core/containers/page-content";
import PartHeader from "./DemoHeader";
import Tens from "../rules/english/Tens";
import FakePresentSimple from "./fake/FakePresentSimple";
import TopHeader from "../TopHeader";

const ComponentDemo = () => {
    return (
        <>
            <TopHeader/>
            <PageContent>
                <PartHeader>Tense</PartHeader>
                <Tens m='x4' data={FakePresentSimple}/>
            </PageContent>
        </>
    )
}

export default ComponentDemo;