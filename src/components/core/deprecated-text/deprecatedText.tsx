import React, {ElementType, FunctionComponent} from "react";
import s from './text.module.scss'
import classNames from "classnames";

export interface DeprecatedTextProps {
    MainElement?: ElementType,
    variant?: "regular" | "header" | "tip" | "subTip" | "subHeader" | "progress" | "headerNorm",
    font?: "roboto" | "sourceSansPro",
    className?: string
}

const DeprecatedText: FunctionComponent<DeprecatedTextProps> = (
    {
        children,
        MainElement = "span",
        variant = "regular",
        font = "roboto",
        className,
        ...other
    }
) =>
    (
        <MainElement className={classNames(s[variant], className, s[font])} {...other}>
            {children}
        </MainElement>
    )

export default DeprecatedText;
