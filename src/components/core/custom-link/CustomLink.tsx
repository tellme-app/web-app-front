import React from "react";
import {Link, LinkProps} from "react-router-dom";
import * as H from "history";
import classNames from "classnames";
import styles from './customLink.module.scss'

interface CustomLinkProps {
    underline?: boolean
}

const CustomLink = <S extends H.LocationState>(
    {underline = false, ...props}
        : CustomLinkProps & React.PropsWithoutRef<LinkProps<S>> & React.RefAttributes<HTMLAnchorElement>
) => {
    return (
        <Link
            {...props}
            className={classNames(props.className, styles.root, underline && styles.underline)}
        />
    )
}

export default CustomLink
