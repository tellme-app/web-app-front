import React, {FunctionComponent} from "react";
import DeprecatedText from "../deprecated-text";
import style from './wordButton.module.scss'
import useStyle from "../../../style/useStyle";
import {BaseButton} from "../buttons";
import classNames from "classnames";
import {getSvgString, wh} from "./wordButtonModel";

interface WordButtonProps {
    word: string,
    progress?: number,
    className?: string
}

const WordButton: FunctionComponent<WordButtonProps> = (
    {
        word,
        progress = 0,
        className
    }
) => {

    if (progress < 0 || progress > 100)
        throw new Error("Неверное знаечение! от 0 до 100")
    const s = useStyle();
    let progressColor = s.bkg.error;

    if (progress >= 33 && progress < 67)
        progressColor = s.bkg.accident
    else if (progress >= 67)
        progressColor = s.bkg.success
    return (

        <BaseButton className={className}>
            <div className={classNames(style.container)}>
                <svg stroke="currentColor" fill="currentColor"
                     className={classNames(style.svgIcon, s.color.secondaryDark)} viewBox={`0 0 ${wh} ${wh}`}>
                    <path d={getSvgString(progress)}/>
                </svg>
                <div className={classNames(style.innerCircle, progressColor)}>
                    <DeprecatedText font={"sourceSansPro"} variant={"progress"} className={s.color.inverse}>
                        {word}
                    </DeprecatedText>
                </div>

            </div>
        </BaseButton>
    )
}

export default WordButton;
