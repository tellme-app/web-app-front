export const countRad = (value: number): number => {
    return value * 3.6 * (Math.PI / 180);
}

export const getSvgString = (value: number) => {
    const r = 98;
    const diff = wh / 2 - r
    const center = wh / 2;
    const afterHalf = `A ${r}, ${r} 1 0,1 ${diff} ${r + diff}`;
    return `M${center}, ${center} L ${wh - diff},${r + diff} ${value > 50 ? afterHalf : ""} A ${r},${r} 1 0,1 ${Math.cos(countRad(value)) * r + r + diff} ${Math.sin(countRad(value)) * r + r + diff}`;
};

export const wh = 200;
