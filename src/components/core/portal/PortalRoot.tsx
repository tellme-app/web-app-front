import React from 'react';
import style from "./portal.module.scss"

export const PORTAL_ID = "portal_id_" + Date.now();

const PortalRoot = () => <div className={style.root} id={PORTAL_ID}/>;

export default PortalRoot;