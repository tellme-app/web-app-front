import {PropsWithChildren, useLayoutEffect, useState} from 'react';
import ReactDOM from 'react-dom';
import {PORTAL_ID} from "./PortalRoot";


const Portal = (props: PropsWithChildren<{}>) => {
    const getPortalRoot = () => document.getElementById(PORTAL_ID);
    const [portalRoot, setPortalRoot] = useState(getPortalRoot());

    useLayoutEffect(() => {
        if (!portalRoot) setPortalRoot(getPortalRoot());
    }, [])

    if (!portalRoot) return null;

    return ReactDOM.createPortal(props.children, portalRoot);
}

export default Portal