import React, {FC} from "react";
import {BaseButton, BasicButtonProps} from "../buttons";
import {IconType} from "react-icons";
import style from './smallIconButton.module.scss'

interface SmallIconButtonProps extends BasicButtonProps {
    Variant: IconType
}

const SmallIconButton: FC<SmallIconButtonProps & BasicButtonProps> = ({Variant, ...props}) => (
    <BaseButton {...props}>
        <Variant className={style.basicClass}/>
    </BaseButton>
)


export default SmallIconButton;
