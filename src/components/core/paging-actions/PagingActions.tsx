import React from 'react';

export interface PagingViewProps {
  pageNumber: number
  totalPages?: number
}

export type PageView = (props: PagingViewProps) => JSX.Element

export interface PagingActionProps {
  onClick: () => void
  disabled?: boolean
}

export type PagingAction = (props: PagingActionProps) => JSX.Element

export interface PagingActionsProps
  extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  disabled?: boolean
  totalPages?: number
  firstPage?: number
  viewedNumber?: number
  pageNumber: number
  onPageChange: (pageNumber: number) => void
  Next?: PagingAction
  Previous?: PagingAction
  First?: PagingAction
  Last?: PagingAction
  PageView?: PageView
}

const PagingActions = (
  {
    disabled,
    totalPages,
    firstPage = 0,
    pageNumber,
    viewedNumber,
    onPageChange,
    Next,
    Previous,
    First,
    Last,
    PageView,
    ...props
  }: PagingActionsProps) => {
  const maxPage =  (totalPages || 0) + firstPage - 1
  const prevDisabled = disabled || pageNumber < 1
  const nextDisabled = disabled || (!totalPages || pageNumber >= maxPage)

  const onFirstClick = () => onPageChange(firstPage)
  const onNextClick = () => onPageChange(pageNumber + 1)
  const onPrevClick = () => onPageChange(pageNumber - 1)
  const onLastClick = () => onPageChange(maxPage)

  return (
    <div {...props}>
      {First && <First onClick={onFirstClick} disabled={prevDisabled} />}
      {Previous && <Previous onClick={onPrevClick} disabled={prevDisabled} />}
      {PageView && <PageView pageNumber={viewedNumber || pageNumber} totalPages={totalPages} />}
      {Next && <Next onClick={onNextClick} disabled={nextDisabled} />}
      {Last && <Last onClick={onLastClick} disabled={!totalPages || nextDisabled} />}
      {props.children}
    </div>
  );
}

export default PagingActions