import React from 'react';
import PagingActions, {PagingActionProps, PagingActionsProps} from "../PagingActions";
import {FiChevronLeft, FiChevronRight, FiChevronsLeft, FiChevronsRight} from "react-icons/all";
import {IconType} from "react-icons";
import IconButton from "../../buttons/icon-button";
import Text from "../../text";
import classNames from "classnames";
import styles from './iconButtonsPaging.module.scss'

const IconButtonsPaging = (
  {
    Next,
    Previous,
    First,
    Last,
    PageView,
    pageNumber,
    totalPages,
    onPageChange,
    ...props
  }: PagingActionsProps) => {

  return (
    <PagingActions
      className={classNames(props.className, styles.root)}
      PageView={(p) => <Text elm='div' variant='tip'>{p.pageNumber}-{p.totalPages}</Text>}
      Next={pageButton(FiChevronRight)}
      Previous={pageButton(FiChevronLeft)}
      First={pageButton(FiChevronsLeft)}
      Last={pageButton(FiChevronsRight)}
      pageNumber={pageNumber}
      totalPages={totalPages}
      onPageChange={onPageChange}
      {...props}
    />);
}

const pageButton = (icon: IconType) => (props: PagingActionProps) => (
  <IconButton icon={icon} {...props}/>
)

export default IconButtonsPaging