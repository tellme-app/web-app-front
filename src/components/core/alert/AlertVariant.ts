type AlertVariant = "error" | "info" | "warn" | "success" | "none"

export default AlertVariant