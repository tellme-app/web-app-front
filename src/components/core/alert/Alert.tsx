import React, {ReactNode} from "react";
import style from "./alertStyle.module.scss";
import AlertVariant from "./AlertVariant";
import classNames from "classnames";
import {MdCancel, MdCheckCircle, MdInfo, MdWarning} from "react-icons/md"
import useStyle from "../../../style/useStyle";
import Text from "../text";

export interface AlertProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    variant?: AlertVariant
    text?: ReactNode
}

const icon = {
    error: MdCancel,
    warn: MdWarning,
    info: MdInfo,
    success: MdCheckCircle,
    none: undefined
}

const Alert = ({variant = "success", ...props}: AlertProps) => {
    const s = useStyle();

    const Icon = icon[variant];

    const background = {
        error: s.bkg.error,
        warn: s.bkg.accident,
        info: s.bkg.secondaryDark,
        success: s.bkg.success,
        none: undefined
    }

    const rootClass = classNames(
        style.root,
        background[variant],
        {[s.color.inverse]: variant !== "none"},
        props.className
    )

    return (
        <div {...props} className={rootClass}>
            {Icon && <Icon className={style.icon}/>}
            {props.text && <Text className={style.text} variant='action'>{props.text}</Text>}
            {props.children}
        </div>
    )
}

export default Alert