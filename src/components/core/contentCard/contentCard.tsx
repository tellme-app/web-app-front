import React, {FunctionComponent} from "react";
import style from './contentCard.module.scss'
import classNames from "classnames";

interface ContentCardProps {
    className?: string
}

const ContentCard: FunctionComponent<ContentCardProps> = (
    {
        children,
        className,
        ...other
    }
) =>
    (
        <div className={classNames(style.contentCard, className)} {...other}>
            {children}
        </div>
    );

export default ContentCard;
