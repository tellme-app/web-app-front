import React from 'react';
import {IconType} from "react-icons";
import classNames from "classnames";
import style from "./icon.module.scss"
import StyleContainer from "../containers/style-container";
import {SpanProps, StyleProps} from "../../props";

export interface IconProps {
    icon?: IconType
    scale?: "1" | "1.1" | "1.2" | "1.3" | "1.4" | "1.5" | "1.6" | "1.7" | "1.8" | "1.9" | "2" | "2.1" | "2.2"
    iconWrapper?: React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement>,
}

const Icon = ({icon, scale = "1.4", iconWrapper, ...props}: IconProps & StyleProps & SpanProps) => {
    const Icon = icon;
    const scaleName = `fontScaleX${scale?.replace('.', '_')}`
    return (
        <StyleContainer elm='span' {...props}>
            <span
                {...iconWrapper}
                className={classNames(style.wrapper, style[scaleName], iconWrapper?.className)}
            >
                {Icon && <Icon/>}
                {props.children}
            </span>
        </StyleContainer>
    )
}


export default Icon