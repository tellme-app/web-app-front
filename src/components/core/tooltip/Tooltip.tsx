import React, {ElementType} from "react";
import StyledPopup, {StyledPopupProps} from "../popup/StyledPopup";
import {HTMLProps, SpanProps, StyleProps} from "../../props";
import Text, {TextProps} from "../text/Text";
import classNames from "classnames";
import styles from "./tooltip.module.scss";

interface TooltipProps<E extends ElementType = any> {
    text?: string
    textProps?: TextProps<E> & HTMLProps<E>
    opacity?: boolean
}

const Tooltip = (
    {
        opacity = true,
        text,
        textProps,
        ...props
    }: TooltipProps & StyledPopupProps & StyleProps & SpanProps) => {
    const getContent = () => <Text disableBottomIdent disableTopIdent variant='action' {...textProps}>{text}</Text>
    return (
        <StyledPopup
            p='x2'
            bkg='inverse'
            color='inverse'
            size='nano'
            popupProps={{
                trigger: 'hover',
                position: 'bottom',
                ...props.popupProps
            }}
            className={classNames(props.className, styles.contentRoot, opacity && styles.contentOpacity)}
            content={text && getContent()}
            {...props}
        />
    );
}

export default Tooltip;
