import React from 'react'
import style from './styles.module.scss'
import logo from "./loader/logoGreen.png";

const FullScreenLoading = () => (
    <div className={style.fullScreenContainer}>
            <span className={style.fullscreenLogoAnim}>
           <img
               alt=''
               width={80}
               height={80}
               src={logo}
               className={style.logoLoadingIcon}
           />
            </span>
    </div>
)

export default FullScreenLoading
