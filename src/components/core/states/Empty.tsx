import React from 'react'
import {StateProps} from './stateProps'

const Empty = (props: StateProps) => props.message || 'Нет данных'


export default Empty
