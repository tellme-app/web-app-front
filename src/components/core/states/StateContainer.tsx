import React from 'react'
import style from './styles.module.scss'
import classNames from 'classnames'
import {StateContainerProps} from "./stateProps";


const StateContainer = (props: StateContainerProps) => (
    <div
        {...props}
        className={classNames(style.stateContainer, props.className)}
    />
)

export default StateContainer
