import React from 'react'
import {StateProps} from './stateProps'
import StateContainer from "./StateContainer";
import classNames from "classnames";
import useStyle from "../../../style/useStyle";
import Text from "../text"

const LocalError = (props: StateProps) => (
    <StateContainer {...props} className={classNames(useStyle().m.x4, props.className)}>
        <Text>{props.message || "Ошибка"}</Text>
    </StateContainer>
)

export default LocalError
