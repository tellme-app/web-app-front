import React from 'react'
import style from './styles.module.scss'
import classNames from 'classnames'
import StateContainer from "./StateContainer";
import {StateProps} from "./stateProps";
import LogoLoader from "./loader/LogoLoader";
import useStyle from "../../../style/useStyle";
import Text from '../text';

const FilledLoading = (props: StateProps) => {
    const s = useStyle();
    return (
        <StateContainer {...props} className={classNames(style.filledState, props.className)}>
            <LogoLoader scale={35} className={s.mTop.x4}/>
            {props.message && <Text disableTopIdent mTop="x8">{props.message}</Text>}
        </StateContainer>
    )
}

export default FilledLoading
