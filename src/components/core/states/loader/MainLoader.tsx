import React from 'react'
import style from './styles.module.scss'
import classNames from "classnames";
import {LoaderProps} from "./loaderProps";

const MainLoader = ({scale, ...props}: LoaderProps) => {
    const itemStyle = {width: scale, height: scale}
    return (
        <div
            style={{height: scale * 2.5}}
            {...props}
            className={classNames(style.mainLoaderContainer, props.className)}
        >
            <div style={itemStyle} className={style.mainLoaderItem1}/>
            <div style={itemStyle} className={style.mainLoaderItem2}/>
            <div style={itemStyle} className={style.mainLoaderItem3}/>
        </div>
    )
}


export default MainLoader
