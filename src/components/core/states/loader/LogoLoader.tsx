import React from 'react'
import logo from './logo.svg'
import style from './styles.module.scss'
import classNames from "classnames";
import {LoaderProps} from "./loaderProps";

const LogoLoader = ({scale, ...props}: LoaderProps) => (
    <div
        style={{width: scale * 1.5, height: scale * 1.5}}
        {...props}
        className={classNames(style.logoLoader, props.className)}
    >
        <img
            alt=''
            width={scale}
            height={scale}
            src={logo}
            className={style.logoLoadingIcon}
        />
    </div>
)


export default LogoLoader
