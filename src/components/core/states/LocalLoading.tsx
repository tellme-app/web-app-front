import React from 'react'
import classNames from 'classnames'
import MainLoader from "./loader/MainLoader";
import StateContainer from "./StateContainer";
import {StateProps} from "./stateProps";
import useStyle from "../../../style/useStyle";
import Text from "../text"

const LocalLoading = (props: StateProps) => (
    <StateContainer {...props} className={classNames(useStyle().m.x4, props.className)}>
        {props.message && <Text mTop="x4">{props.message}</Text>}
        <MainLoader scale={15}/>
    </StateContainer>
)

export default LocalLoading
