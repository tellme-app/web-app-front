import React from 'react'

export interface StateContainerProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
}

export interface StateProps extends StateContainerProps {
    message?: string
}


export interface LoaderProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    scale: number
}

export interface LoadingProps {
    className?: string
    containerProps?: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>,
        HTMLDivElement>
}
