import React, {ElementType} from "react";
import style from './text.module.scss'
import classNames from "classnames";
import {TextVariants} from "./TextVariants";
import {HTMLProps, StyleProps} from "../../props";
import StyleContainer from "../containers/style-container";

const elmMap: TextVariants = {
    h1: {type: "h1", size: "plusSize", color: "dark"},
    h2: {type: "h2", size: "giant", color: "dark"},
    h3: {type: "h3", size: "x-large", color: "dark"},
    h4: {type: "h4", size: "large", color: "dark"},
    h5: {type: "h5", size: "medium", color: "dark"},
    h6: {type: "h6", size: "small", color: "dark"},
    subH1: {type: "h6", size: "small", color: "dark"},
    subH2: {type: "h6", size: "x-small", color: "dark"},
    text1: {type: "p", size: "x-small", color: "main"},
    text2: {type: "p", size: "mini", color: "light"},
    action: {type: "span"},
    tip: {type: "span", size: "mini", color: "light"},
    subTip: {type: "span", size: "micro", color: "light"},
}

export interface TextProps<E extends ElementType = any> extends StyleProps {
    elm?: E,
    variant?: keyof TextVariants
    bold?: boolean
    disableBottomIdent?: boolean
    disableTopIdent?: boolean
    textIdent?: boolean
}

const Text = <E extends ElementType = any>(
    {
        variant = "text1", elm, textIdent, bold, disableBottomIdent, disableTopIdent, ...props
    }: TextProps<E> & HTMLProps<E>
) => {
    const opt = elmMap[variant];
    const action = variant === "action"
    return (
        <StyleContainer
            elm={elm || opt.type}
            size={opt.size}
            color={opt.color}
            {...props}
            className={classNames({
                    [style.bottomIdent]: !action && !disableBottomIdent,
                    [style.topIdent]: !action && !disableTopIdent,
                },
                textIdent && style.textIdent,
                opt.color,
                bold && style.bold,
                props.className,
            )}
        />
    )
}

export default Text;
