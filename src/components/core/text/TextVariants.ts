import {ElementType} from "react";
import {StyleSizes} from "../../../style/StyleSizes";
import StyleColors from "../../../style/StyleColors";

export interface ElementOption {
    type: ElementType,
    size?: keyof StyleSizes
    color?: keyof StyleColors
}

export interface TextVariants {
    h1: ElementOption
    h2: ElementOption
    h3: ElementOption
    h4: ElementOption
    h5: ElementOption
    h6: ElementOption
    subH1: ElementOption
    subH2: ElementOption
    text1: ElementOption
    text2: ElementOption
    action: ElementOption
    tip: ElementOption
    subTip: ElementOption
}