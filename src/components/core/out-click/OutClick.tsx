import React from 'react';
import useOutClick from "../../../util/useOutClick";


interface OutClickProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement> {
    onOutClick: (e: MouseEvent) => void
}

const OutClick = (props: OutClickProps) => {
    const ref = useOutClick(props.onOutClick)
    return <span {...props} ref={ref}/>
}

export default OutClick