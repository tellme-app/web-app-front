import React, {FC} from "react";
import DeprecatedText from "../deprecated-text";
import classNames from "classnames";
import s from './examplePhrase.module.scss'
import {MdAddCircleOutline, MdHelpOutline, MdPanoramaFishEye, MdRemoveCircleOutline} from "react-icons/all";

interface ExamplePhraseProps {
    className?: string
    variant?: "positive" | "negative" | "question" | "neutral"
}

const ExamplePhrase: FC<ExamplePhraseProps> = (
    {
        variant= "neutral",
        className,
        children
    }
) => {

    let icon = <MdPanoramaFishEye className={s.icon}/>
    if (variant === "positive")
        icon = <MdAddCircleOutline className={s.icon}/>
    else if (variant === "negative")
        icon = <MdRemoveCircleOutline className={s.icon}/>
    else if (variant === "question")
        icon = <MdHelpOutline className={s.icon}/>

    return(
        <div className={classNames(className, s.phraseContainer)}>
            {icon}
            <DeprecatedText className={s.phrase} font="roboto" variant={"tip"} MainElement={"p"}>
                {children}
            </DeprecatedText>

        </div>
    )

}

export default ExamplePhrase;
