import React, {ReactNode, useState} from "react";
import {usePopper} from "react-popper";
import {Placement} from "@popperjs/core/lib/enums";
import Portal from "../portal/Portal";
import styles from "./popup.module.scss";
import {Transition} from "../animation/Transition";
import {AnimationSpeed, TransitionProps} from "../animation/animationTypes";
import classNames from "classnames";
import useOutClick from "../../../util/useOutClick";

type TriggerType = 'click' | 'hover' | 'none'

const defaultTransitionSpeed = {open: 250, close: 200}

export interface PopupProps {
    children?: ReactNode
    content?: ReactNode
    position?: Placement
    distance?: number
    show?: boolean
    outClickClose?: boolean
    onClose?: () => void
    onOpen?: () => void
    transition?: boolean
    trigger?: TriggerType
    delay?: AnimationSpeed
    wrapperProps?: React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement>
    contentProps?: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>
    transitionProps?: TransitionProps
}

const Popup = (
    {
        trigger = 'none',
        distance = 10,
        position = 'auto',
        transition = true,
        outClickClose = true,
        ...props
    }: PopupProps) => {
    const [innerShow, setInnerShow] = useState(props.show || false)
    const [referenceElement, setReferenceElement] = useState<HTMLSpanElement | null>(null);
    const [popperElement, setPopperElement] = useState<HTMLDivElement | null>(null);
    const popper = usePopper(referenceElement, popperElement, {
        placement: position,
        modifiers: [
            {
                name: 'offset',
                options: {
                    offset: [0, distance],
                },
            },
            {
                name: 'flip'
            }
        ],
    });

    const controlled = props.show !== undefined

    let show = props.show || innerShow


    const onMouseEnter = () => {
        if (trigger !== 'hover') return
        setInnerShow(true)
        if (props.onOpen) props.onOpen()
    }

    const onMouseLeave = () => {
        if (trigger !== 'hover') return
        setInnerShow(false)
        if (props.onClose) props.onClose()
    }

    const onClick = () => {
        if (trigger !== 'click') return
        setInnerShow(!innerShow)
        if (props.onOpen && !innerShow) props.onOpen()
        if (props.onClose && innerShow) props.onClose()
    }

    useOutClick(() => {
        if (outClickClose && (innerShow || (controlled && props.show))) {
            if (!controlled) setInnerShow(!innerShow)
            if (props.onClose) props.onClose()
        }
    }, referenceElement)

    const getContent = () => (
        <div
            ref={setPopperElement}
            {...props.contentProps}
            style={{...popper.styles.popper, ...props.contentProps?.style}}
            {...popper.attributes.popper}
            className={classNames(styles.contentRoot, props.contentProps?.className)}
        >
            {props.content}
        </div>
    )

    return (
        <>
            <span
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
                onClick={onClick}
                {...props.wrapperProps}
                ref={setReferenceElement}
            >
                {props.children}
            </span>
            <Portal>
                {transition ? (
                        <Transition
                            open={show}
                            unmountAfterClose
                            openingClass={styles.transparencyOpening}
                            closeClass={styles.close}
                            closingClass={styles.transparencyClosing}
                            speed={defaultTransitionSpeed}
                            delay={props.delay}
                            {...props.transitionProps}
                        >
                            {getContent()}
                        </Transition>
                    ) :
                    show && getContent()
                }
            </Portal>
        </>
    );
}

export default Popup;
