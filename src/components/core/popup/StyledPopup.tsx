import React, {ReactNode} from "react";
import Popup, {PopupProps} from "./Popup";
import {SpanProps, StyleProps} from "../../props";
import StyleContainer from "../containers/style-container";

export interface StyledPopupProps {
    popupProps?: PopupProps
    children?: ReactNode
    content?: ReactNode
}

const StyledPopup = ({content, popupProps, children, ...props}: StyledPopupProps & StyleProps & SpanProps) => {
    return (
        <Popup {...popupProps} content={(
            <StyleContainer elm='span' {...props}>
                {content}
            </StyleContainer>
        )}>
            {children}
        </Popup>
    )
}

export default StyledPopup;
