import React, {TextareaHTMLAttributes} from "react";
import style from './input.module.scss'
import classNames from "classnames";
import {StyleProps} from "../../props";
import StyleContainer from "../containers/style-container";

export interface BaseTextAreaProps extends StyleProps, Omit<TextareaHTMLAttributes<'textarea'>, 'color'> {
    nativeProps?: TextareaHTMLAttributes<HTMLTextAreaElement>
}

const BaseTextArea = (props:BaseTextAreaProps) => (
    <StyleContainer
        elm='textarea'
        {...props}
        bkg={props.bkg || 'inherit'}
        className={classNames(style.root, props.className)}
    />
)

export default BaseTextArea