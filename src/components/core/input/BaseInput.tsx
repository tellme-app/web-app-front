import React, {InputHTMLAttributes} from "react";
import style from './input.module.scss'
import classNames from "classnames";
import {StyleProps} from "../../props";
import StyleContainer from "../containers/style-container";

export interface BaseInputProps extends StyleProps, Omit<InputHTMLAttributes<'input'>, 'color' | 'size'> {
    nativeProps?: InputHTMLAttributes<HTMLInputElement>
}

const BaseInput = (props:BaseInputProps) => (
    <StyleContainer
        elm='input'
        {...props}
        bkg={props.bkg || 'inherit'}
        className={classNames(style.root, props.className)}
    />
)

export default BaseInput;