import React, {ElementType, useMemo} from "react";
import classNames from "classnames";
import useStyle from "../../../../style/useStyle";
import {HTMLProps, StyleProps} from "../../../props";

export interface PropsWithCustomElement<E extends ElementType, P extends { [key: string]: any } = { [key: string]: any }> {
    elm: E
    nativeProps?: P
}

const StyleContainer = <E extends ElementType>(
    {
        elm,
        size,
        color,
        colorHover,
        colorActive,
        colorDisabled,
        bkg,
        bkgHover,
        bkgActive,
        bkgDisabled,
        p,
        pTop,
        pBottom,
        pLeft,
        pRight,
        m,
        mTop,
        mBottom,
        mLeft,
        mRight,
        ...props
    }: PropsWithCustomElement<E> & StyleProps & HTMLProps<E>
) => {
    const s = useStyle();

    const classes = useMemo(() => classNames(
        size && s.size[size],
        color && s.color[color],
        colorHover && s.colorHover[colorHover],
        colorActive && s.colorActive[colorActive],
        colorDisabled && s.colorDisabled[colorDisabled],
        bkg && s.bkg[bkg],
        bkgHover && s.bkgHover[bkgHover],
        bkgActive && s.bkgActive[bkgActive],
        bkgDisabled && s.bkgDisabled[bkgDisabled],
        p && s.p[p],
        pTop && s.pTop[pTop],
        pBottom && s.pBottom[pBottom],
        pLeft && s.pLeft[pLeft],
        pRight && s.pRight[pRight],
        m && s.m[m],
        mTop && s.mTop[mTop],
        mBottom && s.mBottom[mBottom],
        mLeft && s.mLeft[mLeft],
        mRight && s.mRight[mRight],
    ), [
        s, size, color, bkg, p, pTop, pBottom, pLeft, pRight,
        m, mTop, mBottom, mLeft, mRight, colorHover, colorActive,
        colorDisabled, bkgHover, bkgActive, bkgDisabled
    ])

    const Elm = elm as ElementType;
    return <Elm {...props} className={classNames(classes, props.className)}/>
}

export default StyleContainer;