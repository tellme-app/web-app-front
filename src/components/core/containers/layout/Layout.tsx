import React, {useMemo} from "react";
import style from "./layout.module.scss"
import classNames from "classnames";
import {DivProps, StyleProps} from "../../../props";
import StyleContainer from "../style-container";

export interface LayoutProps extends DivProps {
    boxShadow?: boolean
    flex?: boolean
    sticky?: boolean
    inlineFlex?: boolean
    justifyContentCenter?: boolean
    justifyContentLeft?: boolean
    justifyContentRight?: boolean
    alignItemsCenter?: boolean
    alignItemsStart?: boolean
    alignItemsEnd?: boolean
    textAlignCenter?: boolean
    textAlignRight?: boolean
    textAlignLeft?: boolean
}

const Layout = (
    {
        boxShadow,
        flex,
        sticky,
        inlineFlex,
        justifyContentCenter,
        justifyContentLeft,
        justifyContentRight,
        alignItemsCenter,
        alignItemsStart,
        alignItemsEnd,
        textAlignCenter,
        textAlignRight,
        textAlignLeft,
        ...props
    }: LayoutProps & StyleProps
) => {
    const classes = useMemo(() => classNames(
        {
            [style.flex]: flex,
            [style.sticky]: sticky,
            [style.inlineFlex]: inlineFlex,
            [style.boxShadow]: boxShadow,
            [style.justifyContentCenter]: justifyContentCenter,
            [style.justifyContentLeft]: justifyContentLeft,
            [style.justifyContentRight]: justifyContentRight,
            [style.alignItemsCenter]: alignItemsCenter,
            [style.alignItemsStart]: alignItemsStart,
            [style.alignItemsEnd]: alignItemsEnd,
            [style.textAlignCenter]: textAlignCenter,
            [style.textAlignRight]: textAlignRight,
            [style.textAlignLeft]: textAlignLeft,

        }
    ), [
        boxShadow,
        flex,
        sticky,
        inlineFlex,
        justifyContentCenter,
        justifyContentLeft,
        justifyContentRight,
        alignItemsCenter,
        alignItemsStart,
        alignItemsEnd,
        textAlignCenter,
        textAlignRight,
        textAlignLeft
    ])

    return <StyleContainer elm={'div'} {...props} className={classNames(classes, props.className)}/>;
}

export default Layout