import React from "react";
import style from "./pageContent.module.scss"
import classNames from "classnames";
import {StyleProps} from "../../../props";
import Layout, {LayoutProps} from "../layout/Layout";

export interface PageProps extends LayoutProps {
    maxWidth?: string | number
}

const PageContent = ({maxWidth = 1000, ...props}: PageProps & StyleProps) => (
    <Layout
        {...props}
        style={{...props.style, maxWidth}}
        className={classNames(style.root, props.className)}
    >
        {props.children}
    </Layout>
);

export default PageContent;