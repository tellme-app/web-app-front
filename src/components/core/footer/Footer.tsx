import React, {ReactNode, useRef} from 'react';
import classNames from "classnames";
import styles from './footer.module.scss'
import Portal from "../portal/Portal";

const DefaultPhantom = ({height}: {height: number}) => <div style={{height}}/>

interface FooterProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  children: ReactNode
  phantom?: boolean
  Phantom?: (props: {height: number}) => JSX.Element
}

const Footer = (
  {
    phantom = true,
    Phantom = DefaultPhantom,
    ...props
  }: FooterProps) => {
  const ref = useRef<HTMLDivElement>(null)
  return (
    <>
      {phantom && <Phantom height={ref.current?.clientHeight || 0}/>}
      <Portal>
        <div {...props} ref={ref} className={classNames(props.className, styles.root)}>
          {props.children}
        </div>
      </Portal>
    </>
  )
}
export default Footer