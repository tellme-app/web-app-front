import React from "react";
import style from "./splitter.module.scss";
import classNames from "classnames";
import Layout from "../containers/layout";
import {LayoutProps} from "../containers/layout/Layout";
import {StyleProps} from "../../props";

const Splitter = (props: LayoutProps & StyleProps) => {
    const classes = classNames(style.root, props.className);
    return <Layout {...props} mTop={props.mTop || 'x4'} className={classes}/>
}

export default Splitter