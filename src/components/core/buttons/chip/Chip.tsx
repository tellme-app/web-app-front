import React from "react";
import style from './chip.module.scss'
import classNames from "classnames";
import Button, {ButtonProps} from "../Button";

const Chip = ({hovers = false, flat = true, ...props}: ButtonProps) => (
    <Button
        hovers={hovers}
        flat={flat}
        {...props}
        className={classNames(style.root, props.className)}
    />
)

export default Chip