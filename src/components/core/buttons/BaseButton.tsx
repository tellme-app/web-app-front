import React from "react";
import style from './button.module.scss'
import classNames from "classnames";
import {ButtonHTMLAttributesProps, StyleProps} from "../../props";
import StyleContainer from "../containers/style-container";

export interface BasicButtonProps extends StyleProps, Omit<ButtonHTMLAttributesProps<'button'>, 'color'> {
}

export const BaseButton = (props: BasicButtonProps) => (
    <StyleContainer
        elm='button'
        {...props}
        className={classNames(style.baseButton, props.className)}
    />
)