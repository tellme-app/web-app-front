import React from "react";
import style from './roundButton.module.scss'
import classNames from "classnames";
import Button, {ButtonProps} from "../Button";

const RoundButton = (props: ButtonProps) => (
    <Button
        {...props}
        iconPosition="inside"
        className={classNames(style.roundButton, props.className)}
    />
)

export default RoundButton
