import React, {useMemo} from "react";
import style from './button.module.scss'
import classNames from "classnames";
import {BaseButton, BasicButtonProps} from "./BaseButton";
import {ButtonIcon} from "./ButtonIcon";
import IconPosition from "./IconPosition";
import {IconType} from "react-icons";
import Text from "../text";
import StyleColors from "../../../style/StyleColors";

export interface ButtonProps extends BasicButtonProps {
    outlined?: boolean
    flat?: boolean
    border?: boolean
    hovers?: boolean
    preset?: keyof StyleColors
    text?: string
    icon?: IconType
    iconPosition?: IconPosition
}

const Button = (
    {
        outlined = false,
        border = false,
        flat = false,
        hovers = true,
        size = "mini",
        preset = "main",
        text,
        icon,
        iconPosition = "left",
        ...props
    }: ButtonProps
) => {
    const classes = useMemo(() => classNames(style.button, {
        [style.buttonBorder]: border,
        [style.buttonShadow]: !flat,
        [style.buttonShadowHovers]: !flat && hovers,
    }), [border, flat, hovers])

    const styleProps = useMemo<BasicButtonProps>(() => outlined ? {
        color: preset,
        colorHover: hovers ? preset : undefined,
        colorActive: hovers ? preset : undefined,
        colorDisabled: hovers ? preset : undefined
    } : {
        bkg: preset,
        bkgHover: flat && hovers ? (preset || "inherit") : undefined,
        bkgActive: flat && hovers ? (preset || "inherit") : undefined,
        bkgDisabled: flat && hovers ? (preset || "inherit") : undefined
    }, [preset, outlined, flat, hovers])

    const buttonIcon = icon && <ButtonIcon icon={icon} position={iconPosition}/>
    return (
        <BaseButton
            size={size}
            {...styleProps}
            {...props}
            className={classNames(classes, props.className)}
        >
            {iconPosition === "left" && buttonIcon}
            {text && <Text variant="action">{text}</Text>}
            {props.children}
            {(iconPosition === "right" || iconPosition === "inside") && buttonIcon}
        </BaseButton>
    )

}

export default Button