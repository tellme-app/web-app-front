import React from "react";
import style from './ellipseButton.module.scss'
import classNames from "classnames";
import Button, {ButtonProps} from "../Button";

const EllipseButton = (props: ButtonProps) => (
    <Button {...props} className={classNames(style.ellipseButton, props.className)}/>
)

export default EllipseButton
