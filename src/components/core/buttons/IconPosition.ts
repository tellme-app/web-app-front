type IconPosition = "left" | "inside" | "right";

export default IconPosition;