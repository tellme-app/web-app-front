import React from "react";
import style from './button.module.scss'
import classNames from "classnames";
import IconPosition from "./IconPosition";
import Icon, {IconProps} from "../icon/Icon";
import {SpanProps, StyleProps} from "../../props";

export interface ButtonIconProps extends IconProps {
    position?: IconPosition
}

export const ButtonIcon = ({position = "left", scale = "1.4", ...props}: ButtonIconProps & StyleProps & SpanProps) => {
    return (
        <Icon
            scale={scale}
            {...props}
            className={classNames(
                style.buttonIcon,
                {
                    [style.buttonIconPosLeft]: position === "left",
                    [style.buttonIconPosRight]: position === "right"
                },
                props.className
            )}
        />
    )
}
