import React from "react";
import {ButtonProps} from "../Button";
import RoundButton from "../round-button";

const IconButton = ({flat = true, outlined = true, ...props}: ButtonProps) => (
    <RoundButton
        {...props}
        flat={flat}
        outlined={outlined}
        text={undefined}
        iconPosition="inside"
    />
)

export default IconButton
