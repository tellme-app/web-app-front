import React, {CSSProperties, TransitionEvent, useEffect, useRef, useState} from "react";
import classNames from "classnames";
import {AnimationSpeed, TransitionProps, TransitionState} from "./animationTypes";

const DEFAULT = {
    SPEED: 250,
    DELAY: 0
}

const executeSpeed = (speed?: number | AnimationSpeed, open?: boolean) => {
    if (!speed) return;
    if (typeof speed === "number") return speed;
    return open ? speed.open : speed.close
}

export const Transition = ({open, onOpenStart, onCloseStart, ...props}: TransitionProps) => {
    const [state, setState] = useState<TransitionState>(() => open ?
        (props.openMountAnimated ? "close" : "open") :
        (props.closeMountAnimated ? "open" : "close")
    )

    const timerId = useRef<NodeJS.Timeout>()

    const timout = (handle: () => void) => {
        if (timerId.current) clearTimeout(timerId.current)
        timerId.current = setTimeout(handle, 80)
    }

    const speed = executeSpeed(props.speed, open) || DEFAULT.SPEED;
    const delay = executeSpeed(props.delay, open) || DEFAULT.DELAY;

    let stateClass: string | undefined;
    let stateStyle: CSSProperties | undefined;

    if (state === "open") {
        stateClass = props.openClass
        stateStyle = props.openStyle
    } else if (state === "opening") {
        stateClass = props.openingClass
        stateStyle = props.openingStyle
    } else if (state === "close") {
        stateClass = props.closeClass
        stateStyle = props.closeStyle
    } else if (state === "closing") {
        stateClass = props.closingClass
        stateStyle = props.closingStyle
    }

    const styles = {
        transitionDuration: `${speed}ms`,
        transitionDelay: `${delay}ms`,
        ...stateStyle,
        ...props.style
    }

    useEffect(() => {
        if (open && (state === "close" || state === "closing")) {
            if (timerId.current) clearTimeout(timerId.current)
            timout(() => setState("opening"))
            if (onOpenStart) onOpenStart()
        }
        if (!open && (state === "open" || state === "opening")) {
            if (timerId.current) clearTimeout(timerId.current)
            timout(() => setState("closing"))
            if (onCloseStart) onCloseStart()
        }
        return () => {
            if (timerId.current) clearTimeout(timerId.current)
        }
    }, [open, state, onOpenStart, onCloseStart])

    const onEnd = (e: TransitionEvent<HTMLDivElement>) => {
        e.stopPropagation()
        if (state === "opening") {
            setState("open")
            if (props.onOpenEnd) props.onOpenEnd(e)
        } else if (state === "closing") {
            setState("close")
            if (props.onCloseEnd) props.onCloseEnd(e)
        }
    }

    return (
        <div
            {...props}
            ref={props.innerRef}
            style={styles}
            className={classNames(stateClass, props.className)}
            onTransitionEnd={onEnd}
        >
            {props.unmountAfterClose && state === "close" ? null : props.children}
        </div>
    );
}