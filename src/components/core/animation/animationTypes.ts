import React, {CSSProperties, TransitionEvent} from "react";

export type AnimationDirection = "left" | "top"

export type AnimationSpeed = { open?: number, close?: number }

export type TransitionState = "opening" | "open" | "closing" | "close"

export interface TransitionProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    openClass?: string
    openingClass?: string
    closeClass?: string
    closingClass?: string
    openStyle?: CSSProperties
    openingStyle?: CSSProperties
    closeStyle?: CSSProperties
    closingStyle?: CSSProperties
    open?: boolean
    speed?: number | AnimationSpeed
    delay?: number | AnimationSpeed
    innerRef?: any
    unmountAfterClose?: boolean
    openMountAnimated?: boolean
    closeMountAnimated?: boolean
    onOpenStart?: () => void
    onCloseStart?: () => void
    onOpenEnd?: (e: TransitionEvent<HTMLDivElement>) => void
    onCloseEnd?: (e: TransitionEvent<HTMLDivElement>) => void
}