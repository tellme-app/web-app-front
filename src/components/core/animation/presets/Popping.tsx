import React from "react";
import {AnimationDirection, TransitionProps} from "../animationTypes";
import style from './transitionPreset.module.scss'
import classNames from "classnames";
import {Transition} from "../Transition";


export interface PoppingProps extends TransitionProps {
    from?: AnimationDirection
}

export const Popping = ({from = "left", ...props}: PoppingProps) => {
    let directionClass = style[`poppingAnimation_${from}`]
    return (
        <Transition
            openingClass={style.poppingOpening}
            closingClass={classNames(style.poppingOpening, directionClass)}
            closeClass={classNames(style.close, directionClass)}
            {...props}
        />
    )
}
