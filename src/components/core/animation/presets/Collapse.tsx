import React, {useRef} from "react";
import {TransitionProps} from "../animationTypes";
import style from './transitionPreset.module.scss'
import {Transition} from "../Transition";

export interface CollapseProps extends TransitionProps {
    maxHeight?: number
}

export const Collapse = ({maxHeight, ...props}: CollapseProps) => {
    const ref = useRef<HTMLDivElement>(null)
    const collapseStyle = {maxHeight: maxHeight || ref.current?.clientHeight}
    return (
        <Transition
            openStyle={collapseStyle}
            openingStyle={collapseStyle}
            openingClass={style.collapseOpening}
            closeClass={style.collapseClose}
            closingClass={style.collapseClosing}
            {...props}
        >
            <div ref={ref}>
                {props.children}
            </div>
        </Transition>
    )
}
