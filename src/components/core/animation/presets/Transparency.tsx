import React from "react";
import {TransitionProps} from "../animationTypes";
import style from './transitionPreset.module.scss'
import {Transition} from "../Transition";

export const Transparency = (props: TransitionProps) => (
    <Transition
        openingClass={style.transparencyOpening}
        closeClass={style.close}
        closingClass={style.transparencyClosing}
        {...props}
    />
)
