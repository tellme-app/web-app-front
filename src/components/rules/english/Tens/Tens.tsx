import React, {useState} from "react";
import Layout from "../../../core/containers/layout";
import {LayoutPropsWithData} from "../../LayoutPropsWithData";
import TensData from "./TensData";
import Text from "../../../core/text";
import Splitter from "../../../core/splitter";
import Chip from "../../../core/buttons/chip";
import {
    MdAddCircleOutline,
    MdHelpOutline,
    MdKeyboardArrowDown,
    MdKeyboardArrowUp,
    MdRemoveCircleOutline
} from "react-icons/all";
import {StyleProps} from "../../../props";
import EllipseButton from "../../../core/buttons/ellipse-button";
import RoundButton from "../../../core/buttons/round-button";

const toArray = (str: string) => str.split(",")

const Tens = ({data, ...props}: StyleProps & LayoutPropsWithData<TensData>) => {
    const [open, setOpen] = useState(false)
    return (
        <div>
            <Layout boxShadow p="x5" {...props}>
                <Text disableTopIdent disableBottomIdent variant="h2">{data.name}</Text>
                <Text variant="tip" disableBottomIdent pBottom="x4" elm='div'>
                    {data.description}
                </Text>
                <Chip color="main" icon={MdAddCircleOutline} text={data.positive}/><br/>
                <Chip color="main" icon={MdRemoveCircleOutline} text={data.question}/><br/>
                <Chip color="main" icon={MdHelpOutline} text={data.negative}/><br/>
                <Splitter/>
                <Text variant="subH1">Helpers words</Text>
                {data.helperWords && (
                    <Layout textAlignCenter>
                        {toArray(data.helperWords).map(w => (
                            <Chip bkg="light" color="main" style={{minWidth: 95}} m="x1">
                                {w}
                            </Chip>
                        ))}
                    </Layout>
                )}
                <Layout flex alignItemsCenter>
                    <Text variant="subH2" style={{marginRight: "auto"}}>
                        Когда использовать {data.name}
                    </Text>
                    <RoundButton
                        m='x2'
                        flat
                        size="x-small"
                        icon={open ? MdKeyboardArrowUp : MdKeyboardArrowDown}
                        onClick={() => setOpen(!open)}
                    />
                </Layout>
                {open && (
                    <Text size="mini">
                        {toArray(data.useCasesDescription).flatMap(d => [d, <br/>])}
                        <br/>
                        <b>{data.useCasesExplanation}</b>
                        <br/><br/>
                        <b>{data.positiveExample}</b>(Утверждение)<br/>
                        {data.positiveExampleTranslate}
                        <br/><br/>
                        <b>{data.questionExample}</b>(Вопрос)<br/>
                        {data.questionExampleTranslate}
                        <br/><br/>
                        <b>{data.negativeExample}</b>(Отрицание)<br/>
                        {data.negativeExampleTranslate}
                        <br/><br/>
                    </Text>
                )}
            </Layout>
            <Layout flex justifyContentCenter m="x11">
                <EllipseButton bkg="accident" text="Next" size="x-small"/>
            </Layout>
        </div>
    )
}

export default Tens;