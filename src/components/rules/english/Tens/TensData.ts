export default interface TensData {
    name: string
    description: string
    positive: string
    question: string
    negative: string
    helperWords: string
    useCasesDescription: string
    useCasesExplanation: string
    positiveExample: string
    positiveExampleTranslate: string
    questionExample: string
    questionExampleTranslate: string
    negativeExample: string
    negativeExampleTranslate: string
}