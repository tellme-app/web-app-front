import React from "react";
import style from "./grammarCard.module.scss";
import WordButton from "../core/wordButton";
import classNames from "classnames";
import useStyle from "../../style/useStyle";

interface Props {
    clearProgress?: number
    preciselyProgress?: number
    show?: boolean
}

const ScoreButtons = (props: Props) => {
    const s = useStyle();
    return (
        <div className={classNames(style.scoreButtonsContainer, s.m.x4)}>
            {props.show && <WordButton word="Clear" progress={props.clearProgress} className={s.m.x2}/>}
            {props.show && <WordButton word="Precisely" progress={props.preciselyProgress} className={s.m.x2}/>}
        </div>
    )
}

export default ScoreButtons;