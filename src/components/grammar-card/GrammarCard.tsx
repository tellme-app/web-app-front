import React, {useEffect, useState} from "react";
import style from "./grammarCard.module.scss";
import PhraseContainer from "../basic/phrase";
import Phrase from "../basic/phrase/Phrase";
import ContentCard from "../core/contentCard";
import useStyle from "../../style/useStyle";
import ScoreButtons from "./ScoreButtons";
import Translates from "./Translates";
import {MdKeyboardArrowDown, MdKeyboardArrowUp} from "react-icons/all";
import classNames from "classnames";
import HelpButtons from "./HelpButtons";
import VoiceActions from "./VoiceActions";
import Translate from "../../models/Translate";
import DeprecatedText from "../core/deprecated-text";
import Sentence from "../../models/Sentence";
import ContextSentences from "./ContextSentences";
import {fetchSynthAudio, fetchUserAudio, getPhraseContext, getTranscript, sendAudio} from "./requests";
import {createAudioRecorder, playAudio} from "../../auidio";
import SpeechResponse from "../../models/SpeechResponse";
import StyleColors from "../../style/StyleColors";
import RoundButton from "../core/buttons/round-button";
import PageContent from "../core/containers/page-content";
import FilledLoading from "../core/states/FilledLoading";
import {Collapse} from "../core/animation/presets/Collapse";


interface Props {
    phrase: string
    contexts: number
    words: number
}


const chooseStyle = (styleName: string, colors: StyleColors) => {
    if (styleName === 'regular') return colors.primary
    if (styleName === 'accident') return colors.accident
    else return colors.error
}

const GrammarCard = (props: Props) => {
    const {phrase} = props;
    const s = useStyle();

    const [init, setInit] = useState(true)
    const [initError, setInitError] = useState("")
    const [loading, setLoading] = useState(false)
    const [showContextList, setShowContextList] = useState(false)
    const [recorded, setRecorded] = useState("")
    const [stop, setStop] = useState<{ call: null | (() => void) }>({call: null})
    const [recordedData, setRecordedData] = useState<SpeechResponse | null>(null)

    const [translates, setTranslates] = useState<Translate[]>([])
    const [transcript, setTranscript] = useState("")

    useEffect(() => {
        Promise.all([
            getPhraseContext(phrase, props.contexts, props.words),
            getTranscript(phrase)
        ])
            .then(([translate, transcript]) => {
                setTranslates(translate.data)
                setTranscript(transcript.data.transcription)
            })
            .catch(() => setInitError("Упс, у нас ошибочка :("))
            .then(() => setInit(false))
    }, [phrase, props.contexts, props.words])

    const getAndPlayAudio = (phrase: string) => {
        setLoading(true)
        fetchSynthAudio(phrase).then(r => playAudio(r, 'audio/wav'))
            .catch(() => console.error("Ну ошибка и чо, кто ее смотреть будет?"))
            .then(() => setLoading(false))
    }

    const hear = () => {
        setLoading(true)
        fetchUserAudio(recordedData?.id_enhanced || '')
            .then(r => playAudio(r, 'audio/wav'))
            .catch(() => console.error("Ну ошибка и чо, кто ее смотреть будет?"))
            .then(() => setLoading(false))
    }

    const startRecord = () => {
        setLoading(true)
        createAudioRecorder("audio/wav")
            .then(recorder => {
                setStop({call: recorder.stop})
                return recorder.start()
            })
            .then(blob => {
                setRecorded(window.URL.createObjectURL(blob))
                return sendAudio(blob, phrase)
            })
            .then(({data}) => setRecordedData(data))
            .catch(() => {
                console.error("Ну ошибка и чо, кто ее смотреть будет?")
                setRecorded("")
            })
            .then(() => setLoading(false))
            .then(() => setStop({call: null}))
    }

    const onMicClick = () => {
        if (stop.call && !recorded) {
            stop.call();
        } else if (!!recorded) {
            setRecorded("")
            setRecordedData(null)
        } else {
            startRecord();
        }
    }

    if (init) return <FilledLoading/>
    if (initError) return <DeprecatedText className={s.p.x4}>{initError}</DeprecatedText>
    if (!translates.length) return (
        <DeprecatedText className={s.p.x4}>
            {`К глубокому сожалению, фразы "${phrase}" нет в нашем словаре :(`}
        </DeprecatedText>
    )

    const sentences: Sentence[][][] = []
    for (let t of translates)
        for (let ctx of t.contexts)
            sentences.push(ctx)

    return (
        <>
            <PageContent flex justifyContentCenter>
                <ContentCard className={s.p.x3}>
                    <div
                        className={classNames(style.imageBox, s.color.primary)}
                        style={{backgroundImage: `url("/backend/get_img?keywords=${props.phrase}")`}}
                    >
                        {recordedData ? (
                            <PhraseContainer>
                                {recordedData.textResponse.map(sen => (
                                    <Phrase className={chooseStyle(sen.style, s.color)}>{sen.text}</Phrase>
                                ))}
                                <p/>
                                [{recordedData.transcriptResponse.map(sen => (
                                <Phrase variant="tip" className={chooseStyle(sen.style, s.color)}>
                                    {sen.text}
                                </Phrase>
                            ))}]
                            </PhraseContainer>
                        ) : (
                            <PhraseContainer>
                                <Phrase className={s.color.inverse}>{props.phrase}</Phrase>
                                <p/>
                                <Phrase className={s.color.inverse} variant="tip">[{transcript}]</Phrase>
                            </PhraseContainer>
                        )
                        }
                        <ScoreButtons
                            show={!!recordedData}
                            clearProgress={recordedData?.clear || 0}
                            preciselyProgress={recordedData?.precisely || 0}
                        />
                    </div>
                    <div className={classNames(style.translateBlock, s.p.x2)}>
                        <Translates variants={translates.map(t => t.name)}/>
                        <HelpButtons
                            disabledPlayExample={loading}
                            disabledPlayYourself={loading || !recordedData?.id_enhanced}
                            onPlayExample={() => getAndPlayAudio(phrase)}
                            onPlayYourself={hear}
                            showPlayYourself={!!recorded}
                        />
                    </div>
                    <div className={classNames(style.translateBlock, s.p.x2)}>
                        <span/>
                        <RoundButton
                            flat
                            icon={showContextList ? MdKeyboardArrowUp : MdKeyboardArrowDown}
                            onClick={() => setShowContextList(!showContextList)}
                        />
                    </div>
                    <Collapse open={showContextList}>
                        <ContextSentences
                            onPlayAudio={getAndPlayAudio}
                            disabled={loading}
                            sentences={sentences}
                        />
                    </Collapse>
                </ContentCard>
            </PageContent>
            <div className={style.emptyBlock}/>
            <VoiceActions
                state={stop.call ? "stop" : "play"}
                disabledMic={(loading && !stop.call) || (!!stop.call && !!recorded)}
                disabledHear={loading || !recorded || !recordedData?.id_enhanced}
                disabledVolume={loading}
                onVolumeClick={() => getAndPlayAudio(phrase)}
                onHearClick={hear}
                onMicClick={onMicClick}
                isRecorded={!!recorded}
            />
        </>
    )
}

export default GrammarCard;
