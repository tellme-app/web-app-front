import {send} from "../../network/send";
import Translate from "../../models/Translate";
import SpeechResponse from "../../models/SpeechResponse";

export const getPhraseContext = (phrase: string, contexts: number, words: number) =>
    send<Translate[]>("POST", "/backend/get", {
        data: {en: phrase, cnt_words: words, cnt_context: contexts}
    })

export const getTranscript = (phrase: string) =>
    send<any>("POST", "/speech_processing/transcript", {data: {text: phrase}})

export const fetchSynthAudio = (phrase: string) =>
    send("POST", "/synth/synth", {data: {text: phrase}})
        .then(async ({response}) => parseStream(response));

export const fetchUserAudio = (phraseId: string) =>
    send("POST", "/backend/get_audio", {data: {text: phraseId}})
        .then(async ({response}) => parseStream(response));

export const parseStream = async (response: Response) => {
    if (!response.body) throw Error("No payload")
    const reader = response.body.getReader();
    const buffers = new Array<any>()
    while (true) {
        const s = await reader.read().then(stream => {
            buffers.push(stream.value)
            return stream;
        })
        if (s.done) break;
    }
    return buffers;
}

export const sendAudio = (blob: Blob, phrase: string) => {
    const formData = new FormData();
    const file = blobToFile(blob, "test.wav");
    formData.append("data", `{"text" : "${phrase}"}`)
    formData.append("file", file)
    return send<SpeechResponse>("POST", "/speech_processing/stt", {body: formData})
}

const blobToFile = (theBlob: Blob, fileName: string): File => {
    let b: any = theBlob;
    b.lastModifiedDate = new Date();
    b.name = fileName;
    return b as File;
}