import React from "react";
import style from "./grammarCard.module.scss";
import classNames from "classnames";
import useStyle from "../../style/useStyle";
import {MdVolumeUp} from "react-icons/all";
import Sentence from "../../models/Sentence";
import DeprecatedText from "../core/deprecated-text";
import StyleColors from "../../style/StyleColors";
import IconButton from "../core/buttons/icon-button";
import Layout from "../core/containers/layout";

interface Props {
    sentences: Sentence[][][]
    disabled: boolean
    onPlayAudio: (phrase: string) => void
}

const chooseStyle = (styleName: string, colors: StyleColors) =>
    styleName === 'accindent' ? colors.success : undefined

const ContextSentences = (props: Props) => {
    const s = useStyle();
    return (
        <div>
            {props.sentences.map((sentences) => {
                const sentence = sentences[0] || [];
                const translate = sentences[1] || [];
                const sentenceString = sentence.map(s => s.text).join(" ")
                return (
                    <Layout className={classNames(style.contextTranslateContainer, s.m.x2)}>
                        <IconButton
                            onClick={() => props.onPlayAudio(sentenceString)}
                            disabled={props.disabled}
                            icon={MdVolumeUp}
                        />
                        <div>
                            {sentence.map(sentence => (
                                <DeprecatedText variant="tip"
                                                className={classNames(chooseStyle(sentence.style, s.color), style.contextSentence)}>
                                    {sentence.text}
                                </DeprecatedText>
                            ))}
                            <p/>
                            {translate.map(sentence => (
                                <DeprecatedText
                                    variant="subTip"
                                    className={classNames(chooseStyle(sentence.style, s.color), style.contextSentenceTranslate)}>
                                    {sentence.text}
                                </DeprecatedText>
                            ))}
                        </div>
                    </Layout>
                )
            })}
        </div>
    )
}

export default ContextSentences;
