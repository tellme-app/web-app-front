import React from "react";
import classNames from "classnames";
import useStyle from "../../style/useStyle";
import DeprecatedText from "../core/deprecated-text";

interface Props {
    variants: string[]
}

const Translates = (props: Props) => {
    const {variants} = props
    const s = useStyle();
    if (!variants.length) return <></>;

    const other = [...variants]
    other.splice(0, 1);

    return (
        <div>
            <DeprecatedText variant={"header"} className={classNames(s.color.main)}>
                {variants[0]}
            </DeprecatedText>
            <p/>
            <DeprecatedText variant={"tip"} className={s.color.light}>
                {other.join(", ")}
            </DeprecatedText>
        </div>
    )
}

export default Translates;