import React from "react";
import style from "./grammarCard.module.scss";
import {MdHearing, MdVolumeUp} from "react-icons/all";
import IconButton from "../core/buttons/icon-button";

interface Props {
    onPlayExample: () => void
    onPlayYourself: () => void
    showPlayYourself: boolean
    disabledPlayExample: boolean
    disabledPlayYourself: boolean
}

const HelpButtons = (props: Props) => (
    <div className={style.helpButtonsContainer}>
        <IconButton
            disabled={props.disabledPlayExample}
            onClick={props.onPlayExample}
            icon={MdVolumeUp}
        />
        {props.showPlayYourself && <IconButton
            disabled={props.disabledPlayYourself}
            onClick={props.onPlayYourself}
            icon={MdHearing}
        />}
    </div>
)

export default HelpButtons;