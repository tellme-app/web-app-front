import React from 'react';
import {MdHearing, MdMic, MdReplay, MdStop, MdVolumeUp} from "react-icons/all";
import style from './grammarCard.module.scss'
import classNames from "classnames";
import useStyle from "../../style/useStyle";
import SmallIconButton from "../core/smallIconButton";

export interface VoiceActionsProps {
    onHearClick: () => void,
    onMicClick: () => void,
    onVolumeClick: () => void,
    state: "stop" | "play"
    disabledMic?: boolean
    disabledVolume: boolean
    disabledHear: boolean
    isRecorded: boolean
}


const VoiceActions = (props: VoiceActionsProps) => {
    const s = useStyle();
    const isPlay = props.state === "play";
    const micButtonVariant = () => {
        if (props.isRecorded)
            return MdReplay;
        if (isPlay)
            return MdMic;
        return MdStop;
    }
    return (
        <div className={style.voiceActionsContainer} {...props}>
            <div className={style.voiceActionsWrapper}>
                <SmallIconButton
                    onClick={props.onHearClick}
                    className={classNames(s.color.primary, style.voiceActionIcon)}
                    Variant={MdHearing}
                    disabled={props.disabledHear}
                />
                <SmallIconButton
                    onClick={props.onMicClick}
                    className={classNames(s.color.primary, style.voiceActionMic)}
                    Variant={micButtonVariant()}
                    disabled={props.disabledMic}
                />
                <SmallIconButton
                    className={classNames(s.color.primary, style.voiceActionIcon)}
                    onClick={props.onVolumeClick}
                    Variant={MdVolumeUp}
                    disabled={props.disabledVolume}
                />
            </div>
        </div>
    )
}

export default VoiceActions;
