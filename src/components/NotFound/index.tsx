import React from 'react'
import s from './NotFound.module.scss'
import {Link} from "react-router-dom";

const NotFound = () => {
    return(
        <div className={s.container}>
            <h1>Ooops!<br/> Nothing found here!</h1>
            <p>Вернуться на <Link to="/">главную</Link> </p>

        </div>
    )
}

export default NotFound;
