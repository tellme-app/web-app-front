import React, {useContext, useState} from "react";
import styles from "./header.module.scss"
import classNames from "classnames";
import {AuthContext} from "../../auth";
import useStyle from "../../style/useStyle";
import Button from "../core/buttons";
import Text from "../core/text";
import logo from "./static/text_logo.png";
import {Link, useHistory} from "react-router-dom";
import {send} from "../../network/send";
import {useSnack} from "../basic/global-snack";
import AuthContextData from "../../auth/AuthContextData";
import CustomLink from "../core/custom-link";

export interface HeaderProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    sticky?: boolean
    actionWrapperProps?: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>
}

const Header = ({sticky, actionWrapperProps, ...props}: HeaderProps) => {
    const [auth, setAuth] = useContext(AuthContext)
    const s = useStyle()
    const history = useHistory()
    const snack = useSnack()

    const [logouting, setLogouting] = useState(false)

    const onLogout = () => {
        setLogouting(true)
        send('GET', '/auth/logout')
            .catch((e) => snack.show('Logout with errors: ' + String(e?.data?.message || '-'), "error"))
            .then(() => setAuth(new AuthContextData(false, auth.data)))
            .then(() => history.push('/login'))
            .then(() => setLogouting(false))
    }

    return (
        <div {...props} className={classNames(s.bkg.primary, s.p.x2, styles.root, sticky && styles.sticky)}>
            <a href="/">
                <img src={logo} alt="SpeakaTalka" className={styles.textLogo}/>
            </a>
            <div className={classNames(styles.actionsWrapper, actionWrapperProps?.className)}>
                {props.children}
                {auth.loggedIn ? (
                    <Button
                        disabled={logouting}
                        outlined flat
                        mRight='x1'
                        text='Log out'
                        size='nano'
                        preset='inverse'
                        onClick={onLogout}
                    />
                ) : (
                    <>
                        <CustomLink to='/login'>
                            <Button preset='light' mRight='x1' outlined flat size='nano'>
                                <Text variant='action' bold>
                                    Log In
                                </Text>
                            </Button>
                        </CustomLink>
                        <CustomLink to='/signup'>
                            <Button mRight='x1' preset='accident' flat text='Sign Up' size='nano'/>
                        </CustomLink>
                    </>
                )
                }
            </div>
        </div>
    )
}

export default Header