import React, {PropsWithChildren} from "react";
import DeprecatedText from "../../core/deprecated-text";
import {DeprecatedTextProps} from "../../core/deprecated-text/deprecatedText";


const Phrase = (props: PropsWithChildren<DeprecatedTextProps>) => (
    <DeprecatedText
        variant="headerNorm"
        font="sourceSansPro"
        {...props}
        className={props.className}
    />
)

export default Phrase;
