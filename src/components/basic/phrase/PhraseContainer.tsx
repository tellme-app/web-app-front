import React from "react";
import style from './phrase.module.scss'
import classNames from "classnames";
import useStyle from "../../../style/useStyle";

const PhraseContainer = (props: React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>) => {
    const s = useStyle();
    return (
        <div
            {...props}
            className={classNames(style.container, s.p.x4, props.className)}
        />
    )
}

export default PhraseContainer;
