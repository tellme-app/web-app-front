import React, {MouseEvent, useEffect, useRef} from "react";
import style from "./snackbar.module.css";
import classNames from "classnames";

export interface SnackbarProps extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    open?: boolean,
    vertical?: "top" | "bottom"
    horizontal?: "left" | "right"
    top?: number
    bottom?: number
    right?: number
    left?: number
    onClose?: () => void
    autoCloseDelay?: number
    autoClose?: boolean
    closeOnClick?: boolean
    closeOnClickEnableDelay?: number
}

export const Snackbar = (
    {
        open = false,
        vertical = "bottom",
        horizontal = "left",
        autoCloseDelay = 5000,
        closeOnClickEnableDelay = 250,
        ...props
    }: SnackbarProps
) => {
    const {top, bottom, left, right, autoClose} = props;
    const delayTimerId = useRef<any>(null)
    const openDate = useRef<number>()

    if (!open) openDate.current = undefined
    else if (!openDate.current) openDate.current = Date.now()

    useEffect(() => {
        if (!autoClose || !props.onClose) return;
        if (delayTimerId.current) clearTimeout(delayTimerId.current)
        if (open) delayTimerId.current = setTimeout(props.onClose, autoCloseDelay)
        return () => clearTimeout(delayTimerId.current)
        //eslint-disable-next-line
    }, [open])

    const classes = classNames(
        style[horizontal],
        style[vertical],
        style.root,
        props.className
    );

    const onClick = (e: MouseEvent<HTMLDivElement>) => {
        if (
            props.closeOnClick &&
            props.onClose &&
            openDate.current &&
            Date.now() - openDate.current > closeOnClickEnableDelay
        ) props.onClose()
        if (props.onClick) props.onClick(e)
    }

    return (
        <div
            {...props}
            onClick={onClick}
            className={classes}
            style={{top, left, right, bottom, ...props.style}}
        />
    )
}