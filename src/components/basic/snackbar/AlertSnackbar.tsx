import React, {ReactNode} from "react";
import Alert, {AlertProps} from "../../core/alert/Alert";
import style from "./snackbar.module.css";
import classNames from "classnames";
import {Transparency} from "../../core/animation";
import {Popping} from "../../core/animation/presets/Popping";
import {Snackbar, SnackbarProps} from "./Snackbar";
import AlertVariant from "../../core/alert/AlertVariant";
import {TransitionProps} from "../../core/animation/animationTypes";

export interface AlertSnackbarProps extends SnackbarProps {
    text?: ReactNode
    variant?: AlertVariant
    onClosed?: () => void
    alertProps?: AlertProps
    poppingProps?: TransitionProps
    transparencyProps?: TransitionProps
}

export const AlertSnackbar = ({open = false, ...props}: AlertSnackbarProps) => {

    const alertProps = {
        text: props.text,
        variant: props.variant,
        ...props.alertProps
    }

    return (
        <Snackbar open={open} {...props}>
            <Popping
                open={open}
                speed={{open: 500, close: 400}}
                className={style.wrapper}
                openMountAnimated
                {...props.poppingProps}
            >
                <Transparency
                    open={open}
                    unmountAfterClose
                    openMountAnimated
                    speed={250}
                    delay={{open: 250}}
                    {...props.transparencyProps}
                    onCloseEnd={props.onClosed}
                >
                    <Alert {...alertProps} className={classNames(style.alert, props.alertProps?.className)}/>
                </Transparency>
            </Popping>
        </Snackbar>
    )
}