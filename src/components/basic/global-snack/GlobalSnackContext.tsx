import React, {ReactNode, useRef, useState} from 'react'
import {GlobalSnack} from "./GlobalSnack";
import {SnackBarState} from "./useSnack";

interface GlobalSnackContextProps {
    children: ReactNode
}

interface GlobalSnackContextState {
    getId: () => number,
    stack: SnackBarState[]
    setStack: (state: SnackBarState[]) => void
}

const defaultContextState: GlobalSnackContextState = {
    getId: () => 0,
    stack: [],
    setStack: () => []
}

export const GlobalSnackContext = React.createContext<GlobalSnackContextState>(
    defaultContextState
)

export const GlobalSnackProvider = (props: GlobalSnackContextProps) => {
    const counter = useRef<number>(1)
    const [stack, setStack] = useState<SnackBarState[]>([])
    return (
        <GlobalSnackContext.Provider
            {...props}
            value={{
                getId: () => counter.current++,
                stack,
                setStack
            }}
        >
            <GlobalSnack/>
            {props.children}
        </GlobalSnackContext.Provider>
    )
}
