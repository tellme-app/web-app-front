import React from "react";
import {AlertSnackbar, AlertSnackbarProps, Snackbar} from "../snackbar";
import {useSnack} from "./useSnack";
import style from "./snackbar.module.css"
import {Collapse} from "../../core/animation/presets/Collapse";
import Portal from "../../core/portal/Portal";

export interface GlobalSnackProps extends AlertSnackbarProps {
    max?: number
}

export const GlobalSnack = ({max = 10, ...props}: GlobalSnackProps) => {
    const {stack, remove, close, collapse} = useSnack()
    let opened = 0;
    return (
        <Portal>
            <Snackbar open={!!stack.length} className={style.root}>
                <div className={style.snacksContainer}>
                    {stack.map((s) => {
                        const open = s.state === "open"
                        if (open) ++opened
                        if (opened > max && open) return null
                        return (
                            <Collapse
                                key={s.id}
                                maxHeight={300}
                                open={s.state !== "collapse"}
                                onCloseEnd={() => remove(s.id)}
                                speed={600}
                            >
                                <AlertSnackbar
                                    autoClose
                                    closeOnClick
                                    autoCloseDelay={5000}
                                    className={style.alertSnackbar}
                                    {...props}
                                    {...s.props}
                                    transparencyProps={{unmountAfterClose: false, ...props.transparencyProps, ...s.props}}
                                    open={open}
                                    onClose={() => close(s.id)}
                                    onClosed={() => collapse(s.id)}
                                />
                            </Collapse>
                        )
                    })}
                </div>
            </Snackbar>
        </Portal>
    )
}