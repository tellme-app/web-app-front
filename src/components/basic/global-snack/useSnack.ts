import {useContext, useRef} from "react";
import {GlobalSnackContext} from "./GlobalSnackContext";
import {AlertSnackbarProps} from "../snackbar";
import AlertVariant from "../../core/alert/AlertVariant";

type SnackState = "open" | "closing" | "collapse";

export interface SnackBarState {
    id: number
    state: SnackState
    props: AlertSnackbarProps
}

interface SnackActions {
    stack: SnackBarState[]
    show: (message: string, opt?: AlertSnackbarProps | AlertVariant) => number
    close: (id: number) => void
    collapse: (id: number) => void
    remove: (id: number) => void
}


export const useSnack = (): SnackActions => {
    const {getId, stack, setStack} = useContext(GlobalSnackContext)
    const ref = useRef(stack)
    ref.current = stack

    const add = (text: string, opt: AlertSnackbarProps | AlertVariant = "success") => {
        const props = typeof opt === "object" ? {text, ...opt} : {text, variant: opt}
        const id = getId();
        setStack([...ref.current, {id, state: "open", props}])
        return id
    }

    const remove = (id: number) => setStack(ref.current.filter(s => s.id !== id))

    const setSnackState = (id: number, state: SnackState, forbidden: SnackState[] = []) => {
        let updated = false;

        const newStack: SnackBarState[] = [];
        for (let s of ref.current) {
            if (s.id !== id) newStack.push(s);
            else if (forbidden.some(f => f === s.state)) return
            else {
                newStack.push({...s, state})
                updated = true
            }
        }

        if (updated) setStack(newStack)
    }

    const close = (id: number) => setSnackState(id, "closing", ["collapse", "closing"])
    const collapse = (id: number) => setSnackState(id, "collapse", ["collapse"])

    return {show: add, close, collapse, remove, stack}
}