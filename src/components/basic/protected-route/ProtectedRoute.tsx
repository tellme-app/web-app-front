import React from "react";
import {Route, Redirect} from "react-router-dom";
import RuleName from "../../../models/auth/RuleName";
import {RouteProps} from "react-router";
import {useAuth} from "../../../auth";

export interface ProtectedRouteProps extends RouteProps {
    rule?: RuleName | RuleName[]
}

const ProtectedRoute = (
    {rule, ...props}: ProtectedRouteProps
) => {
    const auth = useAuth();
    if (!auth.loggedIn)
        return <Redirect to='/'/>;
    const checkedRules = rule ? typeof rule === 'string' ? [rule] : rule : []
    if (checkedRules.length && !auth.hasRule(...checkedRules))
        return <Redirect to='/'/>;
    return <Route {...props} />
}

export default ProtectedRoute;