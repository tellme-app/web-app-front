import React from "react";

export interface LogoProps extends React.SVGProps<SVGSVGElement> {
    fill?: string
    size?: number
}

const Logo = ({fill = '#000000', size = 100, ...props}: LogoProps) => (
    <svg version="1.0" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 240 240"
        width={size} height={size} preserveAspectRatio="xMidYMid meet"
         {...props}
    >
        <g transform="translate(0.000000,240) scale(0.100000,-0.100000)"
           fill="#000000" stroke="none">
            <path d="M991 2365 c-435 -77 -797 -396 -915 -806 -39 -138 -49 -232 -43 -422
7 -222 41 -367 126 -535 76 -149 229 -322 369 -415 282 -189 665 -226 993 -97
119 47 343 34 503 -30 155 -61 268 -31 332 90 18 33 19 68 19 610 0 509 -2
585 -18 662 -49 241 -147 435 -306 601 -159 166 -341 270 -571 327 -113 28
-368 36 -489 15z m-313 -1015 c69 -42 90 -120 50 -191 -50 -89 -185 -93 -238
-6 -26 41 -26 112 -1 153 37 60 127 81 189 44z m1143 -3 c45 -30 71 -82 65
-130 -17 -145 -208 -175 -267 -42 -56 126 88 249 202 172z m-581 -182 c92 -47
104 -164 24 -235 -60 -53 -162 -37 -204 33 -24 39 -26 107 -5 148 32 61 121
87 185 54z"/>
        </g>
    </svg>
)

export default Logo;