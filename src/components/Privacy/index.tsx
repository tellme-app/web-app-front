import React, {FunctionComponent} from 'react'
import PrivacyText from "./PrivacyText";

const Privacy: FunctionComponent = () => {
    return (
            <PrivacyText/>
    )
}

export default Privacy;
