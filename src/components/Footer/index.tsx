import React from 'react'
import s from './Footer.module.scss'
import {Link} from "react-router-dom";
import {FaVk, FaOdnoklassniki, FaInstagram, FaFacebook, FaTwitter, FaTelegram, FaAngellist} from "react-icons/fa";
import {ReactComponent as F6S} from './static/f6s.svg'
const Footer = () => {
    return (
        <div className={s.container}>
            <div className={s.leftPart}>
                <a href="https://vk.com/speakatalka" rel="noopener noreferrer" target="_blank">
                    <FaVk/>
                </a>
                <a href="http://instagram.com/speakatalka" target="_blank" rel="noopener noreferrer">
                    <FaInstagram/>
                </a>
                <a href="http://facebook.com/speakatalka.official" target="_blank" rel="noopener noreferrer">
                    <FaFacebook/>
                </a>
                <a href="https://twitter.com/speakatalka" target="_blank" rel="noopener noreferrer">
                    <FaTwitter/>
                </a>
                <a href="https://t.me/speakatalka" target="_blank" rel="noopener noreferrer">
                    <FaTelegram/>
                </a>
                <a href="https://ok.ru/group/58241338441840" target="_blank" rel="noopener noreferrer">
                    <FaOdnoklassniki/>
                </a>
                <a href="https://angel.co/company/speakatalka" target="_blank" rel="noopener noreferrer">
                    <FaAngellist/>
                </a>
                <a href="https://www.f6s.com/speakatalka" target="_blank" rel="noopener noreferrer">
                    <F6S/>
                </a>
            </div>
            <div className={s.rightPart}>
                <Link to="/privacy">Политика кофиденциальности</Link>
                <a className={s.email} href="mailto:team@speakatalka.com">team@speakatalka.com</a>
            </div>
        </div>
    )
}

export default Footer
