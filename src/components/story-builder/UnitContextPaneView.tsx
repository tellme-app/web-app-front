import React from "react";
import UnitContext from "../../models/story/UnitContext";
import Text from "../core/text";
import Splitter from "../core/splitter";
import Chip from "../core/buttons/chip";
import Layout from "../core/containers/layout";
import styles from "./storyBuilder.module.scss"

interface UnitContextPaneViewProps {
    context: UnitContext
}

const UnitContextPaneView = ({context, ...props}: UnitContextPaneViewProps) => {
    return (
        <div>
            {context.values?.map(v => <Text
                    className={styles.wordBrake}
                    m='x1'
                    elm='div'
                    bkg={v.value ? undefined : 'error'}
                    variant='subTip'
                    bold
                >
                    {v.value || '-'}
                </Text>
            )}
            <Splitter mTop={"x1"}/>
            <Layout p='x1'>
                {context.examples?.map(ex => (
                    <Chip size='nano' m='x1' bkg={!ex.text ? 'error' : ex.isConcrete ? 'primary' : 'light'}>
                        {ex.text}
                    </Chip>
                ))}
            </Layout>
        </div>
    )
}


export default UnitContextPaneView;