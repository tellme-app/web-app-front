import StoryUnit from "../../models/story/StoryUnit";
import UnitsResponse from "../../models/story/UnitsResponse";
import UnitContext from "../../models/story/UnitContext";

export interface Coordinate {
    x: number,
    y: number
}

export interface ArrowProps {
    fromUnitId?: number
    fromCtxId?: number
    toUnitId?: number
    toCtxId?: number
    distance?: number
    color: number
    center: Coordinate
    from: Coordinate
    to: Coordinate
}

export const arrayElmUpdate = <T>(updated: T, index: number, content: T[]): T[] => {
    const newContent = [...content]
    newContent[index] = updated
    return newContent
}

export const calcMinSizes = (content: UnitsResponse) => {
    const max = content.units?.reduce((prev, curr) => ({
        xMax: Math.max(curr.x || 0, prev.xMax),
        yMax: Math.max(curr.y || 0, prev.yMax),
    }), {
        xMax: 1000,
        yMax: 1000
    })
    return {
        minWidth: (max?.xMax || 0) * 1.2,
        minHeight: (max?.yMax || 0) * 1.2
    }
}

export const createArrowProps = (
    units: StoryUnit[],
    unitRefs: HTMLDivElement[],
    contexts: UnitContext[],
    contextsRefs: HTMLDivElement[],
    excludeType?: 'unit' | 'ctx',
    excludeId?: number
) => {
    const directions: ArrowProps[] = []
    const excludeUnitId = excludeType === "unit" ? excludeId : 0
    const excludeCtxId = excludeType === "ctx" ? excludeId : 0

    units.forEach((unit, unitIndex) => {
        if (unit.id === excludeUnitId) return

        const unitRef = unitRefs[unitIndex]
        if (!unitRef) return

        contexts.forEach((ctx, ctxIndex) => {
            if (ctx.unitId !== unit.id || excludeCtxId === ctx.id) return
            const ctxRef = contextsRefs[ctxIndex]
            if (!ctxRef) return
            const fromTo = getDirection(
                {x: unit.x || 0, y: unit.y || 0},
                {x: unitRef.clientWidth, y: unitRef.clientHeight},
                {x: ctx.x || 0, y: ctx.y || 0},
                {x: ctxRef.clientWidth, y: ctxRef.clientHeight}
            )
            directions.push({
                color: unitIndex,
                ...fromTo,
                fromUnitId: unit.id,
                toCtxId: ctx.id,
                center: center(fromTo.from, fromTo.to)
            })
        })

        if (!unit.to) return
        const referUnit = unitById(unit.to, units)
        if (!referUnit || referUnit.unit.id === excludeUnitId) return;
        const referRef = unitRefs[referUnit.index]
        if (!referRef) return
        const fromTo = getDirection(
            {x: unit.x || 0, y: unit.y || 0},
            {x: unitRef.clientWidth, y: unitRef.clientHeight},
            {x: referUnit.unit.x || 0, y: referUnit.unit.y || 0},
            {x: referRef.clientWidth, y: referRef.clientHeight}
        )
        directions.push({
            color: unitIndex,
            ...fromTo,
            fromUnitId: unit.id,
            toUnitId: referUnit.unit.id,
            center: center(fromTo.from, fromTo.to)
        })
    })

    contexts.forEach((ctx, ctxIndex) => {
        if (ctx.id === excludeCtxId || ctx.to === excludeUnitId) return
        const referUnit = unitById(ctx.to || 0, units)
        if (!referUnit) return
        const ctxRef = contextsRefs[ctxIndex]
        const referRef = unitRefs[referUnit.index]
        if (!referRef || !ctxRef) return
        const fromTo = getDirection(
            {x: ctx.x || 0, y: ctx.y || 0},
            {x: ctxRef.clientWidth, y: ctxRef.clientHeight},
            {x: referUnit.unit.x || 0, y: referUnit.unit.y || 0},
            {x: referRef.clientWidth, y: referRef.clientHeight}
        )
        directions.push({
            color: ctxIndex,
            ...fromTo,
            fromCtxId: ctx.id,
            toUnitId: referUnit.unit.id,
            center: center(fromTo.from, fromTo.to)
        })
    })

    return directions
}

const getDirection = (fromXY: Coordinate, fromSize: Coordinate, toXY: Coordinate, toSize: Coordinate) => {
    const fromPoints = getBoundaryPoints({
        x: fromXY.x || 0,
        y: fromXY.y || 0
    }, fromSize.x, fromSize.y)
    const pointsTo = getBoundaryPoints({
        x: toXY.x || 0,
        y: toXY.y || 0
    }, toSize.x, toSize.y)

    const from = {x: 0, y: 0}
    const to = {x: 0, y: 0}
    let currDist: number | null = null

    fromPoints.forEach(fromPoint => {
        pointsTo.forEach(toPoint => {
            const dist = distance(fromPoint, toPoint)
            if (currDist && currDist < dist) return;
            currDist = dist
            from.x = fromPoint.x
            from.y = fromPoint.y
            to.x = toPoint.x
            to.y = toPoint.y
        })
    })

    return {from, to, distance: currDist || 0}
}

const boundaryOffset = 10

const getBoundaryPoints = (pos: Coordinate, width: number, height: number): Coordinate[] => [
    {x: (pos.x + width * 0.2), y: pos.y - boundaryOffset},
    {x: (pos.x + width * 0.5), y: pos.y - boundaryOffset},
    {x: (pos.x + width * 0.8), y: pos.y - boundaryOffset},
    {x: (pos.x + width * 0.2), y: pos.y + height + boundaryOffset},
    {x: (pos.x + width * 0.5), y: pos.y + height + boundaryOffset},
    {x: (pos.x + width * 0.8), y: pos.y + height + boundaryOffset},
    {x: pos.x - boundaryOffset, y: pos.y + height * 0.2},
    {x: pos.x - boundaryOffset, y: pos.y + height * 0.5},
    {x: pos.x - boundaryOffset, y: pos.y + height * 0.8},
    {x: pos.x + width + boundaryOffset, y: pos.y + height * 0.2},
    {x: pos.x + width + boundaryOffset, y: pos.y + height * 0.5},
    {x: pos.x + width + boundaryOffset, y: pos.y + height * 0.8},
]

const distance = (a: Coordinate, b: Coordinate) => Math.abs(Math.sqrt(
    Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2)
))

const unitById = (id: number, units: StoryUnit[]) => {
    for (let index = 0; index < units.length; index++) {
        const unit = units[index]
        if (unit.id === id) return {unit, index}
    }
}

const center = (from: Coordinate, to: Coordinate): Coordinate => ({
    x: (from.x + to.x) / 2,
    y: (from.y + to.y) / 2
})

export const createId = (dataSet: { id?: number }[]) => Math.max(...dataSet.map(d => d.id || 0), 0) + 1


