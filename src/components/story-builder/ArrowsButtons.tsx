import React, {MutableRefObject, useState} from "react";
import {ArrowProps} from "./utils";
import {MdClose} from "react-icons/all";
import Icon from "../core/icon";
import styles from './storyBuilder.module.scss'

interface Props {
    show: boolean
    setReferenceUpdate: MutableRefObject<((arrowProps: ArrowProps[]) => void) | undefined>
    onRemove: (arrow: ArrowProps) => void
}

const ArrowsButtons = (props: Props) => {
    const [arrows, setArrows] = useState<ArrowProps[]>([])
    props.setReferenceUpdate.current = setArrows
    return (
        <>
            {arrows.map((arrow, i) => (
                <Icon
                    key={i}
                    className={props.show ? styles.arrowButtonTransitionOn : styles.arrowButtonTransitionOff}
                    colorHover='dark'
                    color='light'
                    size='nano'
                    icon={MdClose}
                    onClick={e => {
                        if (!props.show) return
                        e.stopPropagation()
                        props.onRemove(arrow)
                    }}
                    style={{
                        position: 'absolute',
                        top: -8,
                        left: -8,
                        transform: `translate(${arrow.center.x}px, ${arrow.center.y}px)`
                    }}
                />
            ))}
        </>
    )
}


export default ArrowsButtons;