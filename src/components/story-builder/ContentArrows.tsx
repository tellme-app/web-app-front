import React, {MutableRefObject, useEffect, useRef} from "react";
import {ArrowProps} from "./utils";

const colors = ["#63ab71", "#537acb", "#FFB74D", "#E10050"]

interface ContentArrowsProps extends React.DetailedHTMLProps<React.CanvasHTMLAttributes<HTMLCanvasElement>, HTMLCanvasElement> {
    width: number
    height: number
    setReferenceUpdate: MutableRefObject<((arrowProps: ArrowProps[]) => void) | undefined>
}

const ContentArrows = (props: ContentArrowsProps) => {
    const ref = useRef<HTMLCanvasElement>(null)

    useEffect(() => {
        const cnv = ref.current?.getContext("2d");
        if (!cnv) return
        props.setReferenceUpdate.current = (arrowProps: ArrowProps[]) => {
            const arrowLen = 10;
            cnv.clearRect(0, 0, props.width, props.height)
            arrowProps.forEach(({from, to, color}) => {
                const dx = to.x - from.x;
                const dy = to.y - from.y;
                const angle = Math.atan2(dy, dx);
                const ang1 = angle - Math.PI / 5
                const ang2 = angle + Math.PI / 5
                cnv.beginPath();
                cnv.moveTo(from.x, from.y);
                cnv.lineTo(to.x, to.y);
                cnv.moveTo(to.x, to.y);
                cnv.lineTo(to.x - arrowLen * Math.cos(ang1), to.y - arrowLen * Math.sin(ang1));
                cnv.moveTo(to.x, to.y);
                cnv.lineTo(to.x - arrowLen * Math.cos(ang2), to.y - arrowLen * Math.sin(ang2));
                cnv.lineWidth = 2

                cnv.strokeStyle = colors[color % 4]
                cnv.lineCap = 'round'
                cnv.stroke();
                cnv.closePath()
            })
        }

    }, [props])

    return <canvas ref={ref} {...props}/>
}


export default ContentArrows;