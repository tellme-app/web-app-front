import React, {ReactNode, useEffect, useRef} from "react";
import Layout from "../core/containers/layout";
import Draggable, {DraggableData, DraggableEvent} from "react-draggable";
import styles from "./storyBuilder.module.scss";
import {MdDragHandle} from "react-icons/all";
import Icon from "../core/icon";
import classNames from "classnames";
import useStyle from "../../style/useStyle";
import {Coordinate} from "./utils";

interface DraggableContentProps {
    edit: boolean
    setRef: (ref: HTMLDivElement) => void
    focused: boolean
    flashed: boolean
    accident: boolean
    onClick: () => void
    onDoubleClick: () => void
    position: Coordinate
    onDragStart: (pos: Coordinate) => void
    onDragEnd: (pos: Coordinate) => void
    onDrag: (pos: Coordinate) => void
    children: ReactNode
}


const DraggableContent = ({position, ...props}: DraggableContentProps) => {
    const onDragEnd = (e: DraggableEvent, data: DraggableData) => {
        props.onDragEnd({x: data.x, y: data.y})
    }
    const s = useStyle()
    const timerId = useRef<NodeJS.Timeout>()
    const lastWidth = useRef(0)

    const onClick = (e: React.MouseEvent<'div', MouseEvent>) => {
        e.stopPropagation()
        props.onClick()
        if (timerId.current) {
            clearTimeout(timerId.current)
            timerId.current = undefined
            props.onDoubleClick()
        } else
            timerId.current = setTimeout(() => timerId.current = undefined, 350)
    }

    useEffect(() => () => {
        if (timerId.current)
            clearTimeout(timerId.current)
    }, [])

    return (
        <Draggable
            bounds="parent"
            onMouseDown={e => e.stopPropagation()}
            handle="strong"
            position={position}
            defaultClassName={classNames(styles.storyUnitDrag, props.focused && styles.zIndex)}
            onStop={onDragEnd}
            onDrag={(_, data) => props.onDrag({x: data.x, y: data.y})}
            onStart={(_, data) => props.onDragStart({x: data.x, y: data.y})}
        >
            <Layout
                boxShadow bkg='main'
                onClick={onClick}
                style={props.edit ? {width: Math.max(lastWidth.current, 250)} : undefined}
            >
                <div
                    className={styles.relative}
                    ref={ref => {
                        if (ref) {
                            props.setRef(ref)
                            if (!props.edit) lastWidth.current = ref.clientWidth
                        }
                    }}
                >
                    {props.focused && (
                        <div className={classNames(
                            styles.unitBorder,
                            props.flashed ? s.color.primaryDark : s.color.secondaryDark,
                            props.flashed && styles.unitFlashed)}
                        />
                    )}
                    {props.accident && (
                        <div className={classNames(
                            styles.unitBorder,
                            s.color.error,
                            styles.unitFlashed)}
                        />
                    )}
                    {props.flashed && <div className={classNames(styles.unitFlashed, s.color.secondaryDark)}/>}
                    <strong>
                        <Layout flex alignItemsCenter bkg='light'>
                            <Icon color='light' mLeft='auto' mRight='auto' icon={MdDragHandle}/>
                        </Layout>
                    </strong>
                    {props.children}
                </div>
            </Layout>
        </Draggable>
    )
}


export default DraggableContent;