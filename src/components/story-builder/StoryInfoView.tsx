import React from "react";
import Story from "../../models/story/Story";
import Text from "../core/text";
import EllipseButton from "../core/buttons/ellipse-button";
import styles from "./storyBuilder.module.scss"

interface StoryInfoViewProps {
    story: Story
    onEditClick: () => void
    disabled: boolean
}

const StoryInfoView = ({story, ...props}: StoryInfoViewProps) => {
    return (
        <>
            {story.description &&
            (
                <Text
                    mBottom='x4'
                    disableBottomIdent
                    className={styles.storyDescription}
                    variant="text2"
                >
                    {story.description}
                </Text>
            )}
            <EllipseButton
                m='x2'
                flat
                preset="secondaryLight"
                size="nano"
                text="Редактировать"
                onClick={props.onEditClick}
                disabled={props.disabled}
            />
        </>
    )
}


export default StoryInfoView;