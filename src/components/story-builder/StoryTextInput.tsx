import React from "react";
import classNames from "classnames";
import styles from "./storyBuilder.module.scss";
import useStyle from "../../style/useStyle";

const StoryTextInput = (props: React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>) => {
    const s = useStyle()
    return <input {...props} className={classNames(s.size.micro, styles.textInput, props.className)}/>
}

export default StoryTextInput