import React from "react";
import StoryUnit from "../../models/story/StoryUnit";
import Text from "../core/text";
import Layout from "../core/containers/layout";
import styles from "./storyBuilder.module.scss";

interface StoryUnitPaneViewProps {
    unit: StoryUnit
}

const StoryUnitPaneView = ({unit}: StoryUnitPaneViewProps) => {
    return (
        <Layout bkg='secondaryLight' p='x1' className={styles.wordBrake}>
            <Text color='none' elm='div' variant='subTip'>
                {unit.text}
            </Text>
        </Layout>
    )
}


export default StoryUnitPaneView