import React from "react";
import RoundButton from "../core/buttons/round-button";
import {MdClose} from "react-icons/all";


interface ArrowButtonProps {
    x: number
    y: number
    onClick: () => void
}

const ArrowButton = (props: ArrowButtonProps) => {
    return (
        <RoundButton
            icon={MdClose}
            style={{transform: `translate(${props.x}px, ${props.y}px)`}}
            size='nano'
            onClick={props.onClick}
        />
    )
}


export default ArrowButton;