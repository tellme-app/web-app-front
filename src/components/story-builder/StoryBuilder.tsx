import React from "react";
import {RouteComponentProps} from "react-router";
import StoryInfo from "./StoryInfo";
import TopHeader from "../TopHeader";
import StoryContent from "./StoryContent";


const StoryBuilder = (props: RouteComponentProps<{ storyId: string }>) => {
    const storyId = +props.match.params.storyId

    return (
        <>
            <TopHeader/>
            <StoryInfo storyId={storyId}/>
            <StoryContent storyId={storyId}/>
        </>
    )
}


export default StoryBuilder;