import React, {useState} from "react";
import StoryUnit from "../../models/story/StoryUnit";
import Text from "../core/text";
import Layout from "../core/containers/layout";
import useDeferCallback from "../../util/useDeferCallback";
import StoryTextInput from "./StoryTextInput";

interface StoryUnitPaneEditProps {
    unit: StoryUnit
    onUnitEdit: (unit: StoryUnit) => void
}

const StoryUnitPaneEdit = ({unit, ...props}: StoryUnitPaneEditProps) => {
    const [editedUnit, setEditedUnit] = useState(unit)
    const reset = useDeferCallback(() => props.onUnitEdit(editedUnit))
    return (
        <Layout bkg='secondaryLight' p='x1'>
            <Text
                autoFocus
                elm='div'
                bkg='main'
                variant='subTip'
            >
                <StoryTextInput
                    value={editedUnit.text}
                    onChange={e => {
                        reset()
                        setEditedUnit({...editedUnit, text: e.target.value})
                    }}
                />
            </Text>
        </Layout>
    )
}


export default StoryUnitPaneEdit