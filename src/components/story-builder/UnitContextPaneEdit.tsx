import React, {useState} from "react";
import UnitContext from "../../models/story/UnitContext";
import Text from "../core/text";
import Splitter from "../core/splitter";
import Chip from "../core/buttons/chip";
import Layout from "../core/containers/layout";
import useDeferCallback from "../../util/useDeferCallback";
import StoryTextInput from "./StoryTextInput";
import {MdAdd, MdCancel, MdRadioButtonChecked, MdRadioButtonUnchecked} from "react-icons/all";
import IconButton from "../core/buttons/icon-button";
import ContextValue from "../../models/story/ContextValue";
import {ButtonIcon, ButtonIconProps} from "../core/buttons";
import ContextExample from "../../models/story/ContextExample";
import {SpanProps, StyleProps} from "../props";
import styles from './storyBuilder.module.scss'
import {createId} from "./utils";

interface UnitContextPaneEditProps {
    edit: boolean
    context: UnitContext
    onCtxEdit: (unit: UnitContext) => void
}

const UnitContextPaneEdit = ({context, ...props}: UnitContextPaneEditProps) => {
    const [editedCtx, setEditedCtx] = useState(context)
    const reset = useDeferCallback(() => props.onCtxEdit(editedCtx))
    const values = editedCtx.values || []
    const examples = editedCtx.examples || []

    const onValueChange = (value: ContextValue, i: number) => {
        reset()
        const newValues = [...values]
        newValues[i] = value
        setEditedCtx({...editedCtx, values: newValues})
    }

    const onValueRemove = (i: number) => {
        reset()
        const newValues = [...values]
        newValues.splice(i, 1)
        setEditedCtx({...editedCtx, values: newValues})
    }

    const onExampleRemove = (i: number) => {
        reset()
        const newExamples = [...examples]
        newExamples.splice(i, 1)
        setEditedCtx({...editedCtx, examples: newExamples})
    }

    const onExampleChange = (example: ContextExample, i: number) => {
        reset()
        const newExamples = [...examples]
        newExamples[i] = example
        setEditedCtx({...editedCtx, examples: newExamples})
    }

    return (
        <div>
            {values.map((v, i) => (
                <Layout flex>
                    <Text m='x1' elm='div' variant='subTip' bold key={i} className={styles.fullWidth}>
                        <StoryTextInput
                            value={v.value}
                            onChange={e => onValueChange({...v, value: e.target.value}, i)}
                        />
                    </Text>
                    <IconButton
                        style={{marginLeft: 'auto'}}
                        icon={MdCancel}
                        size='nano'
                        onClick={() => onValueRemove(i)}
                    />
                </Layout>
            ))}
            <IconButton
                m='x1'
                icon={MdAdd}
                size='nano'
                onClick={() => onValueChange({id: createId(values), value: ""}, values.length)}
            />
            <Splitter mTop={"x1"}/>
            <Layout p='x1' className={styles.contextExampleContainer}>
                {examples?.map((ex, i) => (
                    <Chip size='nano' m='x1' bkg={ex.isConcrete ? 'primary' : 'light'}>
                        <ChipButton
                            icon={ex.isConcrete ? MdRadioButtonChecked : MdRadioButtonUnchecked}
                            position='left'
                            onClick={() => onExampleChange({...ex, isConcrete: !ex.isConcrete}, i)}
                        />
                        <StoryTextInput
                            value={ex.text}
                            onChange={e => onExampleChange({...ex, text: e.target.value}, i)}
                        />
                        <ChipButton
                            icon={MdCancel}
                            position='right'
                            onClick={() => onExampleRemove(i)}
                        />
                    </Chip>
                ))}
            </Layout>
            <IconButton
                m='x1'
                icon={MdAdd}
                size='nano'
                onClick={() => onExampleChange({
                    id: createId(examples),
                    text: "",
                    isConcrete: false
                }, examples.length)}
            />
        </div>
    )
}


export default UnitContextPaneEdit;

const ChipButton = (props: ButtonIconProps & StyleProps & SpanProps) => (
    <ButtonIcon
        colorHover='main'
        colorActive='main'
        color='main'
        {...props}
    />
)
