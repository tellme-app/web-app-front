import React from "react";
import Layout from "../core/containers/layout";
import {ButtonProps} from "../core/buttons/Button";
import {
    MdChatBubble,
    MdClose,
    MdDeleteForever,
    MdDone,
    MdEdit,
    MdQuestionAnswer,
    MdRedo,
    MdSave,
    MdSwapCalls,
    MdUndo
} from "react-icons/all";
import RoundButton from "../core/buttons/round-button";
import Text from "../core/text";
import EllipseButton from "../core/buttons/ellipse-button";
import styles from "./storyBuilder.module.scss"
import useStyle from "../../style/useStyle";
import classNames from "classnames";


interface StoryContentActionProps {
    disabled?: boolean
    onEditEnable: () => void
    onUnitEditOn?: () => void
    onUnitAdd?: () => void;
    onCtxAdd?: () => void;
    onUnitEditOff?: () => void
    onEditDisable?: () => void
    onSave?: () => void
    onUndo?: () => void
    onRedo?: () => void
    onDelete?: () => void
    arrowMode: boolean
    onArrowClick?: () => void
    addType?: 'ctx' | 'unit'
}

const StoryContentAction = (props: StoryContentActionProps) => {
    const s = useStyle()
    return (
        <>
            <div className={classNames(styles.opacityBkg, s.bkg.light)}/>
            <Layout p='x1' pLeft='x6' flex alignItemsCenter className={styles.storyActions}>
                {props.onEditDisable ? (
                    <>
                        <Layout flex>
                            <IconButtonProxy
                                preset='primary'
                                icon={MdSave}
                                disabled={props.disabled || !props.onSave}
                                onClick={props.onSave}
                            />
                            <IconButtonProxy
                                preset='light'
                                icon={MdUndo}
                                disabled={props.disabled || !props.onUndo}
                                onClick={props.onUndo}
                            />
                            <IconButtonProxy
                                preset='light'
                                icon={MdRedo}
                                disabled={props.disabled || !props.onRedo}
                                onClick={props.onRedo}
                            />
                            <IconButtonProxy
                                preset='accident'
                                icon={MdChatBubble}
                                disabled={props.disabled || !props.onUnitAdd}
                                onClick={props.onUnitAdd}
                                className={classNames(props.addType === 'unit' && styles.buttonFlashing)}
                            />
                            <IconButtonProxy
                                preset='accident'
                                icon={MdQuestionAnswer}
                                disabled={props.disabled || !props.onCtxAdd}
                                onClick={props.onCtxAdd}
                                className={classNames(props.addType === 'ctx' && styles.buttonFlashing)}
                            />
                            <IconButtonProxy
                                preset='error'
                                icon={MdDeleteForever}
                                disabled={props.disabled || !props.onDelete}
                                onClick={props.onDelete}
                            />
                            <IconButtonProxy
                                preset={'secondaryDark'}
                                icon={MdSwapCalls}
                                disabled={props.disabled || !props.onArrowClick}
                                onClick={props.onArrowClick}
                                className={classNames(props.arrowMode && styles.buttonFlashing)}
                            />
                            {!props.onUnitEditOff && <IconButtonProxy
                                preset='secondaryLight'
                                disabled={props.disabled || !props.onUnitEditOn}
                                icon={MdEdit}
                                onClick={props.onUnitEditOn}
                            />}
                            {props.onUnitEditOff && <IconButtonProxy
                                preset='primaryDark'
                                icon={MdDone}
                                disabled={props.disabled || !props.onUnitEditOff}
                                onClick={props.onUnitEditOff}
                            />}
                        </Layout>
                        <IconButtonProxy
                            mLeft='auto'
                            preset='light'
                            color='error'
                            icon={MdClose}
                            onClick={props.onEditDisable}
                        />
                    </>
                ) : (
                    <Layout inlineFlex alignItemsCenter>
                        <Text elm='span' variant='h6'>Структура</Text>
                        <EllipseButton
                            flat
                            m='x2'
                            onClick={props.onEditEnable}
                            preset="secondaryLight"
                            size="nano"
                            text="Редактировать"
                        />
                    </Layout>
                )
                }
            </Layout>
        </>
    )
}

const IconButtonProxy = (props: ButtonProps) => <RoundButton flat size='nano' m='x1' {...props}/>

export default StoryContentAction;