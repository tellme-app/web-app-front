import React, {useEffect, useRef, useState} from "react";
import FilledLoading from "../core/states/FilledLoading";
import LocalError from "../core/states/LocalError";
import Layout from "../core/containers/layout";
import StoryUnit from "../../models/story/StoryUnit";
import {useRequestStateBase} from "../../network/useRequestState";
import StoryContentAction from "./StoryContentAction";
import Splitter from "../core/splitter";
import styles from "./storyBuilder.module.scss"
import {arrayElmUpdate, ArrowProps, Coordinate, createArrowProps, createId} from "./utils";
import DraggableContent from "./DarggableContent";
import UnitContext from "../../models/story/UnitContext";
import UnitsResponse from "../../models/story/UnitsResponse";
import {sendSimple} from "../../network/send";
import ContentArrows from "./ContentArrows";
import UnitContextPaneView from "./UnitContextPaneView";
import StoryUnitPaneView from "./StoryUnitPaneView";
import {useSnack} from "../basic/global-snack";
import ArrowsButtons from "./ArrowsButtons";
import classNames from "classnames";
import StoryUnitPaneEdit from "./StoryUnitPaneEdit";
import UnitContextPaneEdit from "./UnitContextPaneEdit";

interface StoryContentProps {
    storyId: number
}

interface ElmIdentifier {
    type: 'ctx' | 'unit'
    index: number
}

const UNDO_REDO_LIMIT = 20

const StoryContent = (props: StoryContentProps) => {
    const {error, loading, content, setContent} =
        useRequestStateBase(
            () => sendSimple<UnitsResponse>(
                'GET',
                `/builder-server/app/story/${props.storyId}/units`
            ), {})

    const [editedContent, setEditedContent] = useState<UnitsResponse>({})
    const unitRefs = useRef<HTMLDivElement[]>([])
    const snack = useSnack()
    const ctxRefs = useRef<HTMLDivElement[]>([])
    const moveFieldCoordinates = useRef<Coordinate>()
    const contentRef = useRef<HTMLDivElement>(null)
    const contentWrapperRef = useRef<HTMLDivElement>(null)
    const [redoState, setRedoState] = useState<UnitsResponse[]>([])
    const [undoState, setUndoState] = useState<UnitsResponse[]>([])
    const [sizes, setSizes] = useState({
        minWidth: 4000,
        minHeight: 4000
    })
    const [contentEdit, setContentEdit] = useState(false)
    const [saving, setSaving] = useState(false)
    const [focus, setFocus] = useState<ElmIdentifier>()
    const [dragged, setDragged] = useState(false)
    const [edit, setEdit] = useState<ElmIdentifier>()
    const [add, setAdd] = useState<'ctx' | 'unit'>()
    const [arrowMode, setArrowMode] = useState(false)
    const timerId = useRef<NodeJS.Timeout>()
    const updateArrowsRef = useRef<(arrowProps: ArrowProps[]) => void>(() => {
    })
    const updateArrowsButtonsRef = useRef<(arrowProps: ArrowProps[]) => void>(() => {
    })
    const arrowsData = useRef<ArrowProps[]>([])

    const data = contentEdit ? editedContent : content
    const setData = contentEdit ? setEditedContent : setContent

    const copyStateToUndo = () => {
        if (!contentEdit) return
        const newUndo = [...undoState, editedContent]
        if (newUndo.length > UNDO_REDO_LIMIT) newUndo.shift()
        setUndoState(newUndo)
        setRedoState([])
    }

    const undo = () => {
        const newUndo = [...undoState]
        const prevState = newUndo.pop() || {}
        setRedoState([...redoState, editedContent])
        setUndoState(newUndo)
        setEditedContent(prevState)
    }

    const redo = () => {
        const newRedo = [...redoState]
        const nextState = newRedo.pop() || {}
        setRedoState(newRedo)
        setUndoState([...undoState, editedContent])
        setEditedContent(nextState)
    }

    const updateArrows = (data: UnitsResponse, exclude?: ElmIdentifier) => {
        arrowsData.current = createArrowProps(
            data.units || [],
            unitRefs.current,
            data.contexts || [],
            ctxRefs.current,
            exclude?.type,
            exclude?.index
        )
        updateArrowsRef.current(arrowsData.current)
    }

    const updateUnit = (unit: StoryUnit, index: number) => {
        const newData = {...data, units: arrayElmUpdate(unit, index, data.units || [])}
        copyStateToUndo()
        setData(newData)
    }

    const remove = () => {
        if (focus === undefined || !editedContent.units || !editedContent.contexts) return;
        const units = [...editedContent.units]
        const contexts = [...editedContent.contexts]
        if (focus.type === 'unit') {
            const unit = editedContent.units[focus.index]
            if (!unit) return;
            unitRefs.current.splice(focus.index, 1)
            units.splice(focus.index, 1)
            units.forEach((u, i) => {
                if (u.to === unit.id) units[i] = {...u, to: undefined}
            })
            contexts.forEach((c, i) => {
                if (c.unitId === unit.id) contexts[i] = {...c, unitId: undefined}
                if (c.to === unit.id) contexts[i] = {...contexts[i], to: undefined}
            })
        }
        if (focus.type === 'ctx') {
            ctxRefs.current.splice(focus.index, 1)
            contexts.splice(focus.index, 1)
        }

        const newData = {units, contexts}
        copyStateToUndo()
        setEditedContent(newData)
        editOff()
        setFocus(undefined)
    }

    const onArrowRemove = (arrow: ArrowProps) => {
        let units = [...editedContent.units || []]
        let contexts = [...editedContent.contexts || []]

        if (arrow.fromUnitId && arrow.toUnitId) {
            units.forEach((u, i) => {
                if (u.id === arrow.fromUnitId && u.to === arrow.toUnitId)
                    units[i] = {...u, to: undefined}
            })
        }


        if (arrow.fromCtxId && arrow.toUnitId || arrow.fromUnitId && arrow.toCtxId) {
            contexts.forEach((c, i) => {
                if (c.id === arrow.fromCtxId && c.to === arrow.toUnitId)
                    contexts[i] = {...c, to: undefined}
                if (c.unitId === arrow.fromUnitId && c.id === arrow.toCtxId)
                    contexts[i] = {...c, unitId: undefined}
            })
        }


        const newData = {units, contexts}
        copyStateToUndo()
        setData(newData)
    }

    const updateContext = (context: UnitContext, index: number) => {
        const newData = {...data, contexts: arrayElmUpdate(context, index, data.contexts || [])}
        copyStateToUndo()
        setData(newData)
        setData(newData)
    }

    const enableContentEdit = () => {
        setEditedContent({...content, units: content.units || [], contexts: content.contexts || []})
        setContentEdit(true)
    }

    const disableContentEdit = () => {
        setContentEdit(false)
    }

    const onElmClick = (identifier: ElmIdentifier) => {
        if (!arrowMode) {
            if (focus && focus.type === identifier.type && focus.index === identifier.index) return
            setFocus(identifier)
            editOff()
        } else
            createArrow(identifier)
    }

    const onElmDblClick = (identifier: ElmIdentifier) => {
        if (!contentEdit) onElmClick(identifier)
        else {
            setFocus(identifier)
            editOn(identifier)
        }
    }

    const onContentFieldClick = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        const contentField = contentRef.current
        if (!add || !contentField) {
            setFocus(undefined)
            editOff()
            return
        }
        const elements = (add === "ctx" ? data.contexts : data.units) || []
        const index = elements.length
        const newElm = {
            id: createId(elements),
            x: e.pageX - contentField.offsetLeft,
            y: e.pageY - contentField.offsetTop
        }

        if (add === "ctx") updateContext(newElm, index)
        if (add === "unit") updateUnit(newElm, index)

        setFocus({index, type: add})
        setEdit({index, type: add})
        setAdd(undefined)
    }

    const createArrow = (identifier: ElmIdentifier) => {
        if (!focus || !data.units || !data.contexts) return;
        if (focus.index === identifier.index && focus.type === identifier.type) return;
        if (focus.type === 'unit') {
            const unit = data.units[focus.index]
            if (identifier.type === 'unit') {
                const unitTo = data.units[identifier.index]


                updateUnit({...unit, to: unitTo.id}, focus.index)
            } else if (identifier.type === 'ctx') {
                const ctxTo = data.contexts[identifier.index]
                updateContext({...ctxTo, unitId: unit.id}, identifier.index)
            }
        } else if (focus.type === 'ctx') {
            const ctx = data.contexts[focus.index]

            if (identifier.type === 'unit') {
                const unitTo = data.units[identifier.index]
                updateContext({...ctx, to: unitTo.id}, focus.index)
            }
            if (identifier.type === 'ctx') return;
        }
        setArrowMode(false)
    }

    const onKeyPress = (e: React.KeyboardEvent<HTMLDivElement>) => {
        if (focus && e.keyCode === 46) remove()
        if (edit) return
        const key = (e.key || "").toLowerCase()
        if (redoState.length && e.ctrlKey && (key === 'y' || (e.shiftKey && key === 'z'))) redo()
        else if (undoState.length && e.ctrlKey && key === 'z') undo()
    }


    const draggedOn = () => setDragged(true)
    const draggedOff = () => setDragged(false)
    const editOn = (identifier: ElmIdentifier) => {
        setEdit(identifier)
        setArrowMode(false)
    }
    const changeArrowMode = () => {
        if (arrowMode)
            setArrowMode(false)
        else {
            setArrowMode(true)
            setEdit(undefined)
        }
    }

    const editOff = () => {
        setEdit(undefined)
        setArrowMode(false)
    }


    const onAdd = (addType: 'unit' | 'ctx') => {
        if (addType === add) {
            setAdd(undefined)
            return
        }
        editOff()
        setFocus(undefined)
        setAdd(addType)
    }

    const onScreenMove = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        if (!moveFieldCoordinates.current || !contentWrapperRef.current) return
        e.preventDefault()
        window.scrollBy(0, moveFieldCoordinates.current.y - e.pageY)
        contentWrapperRef.current.scrollBy(moveFieldCoordinates.current.x - e.pageX, 0)
        moveFieldCoordinates.current.x = e.pageX
        console.log(
            'clientX: ', e.clientX, 'clientY: ', e.clientY,
            'pageX: ', e.pageX, 'pageY: ', e.pageY,
            'delta: ', moveFieldCoordinates.current.x - e.pageX
        )
    }

    useEffect(() => {
        if (timerId.current) clearTimeout(timerId.current)
        timerId.current = setTimeout(() => {
            updateArrows(data)
            updateArrowsButtonsRef.current(arrowsData.current)
        }, 100)
        return () => timerId.current && clearTimeout(timerId.current)
    }, [data, contentEdit])

    const elements = () => {
        const units = data.units || []
        const contexts = data.contexts || []

        const unitFocus = focus && focus.type === 'unit' && focus.index
        const ctxFocus = focus && focus.type === 'ctx' && focus.index
        const unitEdit = edit && edit.type === 'unit' && edit.index
        const ctxEdit = edit && edit.type === 'ctx' && edit.index
        const unitAccident = arrowMode && (ctxEdit !== undefined || unitEdit !== undefined)
        const ctxAccident = arrowMode && unitEdit !== undefined

        const elms = units.map((unit, i) => (
            <DraggableContent
                key={i}
                edit={unitEdit === i}
                focused={unitFocus === i}
                flashed={unitFocus === i && arrowMode}
                accident={unitAccident && unitFocus !== i}
                onDragStart={draggedOn}
                onClick={() => onElmClick({type: 'unit', index: i})}
                onDoubleClick={() => onElmDblClick({type: 'unit', index: i})}
                position={{x: unit.x || 0, y: unit.y || 0}}
                setRef={(ref) => unitRefs.current[i] = ref}
                onDragEnd={pos => {
                    if (posChange(pos, {x: unit.x || 0, y: unit.y || 0}))
                        updateUnit({...unit, ...pos}, i)
                    draggedOff()
                }}
                onDrag={(pos) => updateArrows({
                    ...data,
                    units: arrayElmUpdate({...unit, ...pos}, i, data.units || [])
                })}
            >
                {unitEdit === i ?
                    <StoryUnitPaneEdit unit={unit} onUnitEdit={u => updateUnit({...u, x: unit.x, y: unit.y}, i)}/> :
                    <StoryUnitPaneView unit={unit}/>
                }
            </DraggableContent>
        ))
        contexts.forEach((ctx, i) => elms.push(
            <DraggableContent
                key={i + elms.length}
                edit={ctxEdit === i}
                focused={ctxFocus === i}
                flashed={ctxFocus === i && arrowMode}
                accident={ctxAccident && ctxFocus !== i}
                onDragStart={draggedOn}
                onClick={() => onElmClick({type: 'ctx', index: i})}
                onDoubleClick={() => onElmDblClick({type: 'ctx', index: i})}
                position={{x: ctx.x || 0, y: ctx.y || 0}}
                setRef={(ref) => ctxRefs.current[i] = ref}
                onDragEnd={pos => {
                    if (posChange(pos, {x: ctx.x || 0, y: ctx.y || 0}))
                        updateContext({...ctx, ...pos}, i)
                    draggedOff()
                }}
                onDrag={(pos) => updateArrows({
                    ...data,
                    contexts: arrayElmUpdate({...ctx, ...pos}, i, data.contexts || [])
                })}
            >
                {ctxEdit === i ? <UnitContextPaneEdit
                        edit={ctxEdit === i}
                        context={ctx}
                        onCtxEdit={c => updateContext({...c, x: ctx.x, y: ctx.y}, i)}/> :
                    <UnitContextPaneView context={ctx}/>}
            </DraggableContent>
        ))

        return elms
    }

    if (error) return <LocalError/>
    if (loading) return <FilledLoading/>

    const save = () => {
        setSaving(true)
        sendSimple<UnitsResponse>(
            'PUT',
            `/builder-server/app/story/${props.storyId}/units`,
            {data: editedContent}
        )
            .then(setContent)
            .then(() => setContentEdit(false))
            .then((e) => snack.show('Сохранено'))
            .catch((e) => snack.show("Error: " + String(e?.data?.message || e?.data), "error"))
            .then(() => setSaving(false))
    }

    return (
        <Layout>
            <Splitter mTop='x0'/>
            <Layout sticky className={styles.actionContainer}>
                <StoryContentAction
                    disabled={saving}
                    arrowMode={arrowMode}
                    onSave={save}
                    onArrowClick={focus ? () => changeArrowMode() : undefined}
                    onEditEnable={enableContentEdit}
                    onEditDisable={contentEdit ? disableContentEdit : undefined}
                    onUnitEditOn={!edit && focus ? () => editOn(focus) : undefined}
                    onUnitEditOff={edit ? editOff : undefined}
                    onDelete={!edit && focus ? remove : undefined}
                    onUndo={!edit && undoState.length ? undo : undefined}
                    onRedo={!edit && redoState.length ? redo : undefined}
                    onUnitAdd={() => onAdd('unit')}
                    onCtxAdd={() => onAdd('ctx')}
                    addType={add}
                />
            </Layout>
            <div className={styles.contentWrapper} ref={contentWrapperRef}>
                <div
                    className={classNames(
                        styles.storyUnitDragContainer,
                        styles.grid,
                        styles.cursorGrab,
                    )}
                    onKeyUp={onKeyPress}
                    tabIndex={0}
                    style={sizes}
                    onClick={onContentFieldClick}
                    ref={contentRef}
                    onMouseDown={e => {
                        if (e.button === 0)
                            moveFieldCoordinates.current = {x: e.pageX, y: e.pageY}
                    }}
                    onMouseUp={e => moveFieldCoordinates.current = undefined}
                    onMouseMove={onScreenMove}
                >
                    <ArrowsButtons
                        show={!dragged && contentEdit && !edit}
                        setReferenceUpdate={updateArrowsButtonsRef}
                        onRemove={onArrowRemove}
                    />
                    <ContentArrows
                        width={sizes.minWidth}
                        height={sizes.minHeight}
                        setReferenceUpdate={updateArrowsRef}
                        className={styles.arrows}
                    />
                    {elements()}
                </div>
            </div>
        </Layout>
    )
}

const ZERO_POS_PX = 2

const posChange = (a: Coordinate, b: Coordinate) =>
    Math.abs(a.x - b.x) > ZERO_POS_PX || Math.abs(a.y - b.y) > ZERO_POS_PX

export default StoryContent;