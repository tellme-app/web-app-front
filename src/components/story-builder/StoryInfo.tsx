import React, {useState} from "react";
import Story from "../../models/story/Story";
import Text from "../core/text";
import LocalError from "../core/states/LocalError";
import Layout from "../core/containers/layout";
import styles from "./storyBuilder.module.scss"
import useRequestState from "../../network/useRequestState"
import LocalLoading from "../core/states/LocalLoading";
import {send, sendSimple} from "../../network/send";
import StoryInfoView from "./StoryInfoView";
import StoryInfoEdit from "./StoryInfoEdit";
import {useSnack} from "../basic/global-snack";

interface StoryInfoProps {
    storyId: number
}

const saveStory = (story: Story) => send<Story>('PUT', `/builder-server/app/story`, {data: story})

const StoryInfo = (props: StoryInfoProps) => {
    const {error, loading, content, setContent} = useRequestState(() => sendSimple<Story>(
        'GET',
        `/builder-server/app/story/${props.storyId}`
    ))

    const snack = useSnack()

    const [edit, setEdit] = useState(false)
    const [saving, setSaving] = useState(false)

    if (error) return <LocalError/>
    if (loading) return <LocalLoading className={styles.storyInfoLoader}/>

    const save = (story: Story) => {
        setSaving(true)
        sendSimple<Story>('PUT', `/builder-server/app/story`, {data: story})
            .then(setContent)
            .then(() => snack.show('Сохранено'))
            .then(() => setEdit(false))
            .catch(e => snack.show('Ошибка сохранения', "error"))
            .then(() => setSaving(false))
    }


    return (
        <Layout p='x4' pLeft='x6'>
            <Text className={styles.storyName} elm='span' variant="h4">{content?.storyName}</Text>
            {edit ? (
                <StoryInfoEdit
                    story={content || {}}
                    onSave={save}
                    onClose={() => setEdit(false)}
                    disabled={saving}
                />
            ) : (
                <StoryInfoView
                    story={content || {}}
                    onEditClick={() => setEdit(true)}
                    disabled={saving}
                />
            )}
        </Layout>
    )
}


export default StoryInfo;