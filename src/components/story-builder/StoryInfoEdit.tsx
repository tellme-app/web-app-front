import React, {useState} from "react";
import Story from "../../models/story/Story";
import Text from "../core/text";
import styles from "./storyBuilder.module.scss"
import StoryTextInput from "./StoryTextInput";
import RoundButton from "../core/buttons/round-button";
import {MdClose, MdDone} from "react-icons/all";

interface StoryInfoEditProps {
    story: Story
    onSave: (story: Story) => void
    onClose: () => void
    disabled: boolean
}

const StoryInfoEdit = ({story, ...props}: StoryInfoEditProps) => {
    const [editedStory, setEditedStory] = useState(story)
    return (
        <>
            <Text elm='div' variant='subTip'>Название</Text>
            <StoryTextInput
                className={styles.storyTextFields}
                disabled={props.disabled}
                value={editedStory.storyName}
                onChange={e => setEditedStory({...editedStory, storyName: e.target.value})}
            />
            <Text elm='div' variant='subTip'>Описание</Text>
            <StoryTextInput
                className={styles.storyTextFields}
                disabled={props.disabled}
                value={editedStory.description}
                onChange={e => setEditedStory({...editedStory, description: e.target.value})}
            />
            <div>
                <RoundButton
                    m='x1'
                    mTop='x3'
                    flat
                    icon={MdClose}
                    preset="error"
                    onClick={props.onClose}
                    disabled={props.disabled}
                    size='nano'
                />
                <RoundButton
                    m='x1'
                    mTop='x3'
                    flat
                    icon={MdDone}
                    preset="primary"
                    onClick={() => props.onSave(editedStory)}
                    disabled={props.disabled}
                    size='nano'
                />
            </div>
        </>
    )
}


export default StoryInfoEdit;