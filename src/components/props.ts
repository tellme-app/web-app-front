import {StyleSizes} from "../style/StyleSizes";
import StyleColors from "../style/StyleColors";
import StyleSpaces from "../style/StyleSpaces";
import React, {ElementType} from "react";
import StyleMargins from "../style/StyleMargins";

export interface StyleProps {
    size?: keyof StyleSizes
    color?: keyof StyleColors
    colorHover?: keyof StyleColors
    colorActive?: keyof StyleColors
    colorDisabled?: keyof StyleColors
    bkg?: keyof StyleColors
    bkgHover?: keyof StyleColors
    bkgActive?: keyof StyleColors
    bkgDisabled?: keyof StyleColors
    p?: keyof StyleSpaces,
    pTop?: keyof StyleSpaces,
    pBottom?: keyof StyleSpaces,
    pLeft?: keyof StyleSpaces,
    pRight?: keyof StyleSpaces,
    m?: keyof StyleMargins,
    mTop?: keyof StyleMargins,
    mBottom?: keyof StyleMargins,
    mLeft?: keyof StyleMargins,
    mRight?: keyof StyleMargins,
}

export type HTMLAttributesProps<E extends ElementType> = React.DetailedHTMLProps<React.HTMLAttributes<E>, E>
export type ButtonHTMLAttributesProps<E extends ElementType> = React.DetailedHTMLProps<React.ButtonHTMLAttributes<E>, E>

export type HTMLProps<E extends ElementType> = HTMLAttributesProps<E> | ButtonHTMLAttributesProps<E>;

export type DivProps = HTMLAttributesProps<"div">
export type SpanProps = HTMLAttributesProps<"span">