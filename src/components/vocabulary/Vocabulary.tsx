import {RouteComponentProps} from "react-router";
import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";

const Vocabulary = (props: RouteComponentProps) => {
    const pfx = props.match.path;
    return (
        <Switch>
          <Route path={pfx + "/words"} component={()=> <>words</>}/>
          <Route path={pfx + "/collections"} component={()=> <>collections</>}/>
            <Redirect to={pfx + '/words'}/>
        </Switch>
    )
}

export default Vocabulary