import {RouteComponentProps} from "react-router";
import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";
import Vocabulary from "../vocabulary";
import Header from "../header";
import Logo from "../svg/Logo";
import Layout from "../core/containers/layout";
import Text from "../core/text";

const MainRoute = (props: RouteComponentProps) => {
    const pfx = props.match.path;
    return (
        <>
            <Header/>
            <Layout flex justifyContentCenter mTop='x11'>
                <div>
                    <Layout flex justifyContentCenter>
                        <Logo size={80}/>
                    </Layout>
                    <Text variant='h5' disableTopIdent elm='div' mTop='x8'>to be continued...</Text>
                </div>
            </Layout>
        </>
    )
    /* return (
         <Switch>
             <Route path={pfx + '/vocabulary'} component={Vocabulary}/>
             <Redirect to={pfx + '/vocabulary'}/>
         </Switch>
     )*/
}

export default MainRoute