import React, {ChangeEvent, FormEvent, FunctionComponent, useState} from 'react'
import s from './ModalWindow.module.scss'
import TitleText from "./TitleText";
import {Link} from "react-router-dom";

interface HubspotError {
    errorType: string
}

interface HubspotErrorArray {
    errors: HubspotError[];
}

const ModalWindow: FunctionComponent = () => {

    const [nameValue, setNameValue] = useState<string>("")
    const [emailValue, setEmailValue] = useState<string>("")
    const [emailError, setEmailError] = useState<boolean>(false);
    const [nameError, setNameError] = useState<boolean>(false);
    const [agreeValue, setAgreeValue] = useState<boolean>(false);
    const [agreeError, setAgreeError] = useState<boolean>(false);
    const googleFormUrl: string = process.env.REACT_APP_GOOGLE_FORM_URL ? process.env.REACT_APP_GOOGLE_FORM_URL : "https://docs.google.com/forms/d/e/1FAIpQLSfovjXMqd93xorWMlJx-LT1aJSyhH3FBke1d2lwZtLFlObe3A/viewform?usp=sf_link";

    const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let error = false;
        setNameError(false);
        setEmailError(false);
        setAgreeError(false);
        if (!emailValue.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
            error = true;
            setEmailError(true);
        }
        if (!nameValue) {
            error = true;
            setNameError(true);
        }
        if (!agreeValue) {
            setAgreeError(true);
            error = true;
        }

        if (error)
            return;

        const reqBody = JSON.stringify({
            name: nameValue,
            email: emailValue
        })

        fetch(`/hubspot/contact`, {
            method: 'POST',
            body: reqBody,
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {
            if (response.status === 400) {
                if (response.body)
                    response.json()
                        .then((data: HubspotErrorArray) => {
                            if (data.errors)
                                // eslint-disable-next-line array-callback-return
                                data.errors.map((error: HubspotError) => {
                                    if (error.errorType.includes("EMAIL"))
                                        setEmailError(true);
                                })
                        })
            } else if (!response.ok) {
                console.log("Error! " + response.statusText)
            } else {
                window.location.href = googleFormUrl;
            }
        })
    }

    const onEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
        setEmailValue(e.target.value);
    }

    const onNameChange = (e: ChangeEvent<HTMLInputElement>) => {
        setNameValue(e.target.value);
    }

    const onAgreeChange = (e: ChangeEvent<HTMLInputElement>) => {
        setAgreeValue(e.target.checked);
    }

    const emailClasses = emailError ? [s.email, s.error].join(' ') : s.email;
    const nameClasses = nameError ? [s.name, s.error].join(' ') : s.name;
    const agreeClasses = agreeError ? [s.agree, s.error].join(' ') : s.agree;

    return (
        <div className={s.wrapper}>
            <div className={s.window}>
                <div className={s.leftPart}>
                    <TitleText className={s.title}>
                        Начни говорить на английском сейчас!
                    </TitleText>
                </div>
                <div className={s.rightPart}>
                    <form onSubmit={onFormSubmit}>
                        <div className={s.formTopContainer}>
                            <div className={s.inputContainer}>
                                <div className={nameClasses}>
                                    <input type="text" onChange={onNameChange} placeholder="Name"
                                           value={nameValue}/>
                                </div>
                                {nameError ?
                                    <span style={{top: '-30px'}} className={s.errorMessage}>Введите имя</span> : ''}
                            </div>
                            <div className={s.inputContainer}>
                                <div className={emailClasses}>
                                    <input type="email" onChange={onEmailChange} placeholder="Email"
                                           value={emailValue}/>
                                </div>
                                {emailError ? <span className={s.errorMessage}>Введите корректный E-mail</span> : ''}
                            </div>
                        </div>
                        <input type="submit" className={s.doneButton} value="Готово"/>
                        <label className={agreeClasses}>
                            <input type="checkbox" checked={agreeValue} onChange={onAgreeChange}/>
                            <Link to="/privacy" target="_blank">Даю согласие на обработку</Link><br/>моих персональных
                            данных
                        </label>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ModalWindow;
