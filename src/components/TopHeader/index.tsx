import React from "react";
import s from "./TopHeader.module.scss"
import logo from './static/text_logo.png'
import {Link} from "react-router-dom";
import classNames from "classnames";


const TopHeader = ({sticky = true}: { sticky?: boolean }) => {
    const loginAddress = "/login";
    return (
        <div className={classNames(s.headerRow, sticky && s.sticky)}>
            <div className={[s.leftBlock, s.block].join(' ')}>
                <a href="/">
                    <img src={logo} alt="SpeakaTalka" className={s.logo}/>
                </a>
                <p className={s.sloganText}>твоя личная языковая среда</p>
            </div>
            <div className={[s.rightBlock, s.block].join(' ')}>
                <Link
                    className={[s.button, s.login].join(' ')}
                    type="button"
                    to={loginAddress}
                >
                    Войти
                </Link>
                <Link
                    className={[s.button, s.register].join(' ')}
                    type="button"
                    to={loginAddress}
                >
                    Регистрация
                </Link>
            </div>
        </div>
    )
}

export default TopHeader;
