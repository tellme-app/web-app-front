import ILocalName from "./LocalName";

const defaultLang: ILocalName = {

    Tagline: "your own personal language environment",
    SignIn: "Log in",
    SignUp: "Sign up",
    ModalDone: "Done",
    ModalTagline: "Start speaking English now!",
    ModalAgreement: "my personal data",
    ModalAgreementLink: "I agree to the processing",
    WelcomeScreenTitle: "Start to speak English with Artificial Intelligence",
    WelcomeScreenFirstText: "SpeakaTalka makes you talk in English.",
    WelcomeScreenSecondText: "With lots of practice",
    SecondScreenFirstTitle: "SpeakaTalka is an intellectual assistant which will teach you how to speak English.",
    SecondScreenText: "There are dozens of typical situations and scenarios. They are surrounded by basic dialogues: how to talk to a neighbor on the plane, how to ask for directions to the hotel or attending an interview. You will master them, improve your English, speak fluent and without embarrassment.",
    SecondScreenSecondTitle: "Improve your spoken English with SpeakaTalka on the topics you are interested in.",
    ContentPaneTravel: "travelling",
    ContentPaneInterview: "interviews",
    ContentPaneChatting: "communication",
    StartCommunicateButton: "Start talking",
    ThirdScreenTitle: "How does SpeakaTalka work?",
    ContentPaneEnterTitle: "Entry",
    ContentPaneEnterText: "SpeakaTalka recognizes your speech, transforms it into text and starts to process what you said.",
    ContentPaneProcessingTitle: "Processing",
    ContentPaneProcessingText: "Using our unique Artificial Intelligence technology, SpeakaTalka processes information, analyzes pronunciation, sentence construction and gives an answer based on the selected topic. In doing so, you will see an information on how well your sentence was built and the quality of your pronunciation.",
    ContentPaneExitTitle: "Output",
    ContentPaneExitText: "Depending on the selected topic and dialog, SpeakaTalka will answer you in English. There are hundreds of ways to go through the dialogue",
    FourthScreenFirstTitle: "AI technologies helps you.",
    FourthScreenSecondTitle: "It's easy to learn with them",
    FourthScreenFirstText: "Learn English, words and idioms, practice pronunciation with AI in one application. No needs to switch or get distracted by anything else. The repetition of vocabulary and grammar will be adapted to your level of knowledge.",
    FourthScreenThirdTitle: "Talk to SpeakaTalka 24/7",
    FourthScreenSecondText: "SpeakaTalka listens to you very carefully and during the whole lesson will tell you about grammatical mistakes, correct your pronunciation and teach you how to speak as a native speaker.",
    FifthScreenFirstTitle: "Conversation practice without limits ",
    StartLearnFreeButton: "Start learning for free",
    FinalScreenFirstTitle: "Full analysis in your pocket",
    FinalScreenSecondTitleFirstPlain: "Recommendations ",
    FinalScreenSecondTitleFirstAccident: "from an assistant ",
    FinalScreenSecondTitleSecondPlain: "after each ",
    FinalScreenSecondTitleSecondAccident: "Lesson",
    FinalScreenText: "Handling the language barrier has never been so easy",
    TestLessonButton: "Try the Test Lesson",
    LearnToSpeakEnglishButton: "Learn to speak English",
    FifthScreenSecondTitle: "from the first lessons",
    PrivacyLink: "Privacy Policy",
    ModalEmailError: "Enter correct E-mail",
    ModalNameError: "Enter name",

}

export default defaultLang;
