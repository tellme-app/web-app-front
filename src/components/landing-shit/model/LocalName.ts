interface ILocalName {
    Tagline: string
    SignIn: string
    SignUp: string
    ModalDone: string
    ModalTagline: string
    ModalAgreement: string
    ModalAgreementLink: string
    WelcomeScreenTitle: string
    WelcomeScreenFirstText: string
    WelcomeScreenSecondText: string
    SecondScreenFirstTitle: string
    SecondScreenText: string
    SecondScreenSecondTitle: string
    ContentPaneTravel: string
    ContentPaneInterview: string
    ContentPaneChatting: string
    StartCommunicateButton: string
    ThirdScreenTitle: string
    ContentPaneEnterTitle: string
    ContentPaneEnterText: string
    ContentPaneProcessingTitle: string
    ContentPaneProcessingText: string
    ContentPaneExitTitle: string
    ContentPaneExitText: string
    FourthScreenFirstTitle: string
    FourthScreenSecondTitle: string
    FourthScreenFirstText: string
    FourthScreenThirdTitle: string
    FourthScreenSecondText: string
    FifthScreenFirstTitle: string
    StartLearnFreeButton: string
    FinalScreenFirstTitle: string
    FinalScreenSecondTitleFirstPlain: string
    FinalScreenSecondTitleFirstAccident: string
    FinalScreenSecondTitleSecondPlain: string
    FinalScreenSecondTitleSecondAccident: string
    FinalScreenText: string
    TestLessonButton: string
    LearnToSpeakEnglishButton: string
    FifthScreenSecondTitle: string
    PrivacyLink: string
    ModalNameError: string
    ModalEmailError: string
}

export default ILocalName;
