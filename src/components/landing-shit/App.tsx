import React, {useEffect, useState} from 'react';
import './App.css'
import MainComponent from "./components/MainComponent/MainComponent";
import TopHeader from "./components/TopHeader";
import Footer from "./components/Footer";
import ILocalName from "./model/LocalName";
import defaultLang from "./model";

export interface LocalNameProps {
    data: ILocalName;
}

function App() {

    const [data, setData] = useState<ILocalName>(defaultLang);
    const [userLang, setUserLang] = useState<string>("en")
    const [loading, setLoading] = useState<boolean>(false);
    const [loaded, setLoaded] = useState<boolean>(false);

    const lang = window.navigator.language.slice(0, 2)

    const getData = async () => {
        const currentLang = localStorage.getItem("lang") || lang;
        setLoading(true);
        fetch(`/localization/?localName=${currentLang}&dest=landing`)
            .then(response => {
                if (response.status === 200) {
                    response.json().then((data: ILocalName) => setData(data));
                    localStorage.setItem("lang", currentLang);
                    setUserLang(currentLang);
                } else
                    localStorage.setItem("lang", userLang);
            })
            .finally(() => {
                setLoading(false);
                setLoaded(true);
            });
    }

    const setUserLangDecorator = (lang: string) => {
        localStorage.setItem("lang", lang);
        setLoaded(false);
    }

    useEffect(() => {
        const getDataDecorator = async () => await getData();
        if (!loading && !loaded)
            getDataDecorator()

    })

    return (
        <>
            <TopHeader data={data} userLang={userLang} setUserLang={setUserLangDecorator}/>
            <MainComponent
                data={data}
            />
            <Footer data={data}/>
        </>
    );
}

export default App;
