import React, {FunctionComponent} from 'react'
import s from './ContentPane.module.scss'
import ParagraphText from "../ParagraphText";
import TitleText from "../TitleText";

interface ContentPaneProps {
    imagePath: string,
    text: string,
    bigImage?: boolean,
    subTitle?: string
}

const ContentPane: FunctionComponent<ContentPaneProps> = ({imagePath, text, bigImage, subTitle}) => {
    const imgClasses = [s.cover];

    if (bigImage){
        imgClasses.push(s.bigImage);
    }

    return(
        <div className={s.container}>
            <img src={imagePath} alt="" className={imgClasses.join(' ')}/>
            {subTitle ? <TitleText className={s.subTitle}>{subTitle}</TitleText> : ''}
            <ParagraphText>{text}</ParagraphText>
        </div>
    )
}

export default ContentPane;
