import React, {FunctionComponent} from 'react'
import s from './FifthScreen.module.scss'
import TitleText from "../../SimpleComponents/TitleText";
import {Link} from 'react-router-dom';
import {LocalNameProps} from "../../../App";
import CustomLink from "../../../../core/custom-link";

const FifthScreen: FunctionComponent<LocalNameProps> = ({data}) => {
    return (
        <div className={s.container}>
            <TitleText className={s.text}>
                {data.FifthScreenFirstTitle}
            </TitleText>
            <TitleText className={[s.text, s.secondText].join(' ')}>
                {data.FifthScreenSecondTitle}
            </TitleText>
            <CustomLink
                className={s.button}
                type="button"
                to='/signup'
            >
                {data.StartLearnFreeButton}
            </CustomLink>
        </div>
    );
}

export default FifthScreen;
