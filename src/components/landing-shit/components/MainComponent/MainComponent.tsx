import React, {FunctionComponent} from "react";
import WelcomeHeader from "./WelcomeScreen";
import SecondScreen from "./SecondScreen";
import ThirdScreen from "./ThirdScreen";
import FourthScreen from "./FourthScreen";
import FifthScreen from "./FifthScreen";
import FinalScreen from "./FinalScreen";
import {LocalNameProps} from "../../App";

const MainComponent: FunctionComponent<LocalNameProps> = ({ data}) => {


    return (
        <>
            <WelcomeHeader data={data}/>
            <SecondScreen data={data}/>
            <ThirdScreen data={data}/>
            <FourthScreen data={data}/>
            <FifthScreen data={data}/>
            <FinalScreen data={data}/>
        </>
    )
}

export default MainComponent;
