import React, {FunctionComponent} from 'react';
import s from './FinalScreen.module.scss'
import TitleText from "../../SimpleComponents/TitleText";
import ParagraphText from "../../SimpleComponents/ParagraphText";
import AnalyticImg from './static/analytic-min.png'
import DoneImg from './static/done-min.png'
import {Link} from "react-router-dom";
import {LocalNameProps} from "../../../App";
import CustomLink from "../../../../core/custom-link";

const FinalScreen: FunctionComponent<LocalNameProps> = ({data}) => {
    return (
        <div className={s.container}>
            <TitleText className={s.fullAnalyticsText}>
                {data.FinalScreenFirstTitle}
            </TitleText>
            <div className={s.analyticContainer}>
                <div className={s.textContainer}>
                    <TitleText className={s.title}>
                        {data.FinalScreenSecondTitleFirstPlain + " "}<span
                        className={s.whiteText}>{data.FinalScreenSecondTitleFirstAccident + " "}</span>{data.FinalScreenSecondTitleSecondPlain + " "}<span
                        className={s.whiteText}>{data.FinalScreenSecondTitleSecondAccident}</span>
                    </TitleText>
                    <ParagraphText className={s.text}>
                        {data.FinalScreenText}
                    </ParagraphText>
                    <CustomLink
                        className={s.testLessonButton}
                        type="button"
                        to='/signup'
                    >
                        {data.TestLessonButton}
                    </CustomLink>
                </div>
            </div>
            <img className={s.analyticImg} src={AnalyticImg} alt="Аналитика"/>
            <CustomLink to='/signup' className={s.startLearnButton}>
                <p>{data.LearnToSpeakEnglishButton}</p>
            </CustomLink>
            <CustomLink to='/signup'>
                <img className={s.doneImg} src={DoneImg} alt="Выполнено"/>
            </CustomLink>
        </div>
    )
}

export default FinalScreen;
