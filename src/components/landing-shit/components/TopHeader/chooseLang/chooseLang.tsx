import React, {FC, useEffect, useState} from 'react'
import s from './chooseLang.module.scss'

interface LocalLang {
    localName: string,
    realName: string
}

interface ChooseLangProps {
    classname: string
}

export interface ChangeLangProps {
    userLang: string,
    setUserLang: (lang: string) => void
}

const ChooseLang: FC<ChangeLangProps & ChooseLangProps> = ({userLang, setUserLang, classname}) => {

    const [loading, setLoading] = useState<boolean>(false);
    const [data, setData] = useState<LocalLang[]>()

    const getData = async () => {
        setLoading(true);
        fetch('/localization/available')
            .then(response => {
                response.json().then((data: LocalLang[]) => setData(data))
                setLoading(false);
            })
    }

    useEffect(() => {
        const getDataDecorator = async () => await getData();
        if (!loading && !data)
            getDataDecorator();

    })

    let content;

    if (data) {
        content = <select className={s.select + ' ' + classname} onChange={ e => setUserLang(e.currentTarget.value)}>
            {data.map(t => <option value={t.localName} selected={userLang === t.localName}>{t.realName}</option>)}
        </select>
    }
    return (
        <>
            {content}
        </>
    )
}

export default ChooseLang;
