import React, {FunctionComponent} from "react";
import s from "./TopHeader.module.scss"
import logo from './static/text_logo.png'
import {Link} from "react-router-dom";
import {LocalNameProps} from "../../App";
import ChooseLang from "./chooseLang";
import {ChangeLangProps} from "./chooseLang/chooseLang";
import useStyle from "../../../../style/useStyle";
import CustomLink from "../../../core/custom-link";

const TopHeader: FunctionComponent<LocalNameProps & ChangeLangProps> = ({data, userLang, setUserLang}) => {
    const sT = useStyle()
    return (
        <div className={s.headerRow}>
            <div className={[s.leftBlock, s.block].join(' ')}>
                <Link to="/">
                    <img src={logo} alt="SpeakaTalka" className={s.logo}/>
                </Link>
                <p className={s.sloganText}>{data.Tagline}</p>
            </div>
            <div className={[s.rightBlock, s.block].join(' ')}>
                <CustomLink
                    className={[s.button, s.username, sT.mRight.x2].join(' ')}
                    type="button"
                    to='/login'
                >
                    {data.SignIn}
                </CustomLink>
                <CustomLink
                    className={[s.button, s.register].join(' ')}
                    type="button"
                    to='/signup'
                >
                    {data.SignUp}
                </CustomLink>
                <ChooseLang classname={s.chooseLanguage} userLang={userLang} setUserLang={setUserLang}/>

            </div>
        </div>
    )
}

export default TopHeader;
