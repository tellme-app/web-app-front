export default interface Pageable<T> {
    content: T[]
    totalSize: number,
    totalPages: number,
    empty: boolean,
    size: number,
    offset: number,
    pageNumber: number,
    numberOfElements: number
}