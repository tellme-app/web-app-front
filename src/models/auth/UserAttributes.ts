export default interface UserAttributes {
    roles: string[]
    userEmail: string
    userFio: string
}