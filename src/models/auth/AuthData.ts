import UserAttributes from "./UserAttributes";

export default interface AuthData {
  name: string
  attributes: UserAttributes
}