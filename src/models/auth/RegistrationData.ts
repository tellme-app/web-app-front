export default interface RegistrationData {
    name: string,
    fio: string,
    email: string,
    password: string,
    confirmPassword: string
}