export default interface Sentence {
    text: string
    style: string
}