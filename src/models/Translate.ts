import Sentence from "./Sentence";

export default interface Translate {
    name: string
    contexts: Sentence[][][]
}