import Sentence from "./Sentence";

export default interface SpeechResponse {
    clear: number
    precisely: number
    textResponse: Sentence[]
    transcriptResponse: Sentence[]
    id_enhanced: string
}