export default interface Story {
    storyId?: number
    storyName?: string
    description?: string

}