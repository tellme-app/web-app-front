export default interface ContextValue {
    id?: number
    value?: string
}