import UnitContext from "./UnitContext";
import StoryUnit from "./StoryUnit";

export default interface UnitsResponse {
    units?: StoryUnit[]
    contexts?: UnitContext[]
}