export default interface StoryUnit {
    id?: number
    text?: string
    x?: number
    y?: number
    to?: number
}