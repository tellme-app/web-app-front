export default interface ContextExample {
    id?: number
    text?: string
    isConcrete?: boolean
}