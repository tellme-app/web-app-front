import ContextExample from "./ContextExample";
import ContextValue from "./ContextValue";

export default interface UnitContext {
    id?: number
    unitId?: number
    to?: number
    examples?: ContextExample[]
    values?: ContextValue[]
    x?: number
    y?: number
}