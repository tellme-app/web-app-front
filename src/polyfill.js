import 'webrtc-adapter';
import AudioRecorder from 'audio-recorder-polyfill'

//MediaRecorder polyfill
window.MediaRecorder = AudioRecorder

//Navigator polyfill
if (!navigator.mediaDevices)
    navigator.mediaDevices = {}

if (!navigator.mediaDevices.getUserMedia)
    navigator.mediaDevices.getUserMedia = (constraints) => {
        const getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.getUserMedia;

        if (!getUserMedia)
            return Promise.reject(new Error('getUserMedia is not implemented in this browser'));

        return new Promise((resolve, reject) => getUserMedia.call(navigator, constraints, resolve, reject));
    }