import React, {useContext, useEffect, useState} from 'react';
import './App.css'
import {Redirect, Route, Switch} from 'react-router-dom';
import Privacy from "./components/Privacy";
import NotFound from "./components/NotFound";
import Footer from "./components/Footer";
import Promo from "./components/promo";
import TopHeader from "./components/TopHeader";
import StoryBuilder from "./components/story-builder";
import Stories from "./components/stories";
import MainRoute from "./components/main-route";
import SignIn from "./components/signin";
import Demo from "./components/demo"
import Landing from "./components/landing-shit"
import {AuthContext} from "./auth";
import SignUp from "./components/signin/SignUp";
import FullScreenLoading from "./components/core/states/FullscreenLoading";
import {sendSimple} from "./network/send";
import AuthData from "./models/auth/AuthData";
import {useSnack} from "./components/basic/global-snack";
import AuthContextData from "./auth/AuthContextData";
import ProtectedRoute from "./components/basic/protected-route";

function App() {
    const [authCtx, setAuthCtx] = useContext(AuthContext);
    const snack = useSnack();

    const [loading, setLoading] = useState(true)

    useEffect(() => {
        sendSimple<AuthData>('GET', '/auth')
            .then(auth => setAuthCtx(new AuthContextData(true, auth)))
            .catch(e => {
                const status = e?.response?.status;
                if (status && status >= 500) snack.show('Error', 'error')
            })
            .then(() => setLoading(false))
    }, [])

    if (loading) return <FullScreenLoading/>
    return (
        <Switch>
            <Route path="/promo/:phrase" component={Promo}/>
            <ProtectedRoute path="/story/builder/:storyId([0-9]+)" component={StoryBuilder}/>
            <ProtectedRoute path="/stories" component={Stories}/>
            <ProtectedRoute path="/demo" component={Demo}/>
            <Route path="/privacy">
                <TopHeader/>
                <Privacy/>
                <Footer/>
            </Route>
            <ProtectedRoute path={"/main"} component={MainRoute}/>
            <Route path={"/login"} component={SignIn}/>
            <Route path={"/signup"} component={SignUp}/>
            <Route path="/landing" component={Landing}/>
            <Redirect from='/' to={authCtx.loggedIn ? '/main' : '/landing'}/>
            <Route path="*" component={NotFound}/>
        </Switch>
    );
}

export default App;
