import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './polyfill';
import App from './App';
import {StyleProvider} from "./style/styleContext";
import {GlobalSnackProvider} from "./components/basic/global-snack";
import {BrowserRouter} from "react-router-dom";
import PortalRoot from "./components/core/portal";
import {AuthProvider} from "./auth";
import ErrorBoundary from './components/error-boundary';

ReactDOM.render((
        <React.StrictMode>
            <ErrorBoundary>
                <BrowserRouter>
                    <StyleProvider>
                        <GlobalSnackProvider>
                            <AuthProvider>
                                <PortalRoot/>
                                <App/>
                            </AuthProvider>
                        </GlobalSnackProvider>
                    </StyleProvider>
                </BrowserRouter>
            </ErrorBoundary>
        </React.StrictMode>
    ), document.getElementById('root')
);
