import StyleSpaces from "./StyleSpaces";

export default interface StyleMargins extends StyleSpaces {
    auto: string
}
