import StyleSpaces from "./StyleSpaces";

export default (spaces: { [key: string]: string }, propName: string): StyleSpaces => ({
    x0: spaces[propName + "X0"],
    x1: spaces[propName + "X1"],
    x2: spaces[propName + "X2"],
    x3: spaces[propName + "X3"],
    x4: spaces[propName + "X4"],
    x5: spaces[propName + "X5"],
    x6: spaces[propName + "X6"],
    x7: spaces[propName + "X7"],
    x8: spaces[propName + "X8"],
    x9: spaces[propName + "X9"],
    x10: spaces[propName + "X10"],
    x11: spaces[propName + "X11"]
})
