import StyleColors from "./StyleColors";

export default (colorStyles: { [key: string]: string }, classPostfix: string): StyleColors => {
    const getClass = (className: string) => colorStyles[className + classPostfix]
    return {
        none: "",
        inherit: getClass("inherit"),
        main: getClass("main"),
        light: getClass("light"),
        dark: getClass("dark"),
        inverse: getClass("inverse"),
        accident: getClass("accident"),
        error: getClass("error"),
        success: getClass("success"),
        primary: getClass("primary"),
        primaryLight: getClass("primaryLight"),
        primaryDark: getClass("primaryDark"),
        secondary: getClass("secondary"),
        secondaryLight: getClass("secondaryLight"),
        secondaryDark: getClass("secondaryDark"),
    }
}
