export interface StyleSizes {
    nano: string
    micro: string
    mini: string
    'x-small': string
    small: string
    medium: string
    large: string
    'x-large': string
    giant: string
    plusSize: string
}
