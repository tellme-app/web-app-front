import {StyleSizes} from "./StyleSizes";

export default (text: { [key: string]: string }): StyleSizes => ({
    nano: text.nano,
    micro: text.micro,
    mini: text.mini,
    'x-small': text['x-small'],
    small: text.small,
    medium: text.medium,
    large: text.large,
    'x-large': text['x-large'],
    giant: text.giant,
    plusSize: text.plusSize
})
