import StyleMargins from "./StyleMargins";
import spacesMapping from "./spacesMapping";

export default (spaces: { [key: string]: string }, propName: string): StyleMargins => ({
    ...spacesMapping(spaces, propName),
    auto: spaces[propName + 'Auto'],

})
