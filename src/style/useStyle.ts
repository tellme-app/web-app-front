import {useContext} from "react";
import {StyleContext, StyleContextState} from "./styleContext";

const useStyle = (): StyleContextState => useContext(StyleContext)

export default useStyle
