export default interface StyleColors {
    primary: string
    primaryLight: string
    primaryDark: string
    secondary: string
    secondaryLight: string
    secondaryDark: string
    accident: string
    success: string
    error: string
    main: string
    light: string
    dark: string
    inverse: string
    inherit: string
    none: string
}
