import React, {ReactNode} from 'react'
import colorMapping from "./colorMapping";
import spacesMapping from "./spacesMapping";
import colors from "./colors/light.module.scss"
import spaces from "./spaces.module.scss"
import textSizes from "./text.module.scss"
import textSizesMapping from "./textSizesMapping";
import {StyleSizes} from "./StyleSizes";
import StyleColors from "./StyleColors";
import StyleSpaces from "./StyleSpaces";
import StyleMargins from "./StyleMargins";
import marginsMapping from "./marginsMapping";

interface StyleContextProps {
    colors?: { [key: string]: string }
    spaces?: { [key: string]: string }
    textSizes?: { [key: string]: string }
    children: ReactNode
}

export interface StyleContextState {
    size: StyleSizes,
    color: StyleColors,
    colorHover: StyleColors,
    colorActive: StyleColors,
    colorDisabled: StyleColors,
    bkg: StyleColors,
    bkgHover: StyleColors,
    bkgActive: StyleColors,
    bkgDisabled: StyleColors,
    m: StyleMargins,
    mTop: StyleMargins,
    mBottom: StyleMargins,
    mLeft: StyleMargins,
    mRight: StyleMargins,
    p: StyleSpaces,
    pTop: StyleSpaces,
    pBottom: StyleSpaces,
    pLeft: StyleSpaces,
    pRight: StyleSpaces,
}

const defaultContextState: StyleContextState = {
    color: colorMapping(colors, "Color"),
    colorHover: colorMapping(colors, "ColorHover"),
    colorActive: colorMapping(colors, "ColorActive"),
    colorDisabled: colorMapping(colors, "ColorDisabled"),
    bkg: colorMapping(colors, "Background"),
    bkgHover: colorMapping(colors, "BackgroundHover"),
    bkgActive: colorMapping(colors, "BackgroundActive"),
    bkgDisabled: colorMapping(colors, "BackgroundDisabled"),
    m: marginsMapping(spaces, "margin"),
    mTop: marginsMapping(spaces, "marginTop"),
    mBottom: marginsMapping(spaces, "marginBottom"),
    mLeft: marginsMapping(spaces, "marginLeft"),
    mRight: marginsMapping(spaces, "marginRight"),
    p: spacesMapping(spaces, "padding"),
    pTop: spacesMapping(spaces, "paddingTop"),
    pBottom: spacesMapping(spaces, "paddingBottom"),
    pLeft: spacesMapping(spaces, "paddingLeft"),
    pRight: spacesMapping(spaces, "paddingRight"),
    size: textSizesMapping(textSizes)
}

export const StyleContext = React.createContext<StyleContextState>(
    defaultContextState
)

export const StyleProvider = (props: StyleContextProps) => {
    const colorSet = props.colors || colors;
    const spacesSet = props.spaces || spaces;
    return (
        <StyleContext.Provider {...props} value={{
            color: colorMapping(colorSet, "Color"),
            colorHover: colorMapping(colorSet, "ColorHover"),
            colorActive: colorMapping(colorSet, "ColorActive"),
            colorDisabled: colorMapping(colorSet, "ColorDisabled"),
            bkg: colorMapping(colorSet, "Background"),
            bkgHover: colorMapping(colorSet, "BackgroundHover"),
            bkgActive: colorMapping(colorSet, "BackgroundActive"),
            bkgDisabled: colorMapping(colorSet, "BackgroundDisabled"),
            m: marginsMapping(spacesSet, "margin"),
            mTop: marginsMapping(spacesSet, "marginTop"),
            mBottom: marginsMapping(spacesSet, "marginBottom"),
            mLeft: marginsMapping(spacesSet, "marginLeft"),
            mRight: marginsMapping(spacesSet, "marginRight"),
            p: spacesMapping(spacesSet, "padding"),
            pTop: spacesMapping(spacesSet, "paddingTop"),
            pBottom: spacesMapping(spacesSet, "paddingBottom"),
            pLeft: spacesMapping(spacesSet, "paddingLeft"),
            pRight: spacesMapping(spacesSet, "paddingRight"),
            size: textSizesMapping(props.textSizes || textSizes)
        }}/>
    )
}
