export const playAudio = (buffers: any[], audioType: string) => {
    const blob = new Blob(buffers, {type: audioType});
    const url = window.URL.createObjectURL(blob)
    const audio = new Audio(url);
    return audio.play()
};

interface AudioRecorder {
    start: () => Promise<Blob>
    stop: () => void
}

export const createAudioRecorder = (audioType: string): Promise<AudioRecorder> => {
    if (!navigator?.mediaDevices?.getUserMedia) return Promise.reject(new Error("Media not supported"))

    return navigator?.mediaDevices?.getUserMedia({audio: true})
        .then(stream => {
            const media: any = MediaRecorder;
            if (media.notSupported) throw new Error("Record not supported")

            const mediaRecorder = new MediaRecorder(stream, {bitsPerSecond: 64000});

            const start = () => new Promise<Blob>((resolve, reject) => {
                const voice: Blob[] = [];
                mediaRecorder.addEventListener("error", (event) => {
                    stopStream(stream)
                    reject(event.error)
                });
                mediaRecorder.addEventListener("stop", (event) => {
                    stopStream(stream)
                    resolve(new Blob(voice, {type: audioType}))
                });
                mediaRecorder.addEventListener("dataavailable", (event) =>
                    voice.push(event.data)
                );
                mediaRecorder.start()
            })


            return {start, stop: () => mediaRecorder.stop()}
        });
}

const stopStream = (stream: MediaStream) => stream.getTracks().forEach(track => track.stop());
